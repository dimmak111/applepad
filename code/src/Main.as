package  
{
	import applepad.Constants;
	import applepad.Game;
	
	import com.sociodox.theminer.TheMiner;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	
	import starling.core.Starling;
	import starling.events.KeyboardEvent;
	
	[SWF(frameRate="30", width="1280", height="768", backgroundColor="#000")]
	public class Main extends Sprite
	{
		private var _starling:Starling;
	
		public function Main()
		{		
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			Starling.handleLostContext = true;
			Starling.multitouchEnabled = true;
			
			_starling = new Starling(Game, stage, null, null, Context3DRenderMode.AUTO, "baseline");
			
			_starling.antiAliasing = 2;	
			_starling.showStats = Constants.SHOW_STATS;
			
			_starling.start();
			
			//addChild(new TheMiner());
		}	
	}
}