package applepad   
{	
	import applepad.audio.SoundManager;
	import applepad.effects.Effect;
	import applepad.entities.Player;
	import applepad.prefs.Preferences;
	import applepad.sequences.AchievementSequence;
	import applepad.sequences.CharSelectSequence;
	import applepad.sequences.IntroSequence;
	import applepad.sequences.LoadingSequence;
	import applepad.sequences.LoginSequence;
	import applepad.sequences.MenuSequence;
	import applepad.sequences.MiniGameBubble;
	import applepad.sequences.MiniGameDance;
	import applepad.sequences.MiniGameSnow;
	import applepad.sequences.OptionSequence;
	import applepad.sequences.ResultSequence;
	import applepad.sequences.SelectSequence;
	import applepad.sequences.TutorialSequence;
	
	import flash.events.KeyboardEvent;
	import flash.events.TouchEvent;
	import flash.ui.Keyboard;
	
	import starling.core.Starling;
	import starling.display.Sprite;

	public class Game extends Sprite 
	{		
		private var sequenceManager:SequenceManager;
		private static var soundManager:SoundManager;
		
		private static var preferences:Preferences;
		
		private static var players:Vector.<Player>;
		private static var difficulty:uint; 
		
		public function Game() 
		{	
			//persistance
			preferences = new Preferences();
			preferences.load();
			
			//fonts
			AssetEmbed.loadFonts();
			AssetEmbed.loadBitmapFonts();
			
			//sound
			soundManager = new SoundManager();
			soundManager.loadAllSounds();
			
			//players information
			players = new Vector.<Player>(Constants.NUM_PLAYER_MAX);
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				players[i] = new Player();
			}
			
			// game difficulty
			difficulty = Constants.DEFAULT_DIFFICULTY;
			
			//effects managment
			Effect.Init();
			
			// sequences creations
			sequenceManager = new SequenceManager(this, new LoadingSequence());
			
			sequenceManager.addSequence("IntroSequence", new IntroSequence( ));
			sequenceManager.addSequence("LoginSequence", new LoginSequence( ));
			sequenceManager.addSequence("MenuSequence", new MenuSequence( ));
			sequenceManager.addSequence("AchievementSequence", new AchievementSequence( ));
			sequenceManager.addSequence("OptionSequence", new OptionSequence( ));
			sequenceManager.addSequence("SelectSequence", new SelectSequence( ));
			sequenceManager.addSequence("CharSelectSequence", new CharSelectSequence( ));
			sequenceManager.addSequence("TutorialSequence", new TutorialSequence( ));
			sequenceManager.addSequence("MiniGameBubble", new MiniGameBubble( ));
			sequenceManager.addSequence("MiniGameSnowFight", new MiniGameSnow( ));
			sequenceManager.addSequence("MiniGameDancePad", new MiniGameDance( ));
			sequenceManager.addSequence("ResultSequence", new ResultSequence( ));
			
			sequenceManager.setCurSequence(Constants.START_SEQUENCE);
		}
		
		public static function getPreferences():Preferences {
			return preferences;		
		}
		
		public static function getSoundManager():SoundManager {
			return soundManager;
		}
		
		public static function getPlayer(idx:uint):Player {
			
			if(idx < 0 || idx >= players.length) {
				trace("player: " + idx + "not found");
				return null;
			}		
			return players[idx];
		}
		
		public static function getDifficulty():uint {
			return difficulty;
		}
	}
}