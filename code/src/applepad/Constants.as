package applepad
{
	import starling.core.Starling;

	public class Constants
	{
		static public const START_SEQUENCE:String 			= "IntroSequence";
		static public const PREFERENCES:String				= "preferences";
		static public const DATABASE:String					= "applepad_database";
		static public const PREFS_VERSION:String			= "0.1";
		
		static public const RESET_PREFERENCES:Boolean		= false;
		static public const SHOW_STATS:Boolean 				= false;
		
		static public const FADE_IN_DURATION:Number			= 200.0;
		static public const FADE_OUT_DURATION:Number		= 200.0;
		
		static public const ALPHA_MAX:Number				= 0.99;
		static public const PIXEL_TO_METER:Number			= 30.0;
		
		static public const SCREEN_WIDTH:Number				= Starling.current.viewPort.width;
		static public const SCREEN_HEIGHT:Number			= Starling.current.viewPort.height;
		
		static public const SCREEN_HALF_WIDTH:Number 		= SCREEN_WIDTH >> 1;
		static public const SCREEN_HALF_HEIGHT:Number 		= SCREEN_HEIGHT >> 1;
		
		/**
		* GAME CONSTANTS
		*/
		static public const NUM_GAMES_MAX:uint				= 3;
		static public const NUM_PLAYER_MAX:uint				= 4;
		static public const NUM_CHARACTER_MAX:uint			= 8;
		
		static public const DIFFICULTY_EASY:uint			= 0;
		static public const DIFFICULTY_NORMAL:uint			= 1;
		
		static public const DEFAULT_DIFFICULTY:uint			= DIFFICULTY_EASY;
		
		/**
		* SOUND MAPPING
		*/
	}
}