package applepad.entities
{
	public class Player
	{
		private var _avatar:uint;
		private var _name:String;
		
		public function Player() {
			
			_avatar = 0;
			_name = "";
		}
		
		public function get name():String { return _name; }
		
		public function set avatar(a:uint):void { _avatar = a; }
		public function get avatar():uint { return _avatar; }
	}
}