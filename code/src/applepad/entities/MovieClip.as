package applepad.entities
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.textures.Texture;

	public class MovieClip extends starling.display.MovieClip
	{
		private var added:Boolean;
		
		public function MovieClip(textures:Vector.<Texture>, fps:Number=12, toJuggle:Boolean = false, isLooping:Boolean = true) {
			
			super(textures, fps);
			added = false;
			
			if(!isLooping) {
				loop = false;
			}
			
			if(toJuggle) {
				Starling.juggler.add(this);
				added = true;
			}
		}
		
		public function start():void {	
		
			if(!added) {
				Starling.juggler.add(this);
				added = true;
				
			} else {
				stop();
				play();
			}
		}
		
		public function destroy():void {
			
			Starling.juggler.remove(this);
			dispose();
			added = false;
		}
	}
}