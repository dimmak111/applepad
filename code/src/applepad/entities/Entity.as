package applepad.entities
{	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;

	public class Entity extends Sprite
	{	
		private var imgRef:Image;
		
		public function Entity() {
			
			imgRef = null;
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
			addEventListener(Event.REMOVED_FROM_STAGE, release);
			
			addEventListener(TouchEvent.TOUCH, touch);
		}
		
		public function setSprite(img:Image):void {
			
			if(imgRef)
				removeChild(imgRef);
			
			imgRef = img;
			addChild(img);
		}
		
		public function initialize():void {
		}
		
		public function update( elapsedTime:Number ):void {
		}
		
		public function release():void {
		}
		
		public function touch(e:TouchEvent):void {
		}
	}
}