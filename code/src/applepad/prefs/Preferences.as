package applepad.prefs
{
	import applepad.Constants;
	
	import flash.net.SharedObject;
	
	import org.as3commons.collections.Map;

	public class Preferences
	{
		private var _prefs:SharedObject;
		private var _datas:Map;
		
		public function Preferences() {
			_datas = new Map();
		}
		
		public function load():void {
			
			_prefs = SharedObject.getLocal(Constants.PREFERENCES);
			
			if(Constants.RESET_PREFERENCES)
				_prefs.clear();		
			
			if(_prefs.data.version == undefined) {
				
				_prefs.data.version = Constants.PREFS_VERSION;
				_prefs.data.sound = 0.5;
				_prefs.data.music = 0.5;
				_prefs.data.tutorial = true;
				
			}
				
			_datas.add("version", _prefs.data.version);
			_datas.add("sound", _prefs.data.sound);
			_datas.add("music", _prefs.data.music);
			_datas.add("tutorial", _prefs.data.tutorial);
		}
		
		public function set(key:String, value:*):void {
			_datas.replaceFor(key, value);
		}
		
		public function get(key:String):* {
			return _datas.itemFor(key);
		}
		
		public function save():void {
			
			_prefs.data.version = _datas.itemFor("version");
			_prefs.data.sound = _datas.itemFor("sound");
			_prefs.data.music = _datas.itemFor("music");
			_prefs.data.tutorial = _datas.itemFor("tutorial");
			
			_prefs.flush();
		}
		
		public function close():void {
			
			_datas.clear();
			_prefs.close();
		}
	}
}