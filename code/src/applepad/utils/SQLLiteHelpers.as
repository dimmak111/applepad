package applepad
	
{
	//imports for class sqliteaccess
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.errors.SQLError;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File; 
	
	// sqlite local_db class 
	public class SQLLiteHelpers
	{
		public var _conn:SQLConnection ; //variable of SQL Connection entities (difine the path and parameters of db connection)
		public var _dbConnection:SQLConnection ;
		private var _folder:File;
		private var _dbFile:File;
		private var _file_name:String;
		private var _createStmt:SQLStatement = new SQLStatement(); // Sql statement object
		
		
		public function SQLLiteHelpers()
		{
			_dbConnection = new SQLConnection;
			_conn = new SQLConnection();
			_file_name="DBSample.sqlite";
			_folder = File.applicationStorageDirectory; 
			_dbFile = _folder.resolvePath(_file_name);
			_createStmt = new SQLStatement();
			
		}
		
		public function init_open(file_name:String):void 
		{					
			
			// The database file is in the application storage directory 
			_file_name=file_name;
			_folder = File.applicationStorageDirectory; 
			_dbFile = _folder.resolvePath(_file_name); 	
			
			_conn.addEventListener(SQLEvent.OPEN, openHandler); 
			_conn.addEventListener(SQLErrorEvent.ERROR, open_errorHandler);
			_conn.addEventListener(SQLEvent.CLOSE,close_Handler);
			
			_conn.openAsync(_dbFile);
			//trace("Etat connection:",conn.connected);
		} 
		
		private function openHandler(event:SQLEvent):void 
		{ 
			trace("The database was successfully open on disk");
			
		} 
		
		private function open_errorHandler(event:SQLErrorEvent):void 
		{ 
			trace("Error message:", event.error.message); 
			trace("Details:", event.error.details);
			
		} 
		
		private function close_Handler(event:SQLEvent):void 
		{ 
			trace("The database was successfully close");
		} 
		
		
		public function execute_statement(sql:String):void 
		{ 
			// ... create and open the SQLConnection instance named conn ... 
			// chgmt pr passage par var de conn:SQLconnection pour agir sur une base ouverte.
			// exemple de statement:
			/*"CREATE TABLE IF NOT EXISTS Eleves (" +  
			"    empId INTEGER PRIMARY KEY AUTOINCREMENT, " +  
			"    firstName TEXT, " +  
			"    lastName TEXT, " +  
			"    score NUMERIC CHECK (score > 0)" +  
			")";*/ 
			_conn.addEventListener(SQLEvent.OPEN, openHandler); 
			_conn.addEventListener(SQLEvent.OPEN, ready_executeHandler); 
			_conn.addEventListener(SQLErrorEvent.ERROR, open_errorHandler);
			_conn.addEventListener(SQLEvent.CLOSE,close_Handler);
			
			_conn.openAsync(_dbFile);
			//trace("Etat connection:",_conn.connected);
			
			
			_createStmt.sqlConnection = _conn; 
			
			_createStmt.text = sql; 
			
			_createStmt.addEventListener(SQLEvent.RESULT, execute_statementResult); 
			_createStmt.addEventListener(SQLErrorEvent.ERROR, execute_statementError); 
			
			//_createStmt.execute();
			
			
			
		} 
		
		private function ready_executeHandler(event:SQLEvent):void 
		{ 
			trace("The statement was successfully execute on db");
			_createStmt.execute();
			
		} 
		
		private function execute_statementResult(event:SQLEvent):void 
		{ 
			trace("Statement executed");
			//_conn.close();
			
		} 
		
		private function execute_statementError(event:SQLErrorEvent):void 
		{ 
			trace("Error message:", event.error.message); 
			trace("Details:", event.error.details); 
			_conn.close();
		} 
		
		public function change_score(score:int,idEleve:int):void
		{
			var sql:String="UPDATE Eleves SET score ="+score+" WHERE EleveId="+idEleve;
			this.execute_statement(sql); 
		}
	}
}