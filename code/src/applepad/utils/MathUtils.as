package applepad.utils
{
	import starling.utils.deg2rad;

	public class MathUtils
	{	
		public static function abs(val:Number):Number {
			return Math.abs(val);
		}
		
		public static function ceil(val:Number):Number {
			return Math.ceil(val);
		}
		
		public static function floor(val:Number):Number {
			return Math.floor(val);
		}
		
		public static function sqrt(val:Number):Number {
			return Math.sqrt(val);
		}
		
		public static function cos(angle:Number):Number {
			return Math.cos(deg2rad(angle));
		}
		
		public static function sin(angle:Number):Number {
			return Math.sin(deg2rad(angle));
		}
		
		public static function min(a:Number, b:Number):Number {
			return a < b ? a : b; 
		}
		
		public static function max(a:Number, b:Number):Number {
			return a > b ? a : b; 
		}
		
		public static function clamp(val:Number, min:Number, max:Number):Number {
			
			val = val < min ? min : val;
			val = val > max ? max : val;
			
			return val;
		}
		
		public static function randf(min:Number, max:Number):Number {
			return min + Math.random() * (max - min);
		}
		
		public static function randi(min:int, max:int):int {
			return min + Math.random() * (max - min);
		}
	}
}