package applepad.utils
{
	import applepad.utils.MathUtils;
	
	public class Vector2
	{
		public var x:Number;
		public var y:Number;
		
		public function Vector2(x:Number = 0, y:Number = 0)
		{
			this.x = x;
			this.y = y;
		}
		
		public function clone(v:Vector2):void
		{
			this.x = v.x;
			this.y = v.y;
		}
		
		public function normalize():void {
			
			var l:Number = lenght();
			
			x /= l;
			y /= l;
		}
		
		public function lenght():Number {	
			return MathUtils.sqrt((x * x) + (y * y));
		}
		
		public function substract(a:Vector2):void {
			
			x -= a.x;
			y -= a.y;
		}
	}
}