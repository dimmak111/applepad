package applepad.utils
{
	public class VectorUtils 
	{
		public static function shuffle(obj:Object):void{
			var i:int = obj.length;
			while (i > 0) {
				var j:int = Math.floor(Math.random() * i);
				i--;
				var temp:* = obj[i];
				obj[i] = obj[j];
				obj[j] = temp;
			}				
		}
	}
}