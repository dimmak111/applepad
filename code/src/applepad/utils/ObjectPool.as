package applepad.utils
{		
	public class ObjectPool 
	{
		private var _pool:Vector.<IPoolable>; 
		private var _counter:uint;
		
		private var _type:Class;
		
		public function ObjectPool(type:Class, size:uint = 20, resetMethod:String="") {	
			
			_pool = new Vector.<IPoolable>(size);
			
			for(var i:uint = 0; i < size; ++i) {
				_pool[i] = new type;
			}
			
			_type = type;
			_counter = size;
		}
		
		public function create():* {
			
			if(_counter > 0) {
				--_counter;
				return _pool.pop();
			}
			
			return null;
		}
		
		public function remove(object:IPoolable):void {
			
			object.reset();
			
			++_counter;
			_pool.push(object);
		}
		
		/*
		public function destroy(object:IPoolable):void {
			
			object.destroy();
			
		}
		*/
	}
}