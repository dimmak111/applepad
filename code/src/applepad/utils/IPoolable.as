package applepad.utils
{
	public interface IPoolable
	{
		function reset():void;
		function destroy():void;
	}
}