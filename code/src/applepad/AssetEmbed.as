package applepad
{
	import flash.display.Bitmap;
	import flash.text.Font;
	
	import org.as3commons.collections.Map;
	
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;

	public class AssetEmbed {
		
		[Embed(source="assets/fonts/grobold.ttf", fontName="grobold", fontWeight="normal", advancedAntiAliasing="true", embedAsCFF=false)] 
		static public var grobold:Class;
		
		[Embed(source="assets/fonts/toonish.ttf", fontName="toonish", fontWeight="normal", advancedAntiAliasing="true", embedAsCFF=false)] 
		static public var toonish:Class;
		
		[Embed(source="assets/fonts/custom.xml", mimeType="application/octet-stream")]
		public static const CustomFontXml:Class;
		
		[Embed(source = "assets/fonts/custom.png")]
		public static const CustomFontTexture:Class;
		
		private static var _fonts:Map;
		private static var _isFontsLoaded:Boolean = false;
		
		private static var _isBitmapFontLoaded:Boolean = false;
		
		public static function loadFonts():void {
			
			if(!_isFontsLoaded) {
				
				_fonts = new Map();
				
				_fonts.add("grobold", new grobold());
				_fonts.add("toonish", new toonish());
				
				_isFontsLoaded = true;
			}
		}
		
		public static function loadBitmapFonts():void {
			
			if(!_isBitmapFontLoaded) {
				
				var texture:Texture = Texture.fromBitmap(new CustomFontTexture());
				var xml:XML = XML(new CustomFontXml());
				TextField.registerBitmapFont(new BitmapFont(texture, xml));
				
				_isBitmapFontLoaded = true;
			}
		}
		
		public static function getFont(name:String):Font {
			
			if(_fonts) {
			
				if(_fonts.hasKey(name)) {
					return _fonts.itemFor(name);	
				}
				else {
					throw new Error("Font " + name + " not found");
				}
			}
			
			throw new Error("Fonts not loaded");
		}
	}
}