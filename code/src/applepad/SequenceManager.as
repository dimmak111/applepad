package applepad
{	
	import applepad.sequences.LoadingSequence;
	import applepad.sequences.Sequence;
	import applepad.sequences.SequenceParams;
	
	import flash.system.System;
	import flash.utils.getTimer;
	
	import org.as3commons.collections.Map;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	
	public class SequenceManager
	{	
		private var _sequences:Map;
		private var _curSequence:Sequence;
		private var _root:Sprite;
		
		private var _passedTime:Number;
		
		private var _assetManager:AssetManager;
		private var _loadingSequence:Sequence;
		
		public function SequenceManager( root:Sprite, load:Sequence = null )  {
			
			_sequences 		= new Map();
			_curSequence 	= null;
			_root 			= root;
			
			_passedTime 	= 0;
			
			_assetManager 	= new AssetManager();
			
			if(load) {
				_loadingSequence = load;
				_loadingSequence.prepare(null);
			}
		}
		
		public function addSequence( key:String, sequence:Sequence ):void {
			_sequences.add( key, sequence );
		}
		
		public function setCurSequence( key:String, params:Array=null ):void {				
			
			if(!_sequences.hasKey(key)) {
				throw new Error("Sequence : " + key + " doesn't exist");
			}
			
			_curSequence = _sequences.itemFor( key );
			
			if(params != null) {
				_curSequence.setUserParams(params);
			}
			
			_curSequence.addEventListener( Event.ADDED_TO_STAGE, prepareCurSequence );
			_curSequence.addEventListener( Event.COMPLETE, releaseCurSequence );
			
			_root.addChild( _curSequence );		
		}
		
		public function getCurSequence():Sequence {
			return _curSequence;
		}
		
		private function prepareCurSequence():void {
			_curSequence.prepare(_assetManager);
			
			if(!_assetManager.isQueueEmpty())
			{
				_assetManager.addEventListener( Event.COMPLETE, initCurSequence );
				_assetManager.loadAssets( );
				
				if(_loadingSequence) {
					_root.addChild(_loadingSequence);
					_loadingSequence.initialize(null);
				}
			}
			else {
				
				_curSequence.alpha = Constants.ALPHA_MAX;
				initCurSequence( );
			}
		}
		
		private function initCurSequence():void {
			
			if(_loadingSequence) {
				_root.removeChild(_loadingSequence);
				_loadingSequence.release();
			}
			
			_passedTime = getTimer();
			_curSequence.initialize(_assetManager);
			
			_curSequence.addEventListener( Event.ENTER_FRAME, updateCurSequence );
		}
		
		private function updateCurSequence():void {
			
			var curTime:Number = getTimer();
			var elapsedTime:Number = curTime - _passedTime;
			
			_curSequence.update(elapsedTime);
			_curSequence.updateEffects(elapsedTime);
			
			_passedTime = curTime;
		}
		
		private function releaseCurSequence( e:Event ):void {
			
			_curSequence.removeEventListeners( );
			_curSequence.removeChildren();
			_curSequence.release();
			
			_assetManager.removeEventListeners();
			_assetManager.freeAssets();
			
			// force garbage collector
			//System.gc();
			
			if( e.data )
				nextSequence( e.data as SequenceParams );
		}
		
		private function nextSequence( key:SequenceParams ):void {
			
			_root.removeChild( _curSequence );
			
			setCurSequence( key.nextSequence, key.userParams );
		}	
	}
}