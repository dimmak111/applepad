package applepad.audio
{
	import applepad.Game;
	import applepad.utils.MathUtils;
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import org.as3commons.collections.Map;
	import org.as3commons.collections.framework.IIterator;
	import org.as3wavsound.WavSound;
	import org.as3wavsound.WavSoundChannel;
	
	public class SoundManager
	{
		private var soundMap:Map;
		private var musicMap:Map;
		
		private var musicChannels:Map;
		private var soundChannels:Map;
		
		private var musicVolume:SoundTransform;
		private var soundVolume:SoundTransform;
		
		public function SoundManager() {
			
			musicMap = new Map();
			soundMap = new Map();
			
			musicChannels = new Map();
			soundChannels = new Map();
			
			musicVolume = new SoundTransform();
			musicVolume.volume = Game.getPreferences().get("music");
			
			soundVolume = new SoundTransform();
			soundVolume.volume = Game.getPreferences().get("sound");
		}
		
		// called once
		public function loadAllSounds():void {
					
			loadMp3Music("menu", 					"audio/music/menu_bgm.mp3");
			loadMp3Music("dancepad", 				"audio/music/dancepad_bgm.mp3");
			loadMp3Music("snowfight", 				"audio/music/snowfight_bgm.mp3");	
			loadMp3Music("bubble", 					"audio/music/bubbles_bgm.mp3");
			loadMp3Music("end", 					"audio/music/end_jiggle.mp3");
			loadMp3Music("dancepad_end", 			"audio/music/dancepad_win.mp3");
			
			loadWavSound("slide", 					"audio/sounds/caroussel_slide.wav");
			loadWavSound("checkbox", 				"audio/sounds/caroussel_checkbox.wav");
			
			loadWavSound("bubble_start", 			"audio/sounds/bubble_start.wav");
			loadWavSound("bubble_wrong", 			"audio/sounds/bubble_wrong.wav");
			
			loadWavSound("dancepad_good", 			"audio/sounds/dancepad_good.wav");
			loadWavSound("dancepad_wrong", 			"audio/sounds/dancepad_wrong.wav");
			loadWavSound("dancepad_up", 			"audio/sounds/dancepad_up.wav");
			loadWavSound("dancepad_down", 			"audio/sounds/dancepad_down.wav");
			loadWavSound("dancepad_left", 			"audio/sounds/dancepad_left.wav");
			loadWavSound("dancepad_right", 			"audio/sounds/dancepad_right.wav");
		
			loadWavSound("snowfight_good", 			"audio/sounds/snowfight_good.wav");
			loadWavSound("snowfight_snowball", 		"audio/sounds/snowfight_snowball.wav");
			loadWavSound("snowfight_miss", 			"audio/sounds/snowfight_miss.wav");
			loadWavSound("snowfight_ending", 		"audio/sounds/snowfight_ending.wav");
			loadWavSound("snowfight_background",	"audio/sounds/snowfight_background.wav");
		}
		
		public function set music_volume(v:Number):void {
			
			v = MathUtils.clamp(v, 0.0, 1.0);
			
			musicVolume.volume = v;
			
			var it:IIterator = musicChannels.iterator();
			while(it.hasNext()) 
			{
				var m:SoundChannel = it.next();
				m.soundTransform = musicVolume;
			}
		}
		
		public function set sound_volume(v:Number):void {
			
			v = MathUtils.clamp(v, 0.0, 1.0);
			
			soundVolume.volume = v;
			
			var it:IIterator = soundChannels.iterator();
			while(it.hasNext()) 
			{
				var s:WavSoundChannel = it.next();
				s.soundTransform.volume = soundVolume.volume;
			}
		}
	
		private function loadMp3Music(key:String, file:String):void {
			
			var music:Sound = new Sound();
			music.load( new URLRequest(file) );
			musicMap.add(key, music);	
		}
		
		private function loadWavSound(key:String, file:String):void {
	
			soundMap.add(key, new Wav());
			
			var url:URLRequest = new URLRequest(file);  
			var wav:URLLoader = new URLLoader();  
			wav.dataFormat = 'binary';  
			wav.load(url);  
			wav.addEventListener(Event.COMPLETE, function onComplete(e:Event):void {	
				var w:Wav = soundMap.itemFor(key);
				w.sound = new WavSound(e.target.data as ByteArray);
			});  
		}
		
		public function playSound(key:String):void {
			
			if(!soundMap.hasKey(key))
				return;
			
			var wav:Wav = soundMap.itemFor(key);
			var c:WavSoundChannel = wav.sound.play();
			
			c.soundTransform.volume = soundVolume.volume;
			if(!soundChannels.add(key, c)) {
				soundChannels.replaceFor(key, c);
			}
		}
		
		public function stopSound(key:String):void {
			
			if(!soundChannels.hasKey(key))
				return;
			
			var c:WavSoundChannel = soundChannels.itemFor(key);
			c.stop();
			soundChannels.remove(c);
		}
		
		public function playMusic(key:String):void {
			
			if(!musicMap.hasKey(key))
				return;
			
			stopMusic(key);
			
			var m:Sound = musicMap.itemFor(key);
			var c:SoundChannel = m.play();
			
			c.soundTransform = musicVolume;
			if(musicChannels.add(key, c)) {
				musicChannels.replaceFor(key, c);
			}
		}
		
		public function stopMusic(key:String):void {
			
			if(!musicChannels.hasKey(key))
				return;
			
			var c:SoundChannel = musicChannels.itemFor(key);
			c.stop();
			musicChannels.remove(c);
		}
		
		public function destroy():void {
			
			musicMap.clear();
			soundMap.clear();	
		}
	}
}

import org.as3wavsound.WavSound;

internal class Wav {
	
	public var sound:WavSound;
	
	public function Wav() {
		sound = null;
	}
	
}