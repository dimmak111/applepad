package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	import applepad.entities.MovieClip;
	import applepad.prefs.Preferences;
	import applepad.ui.Button;
	import applepad.ui.CheckBox;
	import applepad.ui.IButton;
	import applepad.ui.ICheckBox;
	import applepad.ui.ISliderBar;
	import applepad.ui.SliderBar;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.events.Event;

	public class OptionSequence extends Sequence implements IFadeIn, ISliderBar, ICheckBox, IButton {
		
		/******************** Graphics component ****************/
		private var _background:Image;
		private var _base:Image;
		private var _splash:MovieClip;
		private var _option:MovieClip;
		private var _display:MovieClip;
		private var _sound:MovieClip;
		private var _colors:MovieClip;
		
		/******************** Offset constant ****************/
		private var OFFSET_ANIM_DECAL:uint = 18;
		private var OFFSET_UI_DECAL_X:uint = 40;
		private var OFFSET_UI_DECAL_Y_FX:uint = 148;
		private var OFFSET_UI_DECAL_Y_MUSIC:uint = 207;
		private var OFFSET_BACKBUTTON_DECAL:uint = 150;
		private var OFFSET_ANIM_SOUND_Y:uint = 82;
		private var OFFSET_ANIM_COLORS_Y:uint = 25;
		private var OFFSET_ANIM_DISPLAY_Y:uint = 30;
		private var OFFSET_UI_CHECK_DECAl_X:uint = 207;
		private var OFFSET_UI_CHECK_DECAl_Y:uint = 116;
		/******************** Feather component ****************/
		private var _fxSlider:SliderBar;
		private var _musicSlider:SliderBar;
		private var _backButton:Button;
		
		private var _yes:CheckBox;
	
		
		public function OptionSequence() {
			
		}
	
		public override function prepare(assetManager:AssetManager):void {
			
			assetManager.loadAtlas("options", "option_01");
			assetManager.loadAtlas("options", "option_02");
			assetManager.loadAtlas("ui", "ui_atlas");
		}
		
		public function initDisplay (assetManager:AssetManager) : void {
			_background = assetManager.getImageFromAtlas("options", "option_01", "BG");
			_background.height = Constants.SCREEN_HEIGHT + 5;
			
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			
			addChild(_background);
			
			_base = assetManager.getImageFromAtlas("options", "option_01", "base");
			_base.touchable = false;
			_base.x = Constants.SCREEN_HALF_WIDTH - ((_base.width >> 1)-10);
			
			addChild(_base);
						
			
			_splash = assetManager.getMovieFromAtlas("options", "option_01", "Composition 1_", 25, false, false);
			_splash.height = Constants.SCREEN_HEIGHT;
			_splash.scaleY = 1.1;
			_splash.touchable = false;
			
			addChild(_splash);
			
			_option = assetManager.getMovieFromAtlas("options", "option_01", "option_", 25, false, false); 
			_option.addEventListener(Event.COMPLETE, completedHandler);
			_option.touchable = false;
			_option.x = Constants.SCREEN_HALF_WIDTH - (_option.width >> 1);
			
			addChild(_option);
			
			_sound= assetManager.getMovieFromAtlas("options", "option_02", "sound_", 25);
			_sound.addEventListener(Event.COMPLETE, completedHandler);
			_sound.loop = false;
			_sound.touchable = false;
			_sound.visible = false;
			_sound.x = Constants.SCREEN_HALF_WIDTH - ((_base.width >> 1) + OFFSET_ANIM_DECAL) ;
			_sound.y += OFFSET_ANIM_SOUND_Y; 
			
			
			addChild(_sound);
			
			_colors = assetManager.getMovieFromAtlas("options", "option_02", "colors_", 25);
			_colors.addEventListener(Event.COMPLETE, completedHandler);
			_colors.loop = false;
			_colors.touchable = false;
			_colors.visible = false;
			_colors.x = Constants.SCREEN_HALF_WIDTH - ((_base.width >> 1) + OFFSET_ANIM_DECAL) ;
			_colors.y = _sound.y + (_sound.height >> 1) + OFFSET_ANIM_COLORS_Y;
			addChild(_colors);
			
			
			_display= assetManager.getMovieFromAtlas("options", "option_02", "display_", 25);
			_display.addEventListener(Event.COMPLETE, completedHandler);
			_display.loop = false;
			_display.touchable = false;
			_display.visible = false;
			_display.x = Constants.SCREEN_HALF_WIDTH - ((_base.width >> 1) + OFFSET_ANIM_DECAL - 20) ;
			_display.y = _colors.y + (_colors.height >> 1 ) + OFFSET_ANIM_DISPLAY_Y;
			
			addChild(_display);
		}
		
		public function initComponent(assetManager:AssetManager) : void {
			
			/*************fxSlider *****************/
			_fxSlider = new SliderBar(assetManager.getTextureFromAtlas("ui", "ui_atlas", "empty"), assetManager.getTextureFromAtlas("ui", "ui_atlas", "full"), 0.0, 1.0, Game.getPreferences().get("sound"), this);
			addChild(_fxSlider);	
			_fxSlider.visible = false;
			_fxSlider.x = _base.x + OFFSET_UI_DECAL_X;
			_fxSlider.y = _sound.y + OFFSET_UI_DECAL_Y_FX;

			
			/*********************Music Slider *************/
			_musicSlider = new SliderBar(assetManager.getTextureFromAtlas("ui", "ui_atlas", "empty"), assetManager.getTextureFromAtlas("ui", "ui_atlas", "full"), 0.0, 1.0, Game.getPreferences().get("music"), this);
			addChild(_musicSlider);
			_musicSlider.visible = false;
			_musicSlider.x = _base.x + OFFSET_UI_DECAL_X;
			_musicSlider.y = _sound.y + OFFSET_UI_DECAL_Y_MUSIC;
			
			/*************** BackButton *****************/			
			_backButton = new Button(assetManager.getTextureFromAtlas("ui", "ui_atlas", "back"), assetManager.getTextureFromAtlas("ui", "ui_atlas", "back_hover"), this);
			
			_backButton.x = Constants.SCREEN_WIDTH - OFFSET_BACKBUTTON_DECAL;
			_backButton.y = Constants.SCREEN_HEIGHT - OFFSET_BACKBUTTON_DECAL;
			_backButton.visible = false;
			
			addChild(_backButton);
			
		
			/*************CheckBox**************/
			
			var tutorial:Boolean = Game.getPreferences().get("tutorial");
			_yes = new CheckBox(assetManager.getTextureFromAtlas("ui", "ui_atlas", "emptyBox"), assetManager.getTextureFromAtlas("ui", "ui_atlas", "fullBox"), tutorial, this);				
			addChild(_yes);
			_yes.x = _base.x + OFFSET_UI_CHECK_DECAl_X;
			_yes.y = _display.y + OFFSET_UI_CHECK_DECAl_Y;
			_yes.visible = false;
		}
		
		public override function initialize(assetManager:AssetManager):void {
			
			initDisplay(assetManager);
			initComponent(assetManager);
			
			doFadeIn(this, 250, this);
		}
		
		
		public function completedHandler (e:Event) :void {
			
			Starling.juggler.remove(e.target as MovieClip);
			
			switch(e.target) {
				case _option:
				{
					_backButton.visible = true;
					_sound.visible = true;
					Starling.juggler.add(_sound);
				}
				break;
				case _sound:
				{
					_fxSlider.visible = true;
					_musicSlider.visible = true;
					_colors.visible = true;
					Starling.juggler.add(_colors);
				}
				break;
				case _colors:
				{
					_display.visible = true;
					Starling.juggler.add(_display);
				}
				break;
				case _display:
				{
					_yes.visible = true;
				}
			}
		}
		public function onFadeInFinished(e:FadeIn):void {
			
			_splash.start();
			_option.start();
		}
		
		public function onButton(b:Button):void {
			
			if(b.state == Button.BUTTON_NORMAL) {
				
				Game.getPreferences().set("sound", _fxSlider.value);
				Game.getPreferences().set("music", _musicSlider.value);
				Game.getPreferences().set("tutorial", _yes.state);
				
				Game.getPreferences().save();
				
				changeSequence("MenuSequence");		
			}
		}
		
		
		public function onSliderChange(slider:SliderBar):void {
			
			if(slider == _musicSlider) {
				Game.getSoundManager().music_volume = slider.value;
			}
			else {
				Game.getSoundManager().sound_volume = slider.value;
			}
		}
		
		public function onCheckBox(checkBox:CheckBox):void {
			Game.getSoundManager().playSound("checkbox");			
		}
		
		public override function release():void {
			
			_splash.destroy();
			_option.destroy();	
			
			//cleanup everything
		}
	}
}