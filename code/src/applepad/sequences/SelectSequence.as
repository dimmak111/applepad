package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	import applepad.effects.ITween;
	import applepad.entities.Entity;
	import applepad.ui.Button;
	import applepad.ui.IButton;
	import applepad.ui.widgets.ISlider;
	import applepad.ui.widgets.Slider;
	import applepad.ui.widgets.SliderSheet;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.core.starling_internal;
	import starling.display.Image;
	import starling.display.Sprite;

	public class SelectSequence extends Sequence implements ISlider, ITween, IButton
	{
		private var slider:Slider;
		
		private var sliderItems:Vector.<SliderSheet>;
		private var slideName:Array = ["bubble_slide", "snowfight_slide", "dancepad_slide"];
		
		private var sliderWidth:Number = Starling.current.viewPort.width;
		private var sliderHeight:Number = 400.0;
		
		private var _title:Image;
		private var _backButton:Button;
		private var _play:Image;
		
		private var _fade:Tween;
				
		override public function prepare(am:AssetManager):void {
		
			am.loadAtlas("slider", "slider_atlas");
			am.loadAtlas("ui", "ui_atlas" );
			
			sliderItems = new Vector.<SliderSheet>;
		}
		
		override public function initialize(am:AssetManager):void {
			
			var background:Image = am.getImageFromAtlas("slider", "slider_atlas", "background");
			addChild(background);
			
			_play = am.getImageFromAtlas("slider", "slider_atlas", "play");
			_play.pivotX = 0;
			_play.pivotY = _play.height;
			_play.scaleX = 0;
			_play.scaleY = 0;
			
			
			
			_title = am.getImageFromAtlas("slider", "slider_atlas", "title");
			
			_title.x = Constants.SCREEN_WIDTH - _title.width >> 1;
			_title.y = -_title.height;
			
			addChild(_title);
			
			slider = new Slider(Starling.current.viewPort.width - sliderWidth >> 1, 
								Starling.current.viewPort.height - sliderHeight >> 1, 
								sliderWidth, sliderHeight, this);
			
			for(var i:uint = 0; i < Constants.NUM_GAMES_MAX; ++i) {
				
				var slide:Image = am.getImageFromAtlas("slider", "slider_atlas", slideName[i]);
				
				sliderItems[i] = new SliderSheet();
				sliderItems[i].addChild(slide);
				
				
				slider.addSheet(sliderItems[i]);
			}
			
			_backButton = new Button(am.getTextureFromAtlas("ui","ui_atlas", "back"), am.getTextureFromAtlas("ui","ui_atlas", "back_hover"),this);
			_backButton.x = Constants.SCREEN_WIDTH - _backButton.width - 50;
			_backButton.y = Constants.SCREEN_HEIGHT - _backButton.height;
			addChild(_backButton);
			
			addChild(slider);
			
			_fade = Effect.CreateFadeIn(this, 1.0, "linear", this);
			
		}
		
		public function onItemChanged(item:Sprite):void {
			_play.removeFromParent(false);
			_play.scaleX = 0;
			_play.scaleY = 0;
			_play.x = 0;
			_play.y = item.height;
			item.addChild(_play);
			Effect.CreateScaleTo(_play,1,0.5,Transitions.EASE_IN_OUT_BACK);

		}
		
		public function onItemSelected(item:Sprite):void {
			
			if(item == sliderItems[0] )	{
				if ( Game.getPreferences().get("tutorial")) {
					
					changeSequence("TutorialSequence", ["MiniGameBubble", "bubble"]);
				}
				else {
					Game.getSoundManager().stopMusic("menu");
					changeSequence("MiniGameBubble");
				}
			}
			else if(item == sliderItems[1]) {
				
				if ( Game.getPreferences().get("tutorial")) {
					changeSequence("TutorialSequence", ["MiniGameSnowFight", "snowfight"]);
				}
				else {
					Game.getSoundManager().stopMusic("menu");
					changeSequence("MiniGameSnowFight");
				}
				
			}
			else if(item == sliderItems[2]) {
				if ( Game.getPreferences().get("tutorial")) {
					changeSequence("TutorialSequence", ["MiniGameDancePad", "dancepad"]);
				}
				else {
					Game.getSoundManager().stopMusic("menu");
					changeSequence("MiniGameDancePad");
				}	
			}
		}
		
		public function onButton (b:Button) : void {
			
			if (b.state == Button.BUTTON_TOUCHED){
				changeSequence("MenuSequence");
			}
		}
		
		public function onTweenFinished(tween:Tween):void {
		
			if(_fade == tween) {
				Effect.CreateMoveTo(_title, _title.x, 0, 0.25, Transitions.EASE_OUT_BACK);
			}
		}
	}
}