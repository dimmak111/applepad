package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.effects.FadeOut;
	import applepad.effects.IFadeOut;
	
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import starling.core.Starling;

	public class IntroSequence extends Sequence implements IFadeOut
	{
		
		private const INTRO:String 	= "app:/intro/intro.flv";
		
		private var intro:Video;
		private var nc:NetConnection;
		private var ns:NetStream;
		
		public function IntroSequence()
		{
			intro = new Video(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			
			nc = new NetConnection();
			nc.connect(null);
			
			ns = new NetStream(nc);
			ns.client = this;
		}
		
		public override function prepare(am:AssetManager):void {
			
			Starling.current.nativeOverlay.addChild(intro);
			intro.attachNetStream(ns);
			
			ns.play(INTRO);
		}
		
		public override function release():void {
			intro = null;
		}
		
		public function onMetaData(e:Object):void {
		}
		
		public function onXMPData(e:Object):void {	
		}
		
		public function onPlayStatus(e:Object):void {
			
			if (e.code == "NetStream.Play.Complete") {
				Starling.current.nativeOverlay.removeChild(intro);
				doFadeOut(this, 500, this);
			}
		}
		
		public function onFadeOutFinished(e:FadeOut):void {
			ns.dispose();
			changeSequence("MenuSequence");
		}
	}
}