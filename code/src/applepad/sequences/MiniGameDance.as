package applepad.sequences
{
	import applepad.AssetEmbed;
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.FadeOut;
	import applepad.effects.IFadeIn;
	import applepad.effects.IFadeOut;
	import applepad.effects.ITween;
	import applepad.entities.MovieClip;
	import applepad.utils.MathUtils;
	import applepad.utils.Vector2;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.deg2rad;

	public class MiniGameDance extends MiniGame implements ITween
	{	
		// consts
		private const TEXTURE_ATLAS_PAD:String 		= "dancepad_arrows_atlas";
		private const TEXTURE_ATLAS_BOOMER:String	= "dancepad_boomer_atlas";
		private const TEXTURE_ATLAS_STARTER:String	= "dancepad_starter_atlas";
		private const XML_THEMES:String 			= "dancepad_themes";
		
		private const STARTER_3:uint 				= 0;
		private const STARTER_2:uint 				= 1;
		private const STARTER_1:uint 				= 2;
		private const STARTER_GO:uint		 		= 3;
		
		private const NUM_PADS_MAX:uint 			= Constants.NUM_PLAYER_MAX;
		private const NUM_TONES_MAX:uint 			= 25;
		private const NUM_ARROW_MAX:uint			= 4;
		
		private const DURATION_MIN:uint				= 2000;
		private const DURATION_MAX:uint				= 3000;
		
		// data
		private var _themes:XML;
		
		// game objects
		private var _starter:Vector.<MovieClip>;
		private var _background:Image;
		private var _boomer:MovieClip;
		
		private var _controllers:Vector.<Controller>;
		
		private var _word_ref:Vector.<String>;
		private var _word_sequence:Vector.<Word>;
		
		private var _scores:Vector.<Number>;
		
		private var _texts:Vector.<TextField>;
		private var _answered:Vector.<Boolean>;
		
		private var _fade:Tween;
		
		public function MiniGameDance() {
			
			_scores = new Vector.<Number>(Constants.NUM_PLAYER_MAX);
			
			_word_ref = new Vector.<String>(NUM_ARROW_MAX);	
			_controllers = new Vector.<Controller>(Constants.NUM_PLAYER_MAX);
			
			_texts = new Vector.<TextField>(Constants.NUM_PLAYER_MAX);
			_starter = new Vector.<MovieClip>(STARTER_GO+1);
			
			_answered = new Vector.<Boolean>(Constants.NUM_PLAYER_MAX);
		}
		
		protected override function prepareGame(am:AssetManager):void {
			
			_themes = am.loadXML("xml", XML_THEMES);
			
			am.loadTexture("minigame_dancepad", "background2");
			am.loadAtlas("minigame_dancepad", TEXTURE_ATLAS_PAD);
			am.loadAtlas("minigame_dancepad", TEXTURE_ATLAS_BOOMER);
			am.loadAtlas("minigame_dancepad", TEXTURE_ATLAS_STARTER);
			
			am.loadAtlas("ui", "char_faces_atlas");
		}
		
		protected override function initGame(am:AssetManager):void {
		
			
			_background = new Image(am.getTexture("minigame_dancepad", "background2"));
			_background.touchable = false;
			_background.blendMode = BlendMode.NONE;
			addChild(_background);
			
			_boomer = am.getMovieFromAtlas("minigame_dancepad", TEXTURE_ATLAS_BOOMER, "boomer_intro_", 25, false, true);
			
			_boomer.x = Constants.SCREEN_HALF_WIDTH - (_boomer.width >> 1);
			_boomer.y = Constants.SCREEN_HALF_HEIGHT - (_boomer.height >> 1);
			
			_boomer.frameLoop = 16;
			addChild(_boomer);
			
			var i:uint = 0;
			
			//pick a theme
			var theme:uint = MathUtils.randi(0, _themes.THEME.length()-1);
			var textures:Vector.<Texture> = new Vector.<Texture>(NUM_ARROW_MAX*2);
			
			for(i = 0; i < NUM_ARROW_MAX; ++i) {
				_word_ref[i] = _themes.THEME.WORD[i];
			}
			
			// create game sequence
			_word_sequence = new Vector.<Word>(NUM_TONES_MAX);
			for(i = 0; i < NUM_TONES_MAX; ++i) {
				
				var value:uint = MathUtils.randi(0, NUM_ARROW_MAX);
				var duration:int = MathUtils.randi(DURATION_MIN, DURATION_MAX);
	
				_word_sequence[i] = new Word(value, duration, _themes.THEME.WORD[value].@SOUND);
			}
			
			// create controllers
			for(i = 0; i < NUM_ARROW_MAX; i++) {
				var textName:String =  _themes.THEME.WORD[i].@TEXTURE;
				textures[i*2] = am.getTextureFromAtlas("minigame_dancepad", TEXTURE_ATLAS_PAD, textName);
				textures[i*2+1] = am.getTextureFromAtlas("minigame_dancepad", TEXTURE_ATLAS_PAD, textName+"_hover");
			}
			
			var keymap:Vector.<uint> = new Vector.<uint>();
			keymap.push(0,1,2,3, 0,2,1,3, 0,2,1,3, 0,1,2,3);
			
			var cpos:Vector.<Vector2> = new Vector.<Vector2>();
			cpos.push(new Vector2(0, 0),
					  new Vector2(Constants.SCREEN_WIDTH, 0),
					  new Vector2(0, Constants.SCREEN_HEIGHT),
					  new Vector2(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT));
			
			var crot:Vector.<Vector2> = new Vector.<Vector2>();
			crot.push(new Vector2(1, 1),
				new Vector2(-1, 1),
				new Vector2(1, -1),
				new Vector2(-1, -1));
			
			for(i = 0; i < NUM_PADS_MAX; ++i) {
				
				var keys:Vector.<uint> = keymap.slice(i*NUM_PADS_MAX, (i*NUM_PADS_MAX)+NUM_PADS_MAX); 
				_controllers[i] = new Controller(keys, textures, am.getTextureFromAtlas("ui", "char_faces_atlas", "char_0"+Game.getPlayer(i).avatar));
				
				_controllers[i].x = cpos[i].x;
				_controllers[i].y = cpos[i].y;
				
				_controllers[i].scaleX = crot[i].x;
				_controllers[i].scaleY = crot[i].y;
				
				_controllers[i].name = i.toString();
				
				_controllers[i].addEventListener("TouchCustom", onControllerTouch);
				
				addChild(_controllers[i]);
			}
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_answered[i] = false;
			}
			
			//hud
			
			var starterName:Array = new Array("03_", "02_", "01_", "go_");
			for (i = 0 ; i < STARTER_GO+1; ++i) {
				
				var mv:MovieClip = am.getMovieFromAtlas("minigame_dancepad", TEXTURE_ATLAS_STARTER, starterName[i], 15);
				mv.addEventListener(Event.COMPLETE, completeHandler);
				mv.loop = false;
				mv.touchable = false;			
				mv.visible = false;
				mv.x = Constants.SCREEN_HALF_WIDTH - (mv.width >> 1);
				mv.y = Constants.SCREEN_HALF_HEIGHT - (mv.height >> 1);	
				
				mv.stop();
				
				_starter[i] = mv;
				
				addChild(mv);
				Starling.juggler.add(mv);
			}
			
			
			var textpos:Vector.<Vector2> = new Vector.<Vector2>();
			textpos.push(new Vector2(Constants.SCREEN_WIDTH >> 2, (Constants.SCREEN_HEIGHT >> 2) + 50),
				new Vector2(Constants.SCREEN_HALF_WIDTH + (Constants.SCREEN_WIDTH >> 2), (Constants.SCREEN_HEIGHT >> 2) + 50),
				new Vector2(Constants.SCREEN_WIDTH >> 2, Constants.SCREEN_HALF_HEIGHT + (Constants.SCREEN_HALF_HEIGHT >> 2)),
				new Vector2(Constants.SCREEN_HALF_WIDTH + (Constants.SCREEN_WIDTH >> 2), Constants.SCREEN_HALF_HEIGHT + (Constants.SCREEN_HALF_HEIGHT >> 2)));
			
			var rot:Vector.<int> = new Vector.<int>();
			rot.push(-40, 40, 40, -40);
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				
				_texts[i] = new TextField(200, 55, "LEFT", AssetEmbed.getFont("grobold").fontName, 45, 0x652804);
				
				_texts[i].pivotX = _texts[i].width >> 1;
				_texts[i].pivotY = _texts[i].height >> 1;
				
				_texts[i].rotation = deg2rad(rot[i]);
				
				_texts[i].x = textpos[i].x;
				_texts[i].y = textpos[i].y;
				
				if( i < 2) {
					_texts[i].scaleX = -1;
					_texts[i].scaleY = -1;
				}
					
				addChild(_texts[i]);
				
				_texts[i].scaleX = 0;
				_texts[i].scaleY = 0;
			}
			
			_fade = Effect.CreateFadeIn(this, 0.5, "linear", this);
		}
		
		protected override function introGame():Boolean {
			return true;
		}
		
		protected override function restartGame():void {
			
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_scores[i] = 0;
			}
		}
		
		private function isSequenceFinished():Boolean {
			return (_word_sequence.length <= 0);
		}
		
		private function getCurWord():Word {
			return _word_sequence[0];
		}
		
		protected override function switchGameState(state:uint):void {
			super.switchGameState(state);
			
			switch(state) {
				
				case GAME_STATE_INTRO:
				break;
				
				case GAME_STATE_UPDATE:
				Game.getSoundManager().music_volume = 0.1;
				Game.getSoundManager().playMusic("dancepad");
				Game.getSoundManager().playSound(getCurWord().sound);
				updateTexts();
				break;
			}	
		}
		
		protected function updateTexts():void {
			
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_texts[i].text = _word_ref[getCurWord().type];
				
				_texts[i].scaleX = 0;
				_texts[i].scaleY = 0;
				
				if(i < 2) {
					Effect.CreateScaleTo(_texts[i], -1.0, 0.3, Transitions.EASE_OUT_BACK);
				}else {
					Effect.CreateScaleTo(_texts[i], 1.0, 0.3, Transitions.EASE_OUT_BACK);	
				}
			}
		}
		
		protected function completeHandler(e:Event):void {
			
			var mv:MovieClip = e.target as MovieClip;
			
			switch(mv) {
				
				case _starter[STARTER_3]:
					//Game.getSoundManager().playSound("bubble_start");
					_starter[STARTER_3].visible = false;
					_starter[STARTER_3].stop();
					_starter[STARTER_2].visible = true;
					_starter[STARTER_2].play();
					break;
				case _starter[STARTER_2]:
					_starter[STARTER_2].visible = false;
					_starter[STARTER_2].stop();
					_starter[STARTER_1].visible = true;
					_starter[STARTER_1].play();
					break;
				case _starter[STARTER_1]:
					_starter[STARTER_1].visible = false;
					_starter[STARTER_1].stop();
					_starter[STARTER_GO].visible = true;
					_starter[STARTER_GO].play();
					break;
				case _starter[STARTER_GO]:
					
					_starter[STARTER_GO].stop();
					_starter[STARTER_GO].visible = false;
					_boomer.start();
					
					switchGameState(GAME_STATE_UPDATE);		
					break;
			}
		}
		
		protected override function updateGame(elapsedTime:Number):Boolean {
			
			if(isSequenceFinished())
				return false;
			
			if(getCurWord().duration > 0) {
				getCurWord().duration -= elapsedTime;
			} else {
				_word_sequence.shift();	
				
				if(!isSequenceFinished()) {
					
					Game.getSoundManager().playSound(getCurWord().sound);
					for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
						_answered[i] = false;
					}
					
					updateTexts();
				}
			}
			
			return true;
		}
		
		protected override function finishGame():void {
			calculateScore();
			Game.getSoundManager().stopMusic("dancepad");
			Effect.CreateFadeOut(this, 1, "linear", this);
			
			Game.getSoundManager().playMusic("dancepad_end");
		}
		
		protected override function releaseGame():void {
			
			//_background.dispose();
			_boomer.dispose();
			
			for(var i:uint = 0; i < _controllers.length; ++i) {
				_controllers[i].destroy();
			}
		}
		
		public function onControllerTouch(e:Event, c:Controller):void {	
			
			if(getCurState() != GAME_STATE_UPDATE) return;
			
			var controllerIdx:uint = parseInt(c.name);
			if(isSequenceFinished() || _answered[controllerIdx])
				return;
			
			_answered[controllerIdx] = true;
			
			if(getCurWord().type == c.getCurTouch()) {
				++_scores[controllerIdx];
				Game.getSoundManager().playSound("dancepad_good");
				Effect.CreateScaleTo(_texts[controllerIdx], 0.0, 0.2, "linear");
			}
			else {
				Game.getSoundManager().playSound("dancepad_wrong");
				Effect.CreateEarthQuake(_texts[controllerIdx], 0.3);
			}
		}
		
		public function onTweenFinished(t:Tween):void {
			
			if(t == _fade) {
				
				_starter[STARTER_3].visible = true;
				_starter[STARTER_3].play();	
			}
			else {
				Game.getSoundManager().stopMusic("dancepad_end");
				changeSequence("ResultSequence", [Constants.NUM_PLAYER_MAX, _scores]);
			}
		}
		
		public function calculateScore():void {
			
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_scores[i] /= NUM_TONES_MAX;	
			}
		}
	}
}

import applepad.ui.Button;
import applepad.ui.IButton;
import applepad.utils.Vector2;

import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.textures.Texture;
import starling.utils.deg2rad;

internal class Controller extends DisplayObjectContainer implements IButton {
	
	private var _keys:Vector.<uint>;
	private var _arrows:Vector.<Button>;
	private var _cur:uint;
	
	public function Controller(keyMap:Vector.<uint>, text:Vector.<Texture>, face:Texture) {	
		
		_cur = 0;
		
		var size:Quad = new Quad(165, 165);
		size.alpha = 0;
		addChild(size);
		
		_keys = keyMap;
		_arrows = new Vector.<Button>();
		
		var arrowpos:Vector.<Vector2> = new Vector.<Vector2>(keyMap.length);
		arrowpos[0] = new Vector2(0, 0);
		arrowpos[1] = new Vector2(width, 0);
		arrowpos[2] = new Vector2(0, height);
		arrowpos[3] = new Vector2(width, height);
		
		var l:uint = text.length>>1;
		for(var i:uint = 0; i < l; ++i) {
			
			_arrows.push(new Button(text[i*2], text[i*2+1], this));
			
			_arrows[i].rotation = deg2rad(-5);
			_arrows[i].scaleX = 1.5;
			_arrows[i].scaleY = 1.5;
			
			_arrows[i].x = arrowpos[i].x;
			_arrows[i].y = arrowpos[i].y;
			_arrows[i].name = _keys[i].toString();	
			
			addChild(_arrows[i]);
		}		
		
		var f:Image = new Image(face);
		
		f.pivotX = f.width >> 1;
		f.pivotY = f.height >> 1;
		f.scaleY = -1;
		
		f.x = width>>1;
		f.y = height>>1;
		
		addChild(f);
	}
	
	public function getCurTouch():uint { return _cur; }
	
	public function onButton(b:Button):void {
		
		if(b.state == Button.BUTTON_NORMAL) {
			
			_cur = parseInt(b.name);
			dispatchEventWith("TouchCustom", false, this);
		}
	}
	
	public function destroy():void {
		
		_keys.length = 0;
		
		for(var i:uint = 0; i < _arrows.length; ++i) {
			_arrows[i].dispose();
		}
		_arrows.length = 0;	
	}
}


internal class Word {
	
	private var _type:uint;
	private var _duration:int;
	private var _sound:String;
	
	public function Word(type:uint, duration:int, sound:String) {
		
		_type = type;
		_duration = duration;
		_sound = sound;
	}
	
	public function set type(t:uint):void { _type = t; }
	public function get type():uint { return _type; }
	
	public function set duration(d:int):void { _duration = d; }
	public function get duration():int { return _duration; }
	
	public function set sound(s:String):void { _sound = s; }
	public function get sound():String { return _sound; }
}