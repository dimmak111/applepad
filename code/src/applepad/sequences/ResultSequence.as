package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.ITween;
	import applepad.entities.MovieClip;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class ResultSequence extends Sequence implements ITween
	{
		private const TEXTURE_ATLAS_BACK:String 		= "result_back_atlas";
		private const TEXTURE_ATLAS_GAMEOVER:String 	= "result_gameover_atlas";
		private const TEXTURE_ATLAS_RANK:String 		= "result_rank_atlas";
		
		private const PLAYERS_INDEX:uint				= 0;
		private const SCORE_INDEX:uint					= 1;
		
		private var _background:Image;
		
		private var _title:MovieClip;
		private var _result:MovieClip;
		private var _splat:Image;
		
		private var _back:MovieClip;
		
		private var _rank:RankContainer;
		
		private var _retry:Quad;
		
		public function ResultSequence(){
		}
		
		public override function prepare(am:AssetManager):void {
			
			am.loadAtlas("result", TEXTURE_ATLAS_BACK);
			am.loadAtlas("result", TEXTURE_ATLAS_GAMEOVER);
			am.loadAtlas("result", TEXTURE_ATLAS_RANK);
			
			am.loadAtlas("ui", "char_faces_atlas");
		}		

		public override function initialize(am:AssetManager):void {
			
			Game.getSoundManager().playMusic("end");
			
			_retry = new Quad(170, 120);
			_retry.alpha = 0.001;
			
			
			
			var numPlayers:uint = getUserParams()[PLAYERS_INDEX];
			var scores:Vector.<Number> = getUserParams()[SCORE_INDEX];
			trace(scores);
			
			_background = am.getImageFromAtlas("result", TEXTURE_ATLAS_BACK, "background");
			_background.height = Constants.SCREEN_HEIGHT + 5;			
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			
			addChild(_background);
			
			_title = am.getMovieFromAtlas("result", TEXTURE_ATLAS_BACK, "title_", 25, false, false);
			addChild(_title);
			
			_splat = am.getImageFromAtlas("result", TEXTURE_ATLAS_BACK, "splat");
			_splat.y = Constants.SCREEN_HEIGHT;
			addChild(_splat);
			
			_result = am.getMovieFromAtlas("result", TEXTURE_ATLAS_GAMEOVER, "animGO_", 25, false, false);
			_result.scaleX = 1.25;
			_result.scaleY = 1.25;
			_result.x = (_title.width>>2);
			addChild(_result);
			
			_back = am.getMovieFromAtlas("result", TEXTURE_ATLAS_GAMEOVER, "returnhome_", 25, false, false);
			_back.x = _result.x - 60;
			_back.y = Constants.SCREEN_HALF_HEIGHT - 50;
			_back.addEventListener(TouchEvent.TOUCH, onTouch);
			addChild(_back);
			
			
			var chars:Vector.<Image> = new Vector.<Image>(numPlayers);
			for(var i:uint = 0; i < numPlayers; ++i) {
				chars[i] = am.getImageFromAtlas("ui", "char_faces_atlas", "char_0"+Game.getPlayer(i).avatar);
			}
			
			var tokens:Vector.<Image> = new Vector.<Image>(numPlayers);
			for(i = 0; i < numPlayers; ++i) {
				tokens[i] = am.getImageFromAtlas("result", TEXTURE_ATLAS_RANK, "token_0"+i );
			}
			
			var base:MovieClip = am.getMovieFromAtlas("result", TEXTURE_ATLAS_RANK, "rank_", 20, false, false);
			var crown:MovieClip = am.getMovieFromAtlas("result", TEXTURE_ATLAS_RANK, "crown_", 20, false, false);
			
			_rank = new RankContainer(base, chars, scores, crown, tokens);
			
			_rank.x = Constants.SCREEN_WIDTH - _rank.width - 20;
			_rank.y = Constants.SCREEN_HALF_HEIGHT - (_rank.height >> 1) - 60;
			addChild(_rank);
		
			_retry.x = _result.width - _retry.width;
			_retry.y = Constants.SCREEN_HALF_HEIGHT - _retry.height;
			_retry.addEventListener(TouchEvent.TOUCH, onTouch);
			addChild(_retry);
			
			Effect.CreateFadeIn(this, 0.25, "linear", this);
		}
		
		public override function update(elapsedTime:Number):void {
		}
		
		public function onTweenFinished(t:Tween):void {
			
			_title.start();
			_result.start();
			_back.start();
			
			_rank.start();
			
			Effect.CreateMoveTo(_splat, 0, _splat.y-_splat.height, 0.25, Transitions.EASE_OUT);
		}
		
		public function onTouch (e:TouchEvent) : void
		{
			var touch:Touch = e.touches[0];
			if(touch.phase == TouchPhase.ENDED) {
			
				if (e.target == _back) {
					changeSequence("MenuSequence");
				} else if (e.target == _retry){
					changeSequence("SelectSequence");
				}
			}
		}
		
		public override function release():void {
			
			_background.dispose();
			
			_title.destroy();
			_back.destroy();
			_result.destroy();
			
			_rank.destroy();
		}
	}
}

import applepad.AssetEmbed;
import applepad.Constants;
import applepad.effects.Effect;
import applepad.entities.MovieClip;
import applepad.sequences.Sequence;

import feathers.core.TokenList;

import starling.animation.Transitions;
import starling.core.Starling;
import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.events.Event;
import starling.events.EventDispatcher;
import starling.text.TextField;

internal class RankContainer extends DisplayObjectContainer 
{
	private var _base:MovieClip;
	private var _scores:Vector.<RankScore>;
	private var _tokens:Vector.<Image>;
	private var _crown:Vector.<MovieClip>;
	
	public function RankContainer(base:MovieClip, faces:Vector.<Image>, scores:Vector.<Number>, crown:MovieClip, tokens:Vector.<Image>):void {
		
		_base = base;
		_base.x = 75;
		
		addChild(_base);
		
		_tokens = tokens;
		
		var i:uint = 0;
		var offset:int = 0;
		
		for(i = 0; i < scores.length; ++i) {
			
			faces[i].scaleX = 0.8;
			faces[i].scaleY = 0.8;
			
			faces[i].pivotX = 0;
			faces[i].pivotY = faces[i].height;
			
			if(i > 0)
				offset += 130;
				
			faces[i].x = (_base.x+100) + offset;
			faces[i].y = 200;
			_tokens[i].x = faces[i].x - 25;
			_tokens[i].y = faces[i].y - 5;
			
			addChild(faces[i]);
			addChild(_tokens[i]);
			
			_tokens[i].scaleX = 0;
			_tokens[i].scaleY = 0;
		}
		
		
		
		var scoreMax:uint = 0;
		var indiceScore:uint = 0;

		var maxs:Vector.<uint> = new Vector.<uint>();
		
		_scores = new Vector.<RankScore>(scores.length);
		for(i = 0; i < scores.length; ++i) {
			
			var text:TextField = new TextField(120, 100, "0%", AssetEmbed.getFont("grobold").fontName, 35, 0x813514);
			_scores[i] = new RankScore(scores[i] * 100, text);
			if (scoreMax < _scores[i]._max)
			{
				scoreMax = _scores[i]._max;
				maxs.length = 0; 
				maxs.push(i);
			}
			else if(scoreMax ==  _scores[i]._max) {
				maxs.push(i);	
			}
			
			text.x = faces[i].x;
			text.y = 350;
			
			addChild(text);
		}
		
		//crown.x = faces[indiceScore].x - 20;
		//crown.y = faces[indiceScore].y + 50;
		
		_crown = new Vector.<MovieClip>(maxs.length);
		for(i = 0; i < maxs.length; ++i) {
			
			var clone:MovieClip = new MovieClip(crown.textures, 25, true, false);
			clone.x = faces[maxs[i]].x - 20;
			clone.y = faces[maxs[i]].y + 50;
			_crown[i] = clone;
			
			addChild(_crown[i]);
			
		}
		
		addEventListener(Event.ENTER_FRAME, updateScores);
	}
	
	public function start():void {
		
		_base.start();
		
		var i:uint = 0;
		
		for(i = 0; i < _tokens.length; ++i) {
			Effect.CreateScaleTo(_tokens[i], 1.0, 0.3, Transitions.EASE_OUT_BACK);
		}
		
		for(i=0;i<_crown.length;++i) {
			_crown[i].start();
		}
		
	}
	
	private function updateScores():void {
		
		for(var i:uint; i < _scores.length; ++i) {
			_scores[i].update();
		}
	}
	
	public function destroy():void {
		
		_base.dispose();
		
		_crown.length = 0;
		_scores.length = 0;
		_tokens.length = 0;
		
		removeChildren();
	}
}

internal class RankScore extends EventDispatcher
{
	public var _max:uint;
	public var _cur:uint;
	public var _textToUpdate:TextField;

	public function RankScore(max:uint, text:TextField) {
		
		_max = max;
		_cur = 0;
		
		_textToUpdate = text;
	}
	
	public function update() : void {
		
		if (_cur < _max) {
			_cur++;
			_textToUpdate.text = _cur.toString() + "%";
		} else  {
			dispatchEventWith(Event.COMPLETE);
		}
	}
}