
package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.audio.SoundManager;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.FadeOut;
	import applepad.effects.IFadeIn;
	import applepad.effects.IFadeOut;
	import applepad.entities.MovieClip;
	import applepad.sequences.Sequence;
	
	import flash.errors.IOError;
	import flash.events.AsyncErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.events.StageVideoEvent;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	
	
	public class MenuSequence extends Sequence implements IFadeIn, IFadeOut
	{	
		//const
		private const TITLE_SCREEN_VIDEO:String 	= "app:/menu/titlescreen_2.flv";
		private const TITLE_ATLAS:String 			= "titlescreen_atlas";
		
		private const STATE_NEWGAME:uint			= 0;
		private const STATE_OPTIONS:uint			= 1;
		private const STATE_TROPHEE:uint			= 2;
		
		//datas
		private var _intro:Video;
		private var nc:NetConnection;
		private var ns:NetStream;
		
		private var _introStatic:Image;
		private var _book:Image;
		private var _touch:MovieClip;
		
		private var _tweenBook:Tween;
		private var _tweenPlay:Tween;
		private var _tweenTitle:Tween;
		
		private var _arrowRight:Image;
		private var _arrowLeft:Image;
		private var _playIcon:Image;
		private var _optionIcon:Image;
		private var _tropheeIcon:Image;
		private var _playTitle:Image;
		private var _optionTitle:Image;
		private var _tropheeTitle:Image;
		
		private var _isIntroFinished:Boolean;
		private var _isTouched:Boolean;
		
		private var _curState:Number = STATE_NEWGAME;
		
		public function MenuSequence():void {

			nc = new NetConnection();
			nc.connect(null);
			ns = new NetStream(nc);
			ns.client = this;
		}
		
		override public function prepare(am:AssetManager):void {		
			
			am.loadAtlas("menu", TITLE_ATLAS);
		}
		
		override public function initialize(am:AssetManager):void {
			
			_intro = new Video(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			Starling.current.nativeOverlay.addChild(_intro);
			_intro.attachNetStream(ns);
			
			ns.play(TITLE_SCREEN_VIDEO);
			
			Game.getSoundManager().playMusic("menu");
			
			_introStatic = am.getImageFromAtlas("menu", TITLE_ATLAS, "touch_screen");
			_introStatic.visible = false;
			_introStatic.height = Constants.SCREEN_HEIGHT;
			
			_isIntroFinished = false;
			_isTouched = false;
			
			_touch = am.getMovieFromAtlas("menu", TITLE_ATLAS, "touchscreen_", 20, true);
			_touch.visible = false;
			
			_touch.pivotX = _touch.width >> 1;
			_touch.pivotY = _touch.height >> 1;
			
			_touch.x = Constants.SCREEN_HALF_WIDTH;
			_touch.y = Constants.SCREEN_HEIGHT - 100;
			
			_book = am.getImageFromAtlas("menu", TITLE_ATLAS, "book");
			_book.touchable = false;
			_book.x = 25;
			
			_book.y = Constants.SCREEN_HEIGHT;
			
						
			_arrowLeft = am.getImageFromAtlas("menu", TITLE_ATLAS, "arrow");
			_arrowLeft.scaleX = -1.5;
			_arrowLeft.scaleY = 1.5;
			_arrowLeft.x = 50;
			_arrowLeft.y = Constants.SCREEN_HEIGHT - 170;
			_arrowLeft.visible = false;
			_arrowLeft.addEventListener(TouchEvent.TOUCH, onTouchArrow);
			
			_arrowRight = am.getImageFromAtlas("menu", TITLE_ATLAS, "arrow");
			_arrowRight.x = _book.width;
			_arrowRight.y = _arrowLeft.y;
			_arrowRight.scaleX = 1.5;
			_arrowRight.scaleY = 1.5;
			_arrowRight.visible = false;
			_arrowRight.addEventListener(TouchEvent.TOUCH, onTouchArrow);
			
			_playIcon = am.getImageFromAtlas("menu", TITLE_ATLAS, "Play");
			_playIcon.x = _book.x + 210;
			_playIcon.y = _book.y - 130;
			_playIcon.pivotX += _playIcon.width >> 1;
			_playIcon.pivotY = _playIcon.height;
			_playIcon.scaleX = 0;
			_playIcon.scaleY = 0;
			_playIcon.visible = false;
			_playIcon.addEventListener(TouchEvent.TOUCH, onIconTouched);
			 
			_optionIcon = am.getImageFromAtlas("menu", TITLE_ATLAS, "option");
			_optionIcon.x = _playIcon.x;
			_optionIcon.y = _playIcon.y;
			_optionIcon.pivotX += _optionIcon.width >> 1;
			_optionIcon.pivotY = _optionIcon.height;
			_optionIcon.scaleX = 0;
			_optionIcon.scaleY = 0;
			_optionIcon.visible = false;
			_optionIcon.addEventListener(TouchEvent.TOUCH, onIconTouched);
			 
			_tropheeIcon = am.getImageFromAtlas("menu", TITLE_ATLAS, "trophees");
			_tropheeIcon.x = _playIcon.x;
			_tropheeIcon.y = _playIcon.y;
			_tropheeIcon.pivotX += _tropheeIcon.width >> 1;
			_tropheeIcon.pivotY = _tropheeIcon.height;
			_tropheeIcon.scaleX = 0;
			_tropheeIcon.scaleY = 0;
			_tropheeIcon.visible = false;
			_tropheeIcon.addEventListener(TouchEvent.TOUCH, onIconTouched);
			
			_playTitle = am.getImageFromAtlas("menu", TITLE_ATLAS, "new_game");
			_playTitle.x = _playIcon.x + 300;
			_playTitle.y = _playIcon.y - 50;
			_playTitle.alpha = 0.001;
			_playTitle.touchable = false;
			_playTitle.addEventListener(TouchEvent.TOUCH, onIconTouched);
			
			_optionTitle = am.getImageFromAtlas("menu", TITLE_ATLAS, "options");
			_optionTitle.x =  _playTitle.x + 10;
			_optionTitle.y =  _playTitle.y - 10;
			_optionTitle.alpha = 0.001;
			_optionTitle.touchable = false
			_optionTitle.addEventListener(TouchEvent.TOUCH, onIconTouched);
			
			_tropheeTitle = am.getImageFromAtlas("menu", TITLE_ATLAS, "trophee");
			_tropheeTitle.x =  _playTitle.x + 15;
			_tropheeTitle.y =  _playTitle.y;
			_tropheeTitle.alpha = 0.001;
			_tropheeTitle.touchable = false
			_tropheeTitle.addEventListener(TouchEvent.TOUCH, onIconTouched);
			_curState = STATE_NEWGAME;
			
			_tweenPlay = new Tween(null, 0);
			Starling.juggler.add(_tweenPlay);
			
			_tweenTitle = new Tween(null,0);
			Starling.juggler.add(_tweenTitle);
			
			addChild(_introStatic);
			addChild(_touch);
			addChild(_book);
			addChild(_arrowRight);
			addChild(_arrowLeft);
			addChild(_playIcon);
			addChild(_optionIcon);
			addChild(_tropheeIcon);
			addChild(_playTitle);
			addChild(_optionTitle);
			addChild(_tropheeTitle);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		override public function update(elapsedTime:Number):void {		
			
		}
		
		public function onMetaData(e:Object):void {
			
		}
		
		public function onPlayStatus(e:Object):void {
			
			if (e.code == "NetStream.Play.Complete") {
				
				onIntroFinished();
				_isIntroFinished = true;
				doFadeIn(_touch, 500);
			}
		}
		
		public function onXMPData(e:Object):void {
			
		}
		
		public function onIntroFinished():void {
			Starling.current.nativeOverlay.removeChild(_intro);
			_introStatic.visible = true;
			_touch.visible = true;
		}
		
		public function onButtonTouch(e:Event):void {
		}
		
		public function onFadeInFinished(e:FadeIn):void {
		}
		
		public function onFadeOutFinished(e:FadeOut):void {
			changeSequence("CharSelectSequence");
		}
		
		public function onTouch(e:TouchEvent):void {			
			
			var touch:Touch = e.getTouch(this);
			
			if(touch == null) return;
			if(touch.phase == TouchPhase.BEGAN) {
			
				if(_isIntroFinished && !_isTouched) {
					_isTouched = true;
					
					Effect.CreateScaleTo(_touch, 0, 0.25, "linear");
					
					//_tweenPlay.reset(_touch, 1, Transitions.EASE_IN);
					//_tweenPlay.scaleTo(0);
					//_touch.pivotX = _touch.width >> 1
					//_touch.pivotY = _touch.height >> 1
					//Starling.juggler.add(_tweenPlay);
					//_touch.visible = false;
					Starling.juggler.remove(_touch);
					
					_tweenBook = new Tween(_book, 1.0, Transitions.LINEAR);
					_tweenBook.moveTo(20,Constants.SCREEN_HEIGHT - _book.height - 20);
					_tweenBook.onComplete =  completedHandler;
					Starling.juggler.add(_tweenBook);
					
					_playTitle.touchable = true;
					_optionTitle.touchable = true;
					_tropheeTitle.touchable = true;
					
				}
			}
		}		

		public function completedHandler (): void
		{
			_arrowRight.visible = true;
			_arrowLeft.visible = true;
			_playIcon.visible = true;
			_playTitle.visible = true;
			tweenPlay(_playIcon, _playTitle);
			
		}
		
		
		public function tweenPlay (object:Image, title:Image) : void {

			object.scaleX = 0;
			object.scaleY = 0;
			_tweenPlay.reset(object, 0.4, Transitions.EASE_OUT_BACK);
			_tweenPlay.scaleTo(1.0);
			Starling.juggler.add(_tweenPlay);
			
			title.alpha = 0.001;
			_tweenTitle.reset(title, 0.4, Transitions.LINEAR);
			_tweenTitle.fadeTo(Constants.ALPHA_MAX);
			Starling.juggler.add(_tweenTitle);
			
		}
		
		
		public function changeState() :void {
				
			if(_curState >= 3){
				_curState = STATE_NEWGAME;
			}
			else if (_curState <= -1){
				_curState = STATE_TROPHEE;
			}
			
			switch(_curState) 
			{	
				case STATE_NEWGAME:
					_playIcon.visible = true;
					_optionIcon.visible = false;
					_tropheeIcon.visible = false;
					_playTitle.visible = true;
					_optionTitle.visible = false;
					_tropheeTitle.visible = false;
					tweenPlay(_playIcon, _playTitle);
					break;
				
				case STATE_OPTIONS:
					_playIcon.visible = false;
					_optionIcon.visible = true;
					_tropheeIcon.visible = false;
					_playTitle.visible = false;
					_optionTitle.visible = true;
					_tropheeTitle.visible = false;
					tweenPlay(_optionIcon, _optionTitle);
					break;
				
				case STATE_TROPHEE:
					_playIcon.visible = false;
					_optionIcon.visible = false;
					_tropheeIcon.visible = true;
					_playTitle.visible = false;
					_optionTitle.visible = false;
					_tropheeTitle.visible = true;
					tweenPlay(_tropheeIcon, _tropheeTitle);
					break;

			}
		}
		
		public function onIconTouched (e:TouchEvent) : void {
			
			var touch:Touch = e.getTouch(this);
			
			if(touch == null) return;
			if(touch.phase == TouchPhase.BEGAN) {
				switch(_curState)
				{
					case STATE_NEWGAME:
					{
						changeSequence("CharSelectSequence");
					}
					break;				
					case STATE_OPTIONS:
					{
						changeSequence("OptionSequence");
					}
					break;
					case STATE_TROPHEE:
					{
						changeSequence("AchievementSequence");
					}
					break;
				}	
			}
		}
		
		public function onTouchArrow (e:TouchEvent) : void {
			
			var touch:Touch = e.getTouch(this);
			
			if(touch == null) return;
			if(touch.phase == TouchPhase.BEGAN) {
				switch(e.target)
				{
					case _arrowLeft:
					{
						_curState--;
						changeState();
					}
					break;				
					case _arrowRight:
					{
						_curState++;
						changeState();
					}
					break;
				}
			}
		}
		
		override public function release():void {
			
			ns.dispose();
			
			_introStatic.dispose();
			_book.dispose();
			_arrowRight.dispose();
			_arrowLeft.dispose();
			_playIcon.dispose();
			_optionIcon.dispose();
			_tropheeIcon.dispose();
			_playTitle.dispose();
			_optionTitle.dispose();
			_tropheeTitle.dispose();
			
			_touch.destroy();
		}
		
	}
}