package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class LoginSequence extends Sequence implements IFadeIn
	{	
		private var _fontXml:XML;	
		private var _font:BitmapFont;	
		private var _loginForm:LoginForm;
		
		private var _background:Image;
		private var _bulle:MovieClip;
		private var _login:MovieClip;
		private var _nickname:MovieClip;
		private var _password:MovieClip;
		private var _barreNickname:MovieClip;
		private var _barrePassword:MovieClip;
		private var _splash1:MovieClip;
		private var _splash2:MovieClip;
		private var _ok:MovieClip;
		
		
		private var OFFSET_SP1_X:uint = 70;
		private var OFFSET_SP1_Y:uint = 80;		
		private var OFFSET_SP2_X:uint = 600;
		private var OFFSET_SP2_Y:uint = 90;
		private var OFFSET_BULLE:uint = 25;
		
		private var OFFSET_LOG_X:uint = 20;
		private var OFFSET_LOG_Y:uint = 60;
		private var OFFSET_NICK_X:uint = 150;
		private var OFFSET_NICK_Y:uint = 240;
		private var OFFSET_PASS_X:uint = 160;
		private var OFFSET_PASS_Y:uint = 290;
		private var OFFSET_NICKBARRE_X:uint = 80;
		private var OFFSET_NICKBARRE_Y:uint = 5;
		
		private var OFFSET_PASSBARRE_Y:uint = 55;
		
		private var OFFSET_OK_X:uint = 85;
		private var OFFSET_OK_Y:uint = 250;
		
		
		override public function prepare(_assetManager:AssetManager):void {		
			
					
			_assetManager.loadAtlas("login", "login_01");
			_assetManager.loadAtlas("login", "login_02");
			_assetManager.loadAtlas("ui", "ui_atlas");
			_assetManager.loadTexture("login", "BG");

		}			
		
		override public function initialize(_assetManager:AssetManager):void {			
			
			_background = new Image(_assetManager.getTexture("login", "BG"));
			_background.height = Constants.SCREEN_HEIGHT;
			
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			
			addChild(_background);
			
			
			_splash1 = _assetManager.getMovieFromAtlas("login", "login_02", "splat_01_", 25); 
			_splash1.addEventListener(Event.COMPLETE, completedHandler);
			_splash1.loop = false;
			_splash1.touchable = false;
			_splash1.x += OFFSET_SP1_X;
			_splash1.y += OFFSET_SP1_Y;
			_splash1.visible = false;
			
			addChild(_splash1);	
			
			_splash2 = _assetManager.getMovieFromAtlas("login", "login_02", "splat_02_", 25); 
			_splash2.addEventListener(Event.COMPLETE, completedHandler);
			_splash2.loop = false;
			_splash2.touchable = false;
			_splash2.x = OFFSET_SP2_X;
			_splash2.y = OFFSET_SP2_Y; 
			_splash2.visible = false;
			
			addChild(_splash2);	
			
						
			_bulle = _assetManager.getMovieFromAtlas("login", "login_01", "bulle_", 25);
			_bulle.addEventListener(Event.COMPLETE, completedHandler);
			_bulle.loop = false;
			_bulle.touchable = false;
			_bulle.x = Constants.SCREEN_HALF_WIDTH - (_bulle.width >> 1) - OFFSET_BULLE;
			_bulle.y = Constants.SCREEN_HALF_HEIGHT - (_bulle.height >> 1) - OFFSET_BULLE;
			addChild(_bulle);

			_login = _assetManager.getMovieFromAtlas("login", "login_01", "login_", 25); 
			_login.addEventListener(Event.COMPLETE, completedHandler);
			_login.loop = false;
			_login.touchable = false;
			_login.x = (_bulle.width >> 1) + OFFSET_LOG_X;
			_login.y = _bulle.y + OFFSET_LOG_Y;
			_login.visible = false;
			
			addChild(_login);		
			
			_nickname = _assetManager.getMovieFromAtlas("login", "login_02", "nickname_", 25); 
			_nickname.addEventListener(Event.COMPLETE, completedHandler);
			_nickname.loop = false;
			_nickname.touchable = false;
			_nickname.x = _bulle.x + OFFSET_NICK_X;
			_nickname.y = _bulle.y + OFFSET_NICK_Y;
			_nickname.visible = false;
			
			addChild(_nickname);
			
			
			_password = _assetManager.getMovieFromAtlas("login", "login_02", "password_", 25); 
			_password.addEventListener(Event.COMPLETE, completedHandler);
			_password.loop = false;
			_password.touchable = false;
			_password.x = _bulle.x + OFFSET_PASS_X;
			_password.y = _bulle.y + OFFSET_PASS_Y;
			_password.visible = false;
			
			addChild(_password);			
			
			
			_barreNickname = _assetManager.getMovieFromAtlas("login", "login_01", "barre_", 25); 
			_barreNickname.addEventListener(Event.COMPLETE, completedHandler);
			_barreNickname.loop = false;
			_barreNickname.touchable = false;
			_barreNickname.x =_nickname.x + OFFSET_NICKBARRE_X;
			_barreNickname.y = _nickname.y - _nickname.height - OFFSET_NICKBARRE_Y;
			_barreNickname.visible = false;
			
			addChild(_barreNickname);	
			
			_barrePassword = _assetManager.getMovieFromAtlas("login", "login_01", "barre_", 25); 
			_barrePassword.addEventListener(Event.COMPLETE, completedHandler);
			_barrePassword.loop = false;
			_barrePassword.touchable = false;
			_barrePassword.x =_barreNickname.x;
			_barrePassword.y = _barreNickname.y + OFFSET_PASSBARRE_Y;
			_barrePassword.visible = false;
			
			addChild(_barrePassword);	
			
			
			
			_ok = _assetManager.getMovieFromAtlas("login", "login_01", "OK_", 25); 
			_ok.addEventListener(Event.COMPLETE, completedHandler);
			_ok.loop = false;
			_ok.touchable = false;
			_ok.x = _bulle.width - OFFSET_OK_X;
			_ok.y = _bulle.height - OFFSET_OK_Y;
			_ok.visible = false;
			
			addChild(_ok);	
			
			
			
			var imageButtonDefault:Image = new Image(_assetManager.getTextureFromAtlas("ui", "ui_atlas", "ok"));
			var imageButtonDown:Image  = new Image(_assetManager.getTextureFromAtlas("ui", "ui_atlas", "ok_hover"));
			
			var imageBackground:Image  = new Image(_assetManager.getTextureFromAtlas("ui", "ui_atlas", "textInput"));
			var imageBackgroundDisabled:Image  = new Image(_assetManager.getTextureFromAtlas("ui", "ui_atlas", "textInput"));
			
			_loginForm = new LoginForm(imageButtonDefault, imageButtonDown,imageBackground, imageBackgroundDisabled,  _font);
			_loginForm.addEventListener(Event.COMPLETE, onPasswordOk);
			addChild(_loginForm);
			_loginForm.visible = false;			
			
			
			doFadeIn(this, 1000, this);
			
		}
		
		public function completedHandler (e:Event) :void {
			
			Starling.juggler.remove(e.target as MovieClip);
			
			switch(e.target) {
				case _bulle:
				{
					//_backButton.visible = true;
					_login.visible = true;
					Starling.juggler.add(_login);
					
					_nickname.visible = true;
					Starling.juggler.add(_nickname);
					
					_password.visible = true;
					Starling.juggler.add(_password);
					
					_barreNickname.visible = true;
					Starling.juggler.add(_barreNickname);
					
					_barrePassword.visible = true;
					Starling.juggler.add(_barrePassword);
					
					_splash1.visible = true;
					Starling.juggler.add(_splash1);
					
					_splash2.visible = true;
					Starling.juggler.add(_splash2);
					
					_ok.visible = true;
					Starling.juggler.add(_ok);
				}
				break;
				case _ok:
				{
					_loginForm.visible = true;
				}
				break;
				
			}
		}
		
		public override function release():void {
			_loginForm.destroy();
		}
		
		public function onPasswordOk () : void {
			changeSequence("MenuSequence");
		}
		
		public function onFadeInFinished(e:FadeIn):void {
			Starling.juggler.add(_bulle);
			
		}
	}
}


import applepad.AssetEmbed;
import applepad.AssetManager;
import applepad.Constants;

import feathers.controls.Button;
import feathers.controls.TextInput;
import feathers.text.BitmapFontTextFormat;

import flash.text.Font;
import flash.text.TextFormat;
import flash.utils.flash_proxy;

import org.osmf.layout.HorizontalAlign;
import org.osmf.layout.VerticalAlign;

import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.BitmapFont;
import starling.text.TextField;

internal class LoginForm extends Sprite
{
	private var _logInput:TextInput;
	private var _passInput:TextInput;
	protected var _loginButton:Button;
	private var myQuad:Quad;
	private var _myFont:Font;
	
	public function LoginForm (imageButtonDefault:Image, imageButtonDown:Image, imageBackground:Image, imageBackgroundDisabled:Image, font:BitmapFont)
	{	
		myQuad = new Quad(Constants.SCREEN_HALF_WIDTH, Constants.SCREEN_HALF_HEIGHT);
		myQuad.visible = false;
		addChild(myQuad);
		
		//_myFont = AssetEmbed.getFont("toonish");	

		_logInput = new TextInput();
		_passInput = new TextInput();
		_loginButton = new Button();
		/*
		var textField:TextField = new TextField(600, 100, "Test de la font 1-2, 1-2 ?!", _myFont.fontName, 25, 0xFFFFFF);
		addChild(textField);
		textField.x = 0;
		textField.y = 0;*/
		
		addChild(_logInput);
		addChild(_passInput);
		addChild(_loginButton);
		
		initLogInput(imageBackground,imageBackgroundDisabled );
		initPassInput(imageBackground,imageBackgroundDisabled );
		initLogButton(imageButtonDefault, imageButtonDown, font);	
		
		x = Constants.SCREEN_HALF_WIDTH - (width >> 1);
		y = Constants.SCREEN_HALF_HEIGHT - (height >> 1);
	}
	
	public function initLogButton (imageButtonDefault:Image, imageButtonDown:Image, font:BitmapFont) :void {
		
		_loginButton.defaultSkin = imageButtonDefault;
		_loginButton.downSkin = imageButtonDown;		
		_loginButton.addEventListener(Event.TRIGGERED, onButtonTouch);

		_loginButton.validate();		

		_loginButton.x = myQuad.width - 184;
		_loginButton.y = myQuad.height - 231;

	}
	
	public function initLogInput (imageBackground:Image, imageBackgroundDisabled:Image) :void {
		
		_logInput.backgroundSkin =  imageBackground;
		_logInput.backgroundDisabledSkin = imageBackgroundDisabled;
		_logInput.validate();		
		
						
		_logInput.paddingLeft = 10;
		_logInput.paddingTop = (_logInput.backgroundSkin.height - 12) >> 1;
		_logInput.x = 166;
		_logInput.y = 113;
		//trace (_myFont);
		//trace (_myFont.fontName);
		//_logInput.stageTextProperties.fontFamily  = _myFont.fontName;
		_logInput.stageTextProperties.color = "0xFFFFFF";
		_logInput.stageTextProperties.fontSize = 16;
		

	}
	
	public function initPassInput (imageBackground:Image, imageBackgroundDisabled:Image) :void {
	
		_passInput.backgroundSkin = imageBackground;
		_passInput.backgroundDisabledSkin = imageBackgroundDisabled;

		_passInput.stageTextProperties.displayAsPassword = true;
		
		_passInput.validate();
		_passInput.paddingLeft = 10;
		_passInput.paddingTop = (_logInput.backgroundSkin.height - 12) >> 1;

		_passInput.x = 166;
		_passInput.y = _logInput.y + 50;	
//		_passInput.stageTextProperties.fontFamily  = _myFont.fontName;
		_passInput.stageTextProperties.color = "0xFFFFFF";
		_passInput.stageTextProperties.fontSize = 16;
	}
	
	public function get logInput () : TextInput {
		return _logInput;
	}
	
	public function get passInput () : TextInput {
		return _passInput;
	}
	
	public function get loginButton () : Button {
		return _loginButton;
	}
	
	public function onButtonTouch () : void {
		
		if (testLogin()) {
			dispatchEventWith(Event.COMPLETE);
		}
	}
	
	public function testLogin (): Boolean {
		
		var log:String = logInput.text;
		var pass:String = passInput.text;
		
		// TODO check if it exist in the database
		return ((log == "admin") && pass == "admin");
	}
	
	public function destroy():void {
		
		_logInput.dispose();
		_passInput.dispose();
		_loginButton.dispose();
		
		myQuad.dispose();
	}
}