package applepad.sequences
{

	import applepad.AssetEmbed;
	import applepad.AssetManager;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.FadeOut;
	import applepad.effects.IFadeIn;
	import applepad.effects.IFadeOut;
	import applepad.entities.MovieClip;
	import applepad.utils.MathUtils;
	import applepad.utils.Vector2;
	
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.Font;
	
	import flashx.textLayout.formats.Direction;
	
	import starling.animation.Juggler;
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.deg2rad;

		

	public class MiniGameSnow extends MiniGame implements IFadeIn,IFadeOut
	{
		private const TEXT_COLOR:uint = 0x4842FF;
		
		private var _Score_Player1:uint;
		private var _Score_Player2:uint;
		private var _scores:Vector.<Number>;

		private var _nb_tours:uint; 
		private var _Sentences_Player1:Array;
		private var _Sentences_Player2:Array;
		private var _Player1_qa:Vector.<String>;
		private var _Player2_qa:Vector.<String>;
		private var _Player1_qa_bool:Vector.<Boolean>;
		private var _Player2_qa_bool:Vector.<Boolean>;
		private var _turn:Boolean; // true => player1 false => player2
		private var _turn1:Boolean;
		private var bonus_update:Boolean=false;
		private var restart_game:Boolean=false;
		
		private var _game:XML;
		private var Fight:MovieClip;
		private var myFont:Font = AssetEmbed.getFont("grobold");
		
		private var legend1:TextField;
		private var legend2:TextField;
		private var legend3:TextField;
		private var legend4:TextField;
		
		private var player1:TextField;
		private var player2:TextField;
		private var player1_name:String = "player1";
		private var player2_name:String = "player2";
		
		private var zone_avatar1_Image:Image;
		private var zone_avatar1_bonus:Player_bonus;
		private var avatar1_animation:MovieClip;
		
		private var zone_avatar2_Image:Image;
		private var zone_avatar2_bonus:Player_bonus;
		private var avatar2_animation:MovieClip;
		
		private var zone_avatar1_shadow:Image;
		private var avatar1_shadow_animation:MovieClip;
		private var zone_avatar2_shadow:Image;
		private var avatar2_shadow_animation:MovieClip;
		
		private var snowman1_Image:Image;
		private var snowman2_Image:Image;
		
		private var Avatar_Hud1_Image:Image;
		//private var Avatar_Hud1_animation:MovieClip;
		private var Avatar_Hud2_Image:Image;
		//private var Avatar_Hud2_animation:MovieClip;
		private var Ice_Hud1_Image:Image;
		private var Ice_Hud1_animation:MovieClip;
		private var Ice_Hud2_Image:Image;
		private var Ice_Hud2_animation:MovieClip;
		
		private var bonhomme0_Image:Image;
		private var bonhomme1_Image:Image;
		private var bonhomme2_Image:Image;
		private var bonhomme3_Image:Image;
		private var bonhomme4_Image:Image;
		private var bonhomme5_Image:Image;
		private var bonhomme6_Image:Image;
		
		private var canon1_Image:Image;
		private var canon1_animation:MovieClip;
		private var canon2_Image:Image;
		private var canon2_animation:MovieClip;
		
		private var wallpaper_Image:Image;
		
		private var throw_snowball1_animation:MovieClip;
		private var throw_snowball2_animation:MovieClip;
		
		private var snowball_explosion:MovieClip;
		
		private var snowball1_Image:Image;	
		private var snowball2_Image:Image;
		private var snowball3_Image:Image;
		private var snowball4_Image:Image;
		
		private var _selected:TextField;
		
		
		public function MiniGameSnow() {
		}
		
		
		public function drawZonePlayer (assetManager:AssetManager) : void {
			
			_Score_Player1=0;
			_Score_Player2=0;
			_nb_tours=0;  
			_turn=true;
			_turn1=false;
			_scores = new Vector.<Number>(2);
			var NB_SENTENCE_MAX:uint=8;
			_Player1_qa = new Vector.<String>(NB_SENTENCE_MAX);
			_Player2_qa = new Vector.<String>(NB_SENTENCE_MAX);
			_Player1_qa_bool = new Vector.<Boolean>(NB_SENTENCE_MAX);
			_Player2_qa_bool = new Vector.<Boolean>(NB_SENTENCE_MAX);
			 
			wallpaper_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight","snowfight_assets", "BG"));
			wallpaper_Image.touchable = false;
			wallpaper_Image.blendMode = BlendMode.NONE;
			addChild (wallpaper_Image);

			zone_avatar1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "character_00013"));
			zone_avatar1_Image.x = (stage.stageWidth>>1)-(zone_avatar1_Image.width>>1);
			zone_avatar1_Image.y = 0;
			//addChild (zone_avatar1_Image);
			zone_avatar1_bonus = new Player_bonus((stage.stageWidth>>1)-(zone_avatar1_Image.width>>1),0,true);
			zone_avatar1_bonus.setSprite(new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "character_00013")));
			zone_avatar1_bonus.initPosX=(stage.stageWidth>>1)-(zone_avatar1_Image.width>>1);
			zone_avatar1_bonus.initPosY=0;

			
			zone_avatar1_shadow = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "shadow_00014"));
			zone_avatar1_shadow.x = (stage.stageWidth>>1)-(zone_avatar1_Image.width>>1);
			zone_avatar1_shadow.y = zone_avatar1_Image.width>>2;
			zone_avatar1_shadow.touchable = false;
			avatar1_shadow_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "shadow_", 25,false,false); 
			avatar1_shadow_animation.x=zone_avatar1_shadow.x;
			avatar1_shadow_animation.y=zone_avatar1_shadow.y;
			avatar1_shadow_animation.addEventListener(Event.COMPLETE, completedHandler);
			avatar1_shadow_animation.loop = false;
			avatar1_shadow_animation.touchable = false;
			avatar1_shadow_animation.visible = false;
			addChild(avatar1_shadow_animation);
			addChild(zone_avatar1_shadow);
			addChild (zone_avatar1_bonus);
			
			avatar1_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "character_", 25,false,false); 
			avatar1_animation.x=zone_avatar1_Image.x;
			avatar1_animation.y=zone_avatar1_Image.y;
			avatar1_animation.addEventListener(Event.COMPLETE, completedHandler);
			avatar1_animation.loop = false;
			avatar1_animation.touchable = false;
			avatar1_animation.visible = false;
			addChild(avatar1_animation);
			
			zone_avatar2_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "character_00013"));
			zone_avatar2_Image.x = (stage.stageWidth>>1)-(zone_avatar2_Image.width>>1)+(zone_avatar2_Image.width>>2)+(zone_avatar2_Image.width>>2);
			zone_avatar2_Image.y =  stage.stageHeight+(zone_avatar2_Image.height>>3);
			//addChild (zone_avatar2_Image);
			zone_avatar2_bonus = new Player_bonus((stage.stageWidth>>1)-(zone_avatar2_Image.width>>1)+(zone_avatar2_Image.width>>2)+(zone_avatar2_Image.width>>2),stage.stageHeight+(zone_avatar2_Image.height>>3),false);
			zone_avatar2_bonus.setSprite(new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "character_00013")));
			zone_avatar2_bonus.initPosX=(stage.stageWidth>>1)-(zone_avatar2_Image.width>>1)+(zone_avatar2_Image.width>>2)+(zone_avatar2_Image.width>>2);
			zone_avatar2_bonus.initPosY=stage.stageHeight+(zone_avatar2_Image.height>>3);
			zone_avatar2_bonus.scaleY=-1;
			
			
			canon1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "canon_00014"));
			canon1_Image.x = zone_avatar1_Image.x+canon1_Image.width-(zone_avatar1_Image.width>>2);
			canon1_Image.y = canon1_Image.height>>2;
			canon1_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "canon_", 25,false,false);
			canon1_animation.x = canon1_Image.x ;
			canon1_animation.y = canon1_Image.y ;
			canon1_animation.addEventListener(Event.COMPLETE, completedHandler);
			canon1_animation.loop = false;
			canon1_animation.touchable = false;
			canon1_animation.visible = false;
			addChild(canon1_animation);
			addChild (canon1_Image);

			throw_snowball1_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "animSnow_", 25,false,false); 
			throw_snowball1_animation.x=zone_avatar1_Image.x;
			throw_snowball1_animation.y=zone_avatar1_Image.y-(zone_avatar1_Image.height);
			throw_snowball1_animation.scaleY=2;
			throw_snowball1_animation.addEventListener(Event.COMPLETE, completedHandler);
			throw_snowball1_animation.loop = false;
			throw_snowball1_animation.touchable = false;
			throw_snowball1_animation.visible = false;
			addChild(throw_snowball1_animation);
			
			canon2_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "canon_00014"));
			canon2_Image.x = zone_avatar2_Image.x-zone_avatar2_Image.width+(zone_avatar2_Image.width)-(zone_avatar2_Image.width>>1);
			canon2_Image.y = stage.stageHeight-(canon2_Image.height>>3);
			canon2_Image.scaleY=-1;
			canon2_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "canon_", 25,false,false);
			canon2_animation.x = canon2_Image.x ;
			canon2_animation.y = canon2_Image.y ;
			canon2_animation.scaleY=-1;
			canon2_animation.addEventListener(Event.COMPLETE, completedHandler);
			canon2_animation.loop = false;
			canon2_animation.touchable = false;
			canon2_animation.visible = false;
			
			zone_avatar2_shadow = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets", "shadow_00014"));
			zone_avatar2_shadow.x = canon2_Image.x-(zone_avatar2_shadow.width>>5);
			zone_avatar2_shadow.y = stage.stageHeight-zone_avatar2_shadow.height-(zone_avatar2_shadow.height>>3);
			zone_avatar2_shadow.touchable = false;
			avatar2_shadow_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "shadow_", 25,false,false); 
			avatar2_shadow_animation.x=zone_avatar2_shadow.x;
			avatar2_shadow_animation.y=zone_avatar2_shadow.y;
			avatar2_shadow_animation.addEventListener(Event.COMPLETE, completedHandler);
			avatar2_shadow_animation.loop = false;
			avatar2_shadow_animation.touchable = false;
			avatar2_shadow_animation.visible = false;
			addChild(avatar2_shadow_animation);
			addChild(zone_avatar2_shadow);
			
			avatar2_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "character_", 25); 
			avatar2_animation.x=zone_avatar2_Image.x;
			avatar2_animation.y=zone_avatar2_Image.y;
			avatar2_animation.scaleY = -1;
			avatar2_animation.addEventListener(Event.COMPLETE, completedHandler);
			avatar2_animation.loop = false;
			avatar2_animation.touchable = false;
			avatar2_animation.visible = false;
			addChild(avatar2_animation);
			addChild(canon2_animation);
			addChild (canon2_Image);
			addChild (zone_avatar2_bonus);
			
			throw_snowball2_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "animSnow_", 25,false,false); 
			throw_snowball2_animation.x=zone_avatar2_Image.x;
			throw_snowball2_animation.y=zone_avatar2_Image.y+(zone_avatar2_Image.height);
			throw_snowball2_animation.scaleY=-2;
			throw_snowball2_animation.addEventListener(Event.COMPLETE, completedHandler);
			throw_snowball2_animation.loop = false;
			throw_snowball2_animation.touchable = false;
			throw_snowball2_animation.visible = false;
			addChild(throw_snowball2_animation);
			
			snowman1_Image = new Image(assetManager.getTexture("minigame_snowfight", "snowman"));
			snowman1_Image.x = stage.stageWidth-(snowman1_Image.width<<1);
			snowman1_Image.y =snowman1_Image.height;
			snowman1_Image.scaleY=-1;
			snowman1_Image.touchable = false;
			snowman1_Image.visible=false;
			addChild (snowman1_Image);
			
			snowman2_Image = new Image(assetManager.getTexture("minigame_snowfight", "snowman"));
			snowman2_Image.x = snowman2_Image.width;
			snowman2_Image.y = stage.stageHeight-snowman2_Image.height;
			snowman2_Image.touchable = false;
			snowman2_Image.visible=false;
			addChild (snowman2_Image);
			
			Avatar_Hud1_Image = new Image(assetManager.getTextureFromAtlas("ui", "char_faces_atlas","char_0"+Game.getPlayer(0).avatar));// new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","avatar_00015"));
			Avatar_Hud1_Image.x = (Avatar_Hud1_Image.width>>2) + (Avatar_Hud1_Image.width>>3);
			Avatar_Hud1_Image.y = Avatar_Hud1_Image.height + ( Avatar_Hud1_Image.height>>2)+ ( Avatar_Hud1_Image.height>>3);
			Avatar_Hud1_Image.scaleY=-1;
			Avatar_Hud1_Image.touchable = false;
			/*Avatar_Hud1_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "avatar_", 25,false,false); 
			Avatar_Hud1_animation.x=Avatar_Hud1_Image.x;
			Avatar_Hud1_animation.y=Avatar_Hud1_Image.y;
			Avatar_Hud1_animation.scaleY=-1;
			Avatar_Hud1_animation.addEventListener(Event.COMPLETE, completedHandler);
			Avatar_Hud1_animation.loop = false;
			Avatar_Hud1_animation.touchable = false;
			Avatar_Hud1_animation.visible = false;*/
			Ice_Hud1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","ice_hud_00010"));
			Ice_Hud1_Image.x = -Ice_Hud1_Image.width>>3;
			Ice_Hud1_Image.y = Ice_Hud1_Image.height-( Ice_Hud1_Image.height>>3);
			Ice_Hud1_Image.scaleY=-1;
			Ice_Hud1_Image.touchable = false;
			Ice_Hud1_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "ice_hud_", 25,false,false); 
			Ice_Hud1_animation.x=Ice_Hud1_Image.x;
			Ice_Hud1_animation.y=Ice_Hud1_Image.y;
			Ice_Hud1_animation.scaleY=-1;
			Ice_Hud1_animation.addEventListener(Event.COMPLETE, completedHandler);
			Ice_Hud1_animation.loop = false;
			Ice_Hud1_animation.touchable = false;
			Ice_Hud1_animation.visible = false;
			player1 = new TextField(Ice_Hud1_Image.width, Ice_Hud1_Image.height, player1_name,  myFont.fontName, 18, 0x000000);
			player1.x = Ice_Hud1_Image.x + (Ice_Hud1_Image.width) + (player1.width>>1);
			player1.scaleX = -1;
			player1.x = Ice_Hud1_Image.y + (Ice_Hud1_Image.height);// + (player1.width>>1);
			player1.scaleY = -1;
			addChild(player1);
			addChild (Ice_Hud1_Image);
			addChild (Ice_Hud1_animation);
			addChild (Avatar_Hud1_Image);
			//addChild (Avatar_Hud1_animation);
			
			Avatar_Hud2_Image = new Image(assetManager.getTextureFromAtlas("ui", "char_faces_atlas","char_0"+Game.getPlayer(1).avatar));//new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","avatar_00015"));
			Avatar_Hud2_Image.x = stage.stageWidth - (Avatar_Hud2_Image.width>>2) - (Avatar_Hud2_Image.width>>3);
			Avatar_Hud2_Image.y = stage.stageHeight-Avatar_Hud2_Image.height - ( Avatar_Hud2_Image.height>>2) - ( Avatar_Hud2_Image.height>>3);
			Avatar_Hud2_Image.scaleX=-1;
			Avatar_Hud2_Image.touchable = false;
			/*Avatar_Hud2_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "avatar_", 25,false,false); 
			Avatar_Hud2_animation.x=Avatar_Hud2_Image.x;
			Avatar_Hud2_animation.y=Avatar_Hud2_Image.y;
			Avatar_Hud2_animation.scaleX=-1;
			Avatar_Hud2_animation.addEventListener(Event.COMPLETE, completedHandler);
			Avatar_Hud2_animation.loop = false;
			Avatar_Hud2_animation.touchable = false;
			Avatar_Hud2_animation.visible = false;*/
			Ice_Hud2_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","ice_hud_00010"));
			Ice_Hud2_Image.x=stage.stageWidth+(Ice_Hud2_Image.width>>3);
			Ice_Hud2_Image.y=stage.stageHeight-Ice_Hud2_Image.height+(Ice_Hud2_Image.height>>3);
			Ice_Hud2_Image.scaleX = -1;
			Ice_Hud2_Image.touchable = false;
			Ice_Hud2_animation = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "ice_hud_", 25,false,false); 
			Ice_Hud2_animation.x=Ice_Hud2_Image.x;
			Ice_Hud2_animation.y=Ice_Hud2_Image.y;
			Ice_Hud2_animation.scaleX=-1;
			Ice_Hud2_animation.addEventListener(Event.COMPLETE, completedHandler);
			Ice_Hud2_animation.loop = false;
			Ice_Hud2_animation.touchable = false;
			Ice_Hud2_animation.visible = false;
			addChild (Ice_Hud2_Image);
			addChild (Ice_Hud2_animation);
			addChild (Avatar_Hud2_Image);
			//addChild (Avatar_Hud2_animation);
			
			bonhomme0_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","01"));//new Image(assetManager.getTexture("minigame_snowfight", "snowman"));
			bonhomme1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","01"));
			bonhomme2_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","01"));
			bonhomme3_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","02"));
			bonhomme4_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","03"));
			bonhomme5_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","04"));
			bonhomme6_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snowfight_assets","05"));
			
			snowball1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight","snowfight_assets", "snowball"));
			snowball2_Image = snowball1_Image;//= new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snow_placeholder", "bouledeneige"));
			snowball3_Image = snowball1_Image;//= new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snow_placeholder", "bouledeneige"));
			snowball4_Image = snowball1_Image;//= new Image(assetManager.getTextureFromAtlas("minigame_snowfight", "snow_placeholder", "bouledeneige"));
			
			Fight = assetManager.getMovieFromAtlas("minigame_snowfight", "snowfight_assets", "fight_", 25,false,false); 
			Fight.addEventListener(Event.COMPLETE, completedHandler);
			Fight.touchable = false;
			Fight.x=(stage.stageWidth>>1)-(Fight.width>>1);
			Fight.y=(stage.stageHeight>>1)-(Fight.height>>1);
			addChild(Fight);
			
			snowball_explosion = assetManager.getMovieFromAtlas("minigame_snowfight","snowfight_assets","anim_SB_",25,false,false);
			snowball_explosion.addEventListener(Event.COMPLETE, completedHandler);
			snowball_explosion.loop = false;
			snowball_explosion.touchable = false;
			snowball_explosion.visible = false;
			snowball_explosion.x=0;
			snowball_explosion.y=0;
			addChild(snowball_explosion);
		}
		
		private function place_questions_player(assetManager:AssetManager) : void
		{
			//snowman1_Image.visible=true;
			//snowman2_Image.visible=true;
			// sentence 1
			snowball1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight","snowfight_assets", "snowball"));
			// create the TextField object
			legend1 = new TextField((stage.stageWidth-snowball1_Image.width), stage.stageHeight>>4, _Sentences_Player1[0],  myFont.fontName, 18, TEXT_COLOR);
			
			// centers the text on stage
			legend1.x = stage.stageWidth-snowball1_Image.width;
			legend1.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height;
			legend1.rotation=deg2rad(180.0);
			legend1.hAlign="left";
			snowball1_Image.x =stage.stageWidth;
			snowball1_Image.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height-(snowball1_Image.height>>1);
			snowball1_Image.rotation=deg2rad(180.0);
			
			// show it
			addChild(legend1);
			
			// sentence 2
			snowball2_Image = snowball1_Image = new Image(assetManager.getTextureFromAtlas("minigame_snowfight","snowfight_assets", "snowball"));
			// create the TextField object
			legend2 = new TextField((stage.stageWidth-snowball2_Image.width), stage.stageHeight>>4, _Sentences_Player1[1],  myFont.fontName, 18, TEXT_COLOR);
			
			// centers the text on stage
			legend2.x = stage.stageWidth;
			legend2.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height;
			legend2.rotation=deg2rad(180.0);
			legend2.hAlign="right";
			snowball2_Image.x =snowball2_Image.width;
			snowball2_Image.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height-(snowball2_Image.height>>1);
			snowball2_Image.rotation=deg2rad(180.0);
			// show it
			addChild(legend2);

			// sentence 3

			// create the TextField object
			legend3 = new TextField((stage.stageWidth-snowball3_Image.width), stage.stageHeight>>4, _Sentences_Player1[2],  myFont.fontName, 18, TEXT_COLOR);
			
			// centers the text on stage
			legend3.x = stage.stageWidth-snowball3_Image.width;
			legend3.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height+legend3.height;
			legend3.rotation=deg2rad(180.0);
			legend3.hAlign="left";
			snowball3_Image.x =stage.stageWidth;
			snowball3_Image.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height+legend3.height-(snowball3_Image.height>>1);
			snowball3_Image.rotation=deg2rad(180.0);
			// show it
			addChild(legend3);
			
			// sentence 4

			// create the TextField object
			legend4 = new TextField((stage.stageWidth-snowball4_Image.width), stage.stageHeight>>4, _Sentences_Player1[3],  myFont.fontName, 18, TEXT_COLOR);
			
			// centers the text on stage
			legend4.x = stage.stageWidth;
			legend4.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height+legend3.height+legend4.height;
			legend4.rotation=deg2rad(180.0);
			legend4.hAlign="right";
			snowball4_Image.x =snowball4_Image.width;
			snowball4_Image.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+legend1.height+legend2.height+legend3.height+legend4.height-(snowball4_Image.height>>1);
			snowball4_Image.rotation=deg2rad(180.0);
			// show it
			addChild(legend4);

			
			tabBubble[0].setSprite(tabCircle[0]);
			
			
			tabBubble[1].setSprite(tabCircle[1]);
			
			
			tabBubble[2].setSprite(tabCircle[2]);
			
			
			tabBubble[3].setSprite(tabCircle[3]);
			
			tabBubble[0].initPosX=stage.stageWidth;
			tabBubble[0].initPosY=legend1.y;
			tabBubble[0].x=stage.stageWidth;
			tabBubble[0].y=legend1.y;
			tabBubble[0].rotation=deg2rad(180);
			
			tabBubble[1].initPosX=tabBubble[1].width;
			tabBubble[1].initPosY=legend2.y;
			tabBubble[1].x=tabBubble[1].width;
			tabBubble[1].y=legend2.y;
			tabBubble[1].rotation=deg2rad(180);
			
			tabBubble[2].initPosX=stage.stageWidth;
			tabBubble[2].initPosY=legend3.y;
			tabBubble[2].x=stage.stageWidth;
			tabBubble[2].y=legend3.y;
			tabBubble[2].rotation=deg2rad(180);
			
			tabBubble[3].initPosX=tabBubble[3].width;
			tabBubble[3].initPosY=legend4.y;
			tabBubble[3].x=tabBubble[3].width;
			tabBubble[3].y=legend4.y;
			tabBubble[3].rotation=deg2rad(180);
			
			for (var i:uint = 0; i < tabBubble.length ; i++)
			{
				this.addChild(tabBubble[i]);
			}
		}

		private function turn_player1():void 
		{
			bonus_update=false;
			new_sentences1();
			
			legend1.x = (snowball1_Image.width<<3)+(snowball1_Image.width>>1);
			legend1.y = zone_avatar2_Image.y - (legend1.height<<3)+(legend1.height)+(legend1.height>>1);
			legend1.rotation=deg2rad(0);
			legend1.text=_Sentences_Player1[0];
			tabBubble[0].initPosX=legend1.x-snowball1_Image.width;
			tabBubble[0].initPosY=legend1.y;
			tabBubble[0].x=legend1.x-snowball1_Image.width;
			tabBubble[0].y=legend1.y;
			tabBubble[0].rotation=deg2rad(0);
			
			legend2.x = (-snowball2_Image.width<<3)+(snowball1_Image.width>>1);
			legend2.y = legend1.y - legend2.height;
			legend2.rotation=deg2rad(0);
			legend2.text=_Sentences_Player1[1];
			tabBubble[1].initPosX=legend2.x + legend2.width;
			tabBubble[1].initPosY=legend2.y;
			tabBubble[1].x=legend2.x + legend2.width;
			tabBubble[1].y=legend2.y;
			tabBubble[1].rotation=deg2rad(0);
			
			legend3.x = (snowball3_Image.width<<3)+(snowball1_Image.width>>1);
			legend3.y = legend2.y - legend3.height;
			legend3.rotation=deg2rad(0);
			legend3.text=_Sentences_Player1[2];
			tabBubble[2].initPosX=legend3.x-snowball3_Image.width;
			tabBubble[2].initPosY=legend3.y;
			tabBubble[2].x=legend3.x-snowball3_Image.width;
			tabBubble[2].y=legend3.y;
			tabBubble[2].rotation=deg2rad(0);
			
			legend4.x = (-snowball4_Image.width<<3)+(snowball1_Image.width>>1);
			legend4.y = legend3.y - legend4.height;
			legend4.rotation=deg2rad(0);
			legend4.text=_Sentences_Player1[3];
			tabBubble[3].initPosX=legend4.x + legend4.width;
			tabBubble[3].initPosY=legend4.y;
			tabBubble[3].x=legend4.x + legend4.width;
			tabBubble[3].y=legend4.y;
			tabBubble[3].rotation=deg2rad(0);
			
		}
		
		private function turn_player2():void 
		{
			bonus_update=false;

			new_sentences2();
			
			legend1.x = stage.stageWidth-(snowball1_Image.width<<3)-(snowball1_Image.width>>1);
			legend1.y = (zone_avatar1_Image.y + zone_avatar1_Image.height)+(legend1.height<<2)-(legend1.height)-(legend1.height>>1);
			legend1.rotation=deg2rad(180);
			legend1.text=_Sentences_Player2[0];
			tabBubble[0].initPosX=legend1.x + snowball1_Image.width;
			tabBubble[0].initPosY=legend1.y;
			tabBubble[0].x=legend1.x + snowball1_Image.width;
			tabBubble[0].y=legend1.y;
			tabBubble[0].rotation=deg2rad(180);
			
			legend2.x = stage.stageWidth+(snowball2_Image.width<<3)-(snowball1_Image.width>>1);
			legend2.y = legend1.y +legend2.height;
			legend2.rotation=deg2rad(180);
			legend2.text=_Sentences_Player2[1];
			tabBubble[1].initPosX=legend2.x - legend2.width;
			tabBubble[1].initPosY=legend2.y;
			tabBubble[1].x=legend2.x - legend2.width;
			tabBubble[1].y=legend2.y;
			tabBubble[1].rotation=deg2rad(180);
			
			legend3.x = stage.stageWidth-(snowball3_Image.width<<3)-(snowball1_Image.width>>1);
			legend3.y = legend2.y +legend3.height;
			legend3.rotation=deg2rad(180);
			legend3.text=_Sentences_Player2[2];
			tabBubble[2].initPosX=legend3.x + snowball3_Image.width;
			tabBubble[2].initPosY=legend3.y;
			tabBubble[2].x=legend3.x + snowball3_Image.width;
			tabBubble[2].y=legend3.y;
			tabBubble[2].rotation=deg2rad(180);
			
			legend4.x = stage.stageWidth+(snowball4_Image.width<<3)-(snowball1_Image.width>>1);
			legend4.y = legend3.y +legend4.height;
			legend4.rotation=deg2rad(180);
			legend4.text=_Sentences_Player2[3];
			tabBubble[3].initPosX=legend4.x - legend4.width;
			tabBubble[3].initPosY=legend4.y;
			tabBubble[3].x=legend4.x - legend4.width;
			tabBubble[3].y=legend4.y;
			tabBubble[3].rotation=deg2rad(180);
			
			
		}
		private function snowman_maj_player1():void{

			switch(_Score_Player1)
			{
				case 1 :
					snowman1_Image.texture =  bonhomme1_Image.texture;
					snowman1_Image.visible=true;
					break;
				case 2 :
					snowman1_Image.texture =  bonhomme2_Image.texture;
					break;
				case 3 :
					snowman1_Image.texture =  bonhomme3_Image.texture;
					break;
				case 4 :
					snowman1_Image.texture = bonhomme4_Image.texture;
					break;
				case 5 :
					snowman1_Image.texture =  bonhomme5_Image.texture;
					break;
				case 6 :
					snowman1_Image.texture =  bonhomme6_Image.texture;
					break;
				default :
					
					snowman1_Image.texture = bonhomme0_Image.texture;
			}
	
		}
		
		private function snowman_maj_player2():void{
			switch(_Score_Player2)
			{
				case 1 :
					snowman2_Image.texture = bonhomme1_Image.texture;
					snowman2_Image.visible = true;
					break;
				case 2 :
					snowman2_Image.texture = bonhomme2_Image.texture;
					break;
				case 3 :
					snowman2_Image.texture = bonhomme3_Image.texture;
					break;
				case 4 :
					snowman2_Image.texture = bonhomme4_Image.texture;
					break;
				case 5 :
					snowman2_Image.texture = bonhomme5_Image.texture;
					break;
				case 6 :
					snowman2_Image.texture = bonhomme6_Image.texture;
					break;
				default :
					snowman2_Image.texture = bonhomme0_Image.texture;
			}
			
		}
		
			 		 	
		private var tabCircle:Array;
		public var tabBubble:Array;
		public function loadBubble() : void
		{
			tabCircle = new Array();
			var tabTexture:Array = new Array();
			
			
			for(var i:uint = 0; i < 4; i++)
			{
				tabTexture.push (snowball1_Image.texture);
				tabCircle.push( new Image(tabTexture[i]));
	
				
			}
			
			tabBubble = new Array;
			tabBubble[0] = new Sentence(true, stage.stageWidth>>1,stage.stageHeight>>1);
			tabBubble[1] = new Sentence(false, stage.stageWidth>>1,stage.stageHeight>>1);
			tabBubble[2] = new Sentence(false,  stage.stageWidth>>1,stage.stageHeight>>1);
			tabBubble[3] = new Sentence(false,stage.stageWidth>>1,stage.stageHeight>>1);
			
		}
		
		private function new_sentences1() : void
		{
			var temp_tabBubble1:Array;
			var temp_Sentences_Player1:Array;
			var randomPos:Number = 0;
			var i:int;
			
			// maj du bonus
			zone_avatar1_bonus._turn=_turn;
			zone_avatar1_bonus._turn1=_turn1;
			zone_avatar2_bonus._turn=_turn;
			zone_avatar2_bonus._turn1=_turn1;
			zone_avatar1_bonus.next_turn=true;
			zone_avatar2_bonus.next_turn=true;
			
			if ((_turn==false && _turn1==false)|| (_turn==true && _turn1==false))
			{

				// selon _turn1 false ou true
				temp_tabBubble1 = new Array;
				temp_tabBubble1[0] = tabBubble[0];
				temp_tabBubble1[1] = tabBubble[1];
				temp_tabBubble1[2] = tabBubble[2];
				temp_tabBubble1[3] = tabBubble[3];
				temp_Sentences_Player1= new Array();
				temp_Sentences_Player1[0]=_Player2_qa[4];
				temp_tabBubble1[0].setcorrect(_Player2_qa_bool[4]);
				temp_Sentences_Player1[1]=_Player2_qa[5];
				temp_tabBubble1[1].setcorrect(_Player2_qa_bool[5]);
				temp_Sentences_Player1[2]=_Player2_qa[6];
				temp_tabBubble1[2].setcorrect(_Player2_qa_bool[6]);
				temp_Sentences_Player1[3]=_Player2_qa[7];
				temp_tabBubble1[3].setcorrect(_Player2_qa_bool[7]);
				
				this._Sentences_Player1 = new Array(temp_Sentences_Player1.length);
				
				randomPos = 0;
				for (i = 0; i < _Sentences_Player1.length; i++)
				{
					randomPos = int(Math.random() * temp_Sentences_Player1.length);
					_Sentences_Player1[i] = temp_Sentences_Player1.splice(randomPos, 1)[0];   //since splice() returns an Array, we have to specify that we want the first (only) element
					tabBubble[i] = temp_tabBubble1.splice(randomPos, 1)[0];
				}
			}
			if (_turn==false && _turn1==true)
			{	
				
				var random_qa:int = MathUtils.randi(0,_game.QUESTION.GROUP.length());

				//selection de la salve de qa
				_Player1_qa[0]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[0];//"qtrue";
				_Player1_qa[1]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[1];//"qfalse1";
				_Player1_qa[2]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[2];//"qfalse2";
				_Player1_qa[3]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[3];//"qfalse3";
				_Player1_qa[4]=_game.ANSWER.GROUP[random_qa].SENTENCEA[0];//"atrue2";
				_Player1_qa[5]=_game.ANSWER.GROUP[random_qa].SENTENCEA[1];//"afalse12";
				_Player1_qa[6]=_game.ANSWER.GROUP[random_qa].SENTENCEA[2];//"afalse22";
				_Player1_qa[7]=_game.ANSWER.GROUP[random_qa].SENTENCEA[3];//"afalse32";
				
				for (i=0;i<4;i++)
				{
					if ( _game.QUESTION.GROUP[random_qa].SENTENCEQ[i].@answer == "true")
					{
						_Player1_qa_bool[i]=true;
					}
					else{
						_Player1_qa_bool[i]=false;
					}
					
					if ( _game.ANSWER.GROUP[random_qa].SENTENCEA[i].@answer == "true")
					{
						_Player1_qa_bool[i+4]=true;
					}
					else{
						_Player1_qa_bool[i+4]=false;
					}
						
				}
					
				
				// selon _turn1 false ou true
				temp_tabBubble1 = new Array;
				temp_tabBubble1[0] = tabBubble[0];
				temp_tabBubble1[1] = tabBubble[1];
				temp_tabBubble1[2] = tabBubble[2];
				temp_tabBubble1[3] = tabBubble[3];
				temp_Sentences_Player1= new Array();
				temp_Sentences_Player1[0]=_Player1_qa[0];
				temp_tabBubble1[0].setcorrect(_Player1_qa_bool[0]);
				temp_Sentences_Player1[1]=_Player1_qa[1];
				temp_tabBubble1[1].setcorrect(_Player1_qa_bool[1]);
				temp_Sentences_Player1[2]=_Player1_qa[2];
				temp_tabBubble1[2].setcorrect(_Player1_qa_bool[2]);
				temp_Sentences_Player1[3]=_Player1_qa[3];
				temp_tabBubble1[3].setcorrect(_Player1_qa_bool[3]);
				
				
				this._Sentences_Player1 = new Array(temp_Sentences_Player1.length);
				
				randomPos = 0;
				for (i = 0; i < _Sentences_Player1.length; i++)
				{
					randomPos = int(Math.random() * temp_Sentences_Player1.length);
					_Sentences_Player1[i] = temp_Sentences_Player1.splice(randomPos, 1)[0];   //since splice() returns an Array, we have to specify that we want the first (only) element
					tabBubble[i] = temp_tabBubble1.splice(randomPos, 1)[0];
				}
			}

		}
		
		private function new_sentences2() : void
		{
			// maj du bonus
			zone_avatar1_bonus._turn=_turn;
			zone_avatar1_bonus._turn1=_turn1;
			zone_avatar2_bonus._turn=_turn;
			zone_avatar2_bonus._turn1=_turn1;
			zone_avatar1_bonus.next_turn=true;
			zone_avatar2_bonus.next_turn=true;
			
			var temp_tabBubble2:Array;
			var temp_Sentences_Player2:Array;
			var randomPos:Number = 0;
			var i:int;
			if (_turn==true && _turn1==true || (_turn==false && _turn1==true))
			{
				temp_tabBubble2 = new Array;
				temp_tabBubble2[0] = tabBubble[0];
				temp_tabBubble2[1] = tabBubble[1];
				temp_tabBubble2[2] = tabBubble[2];
				temp_tabBubble2[3] = tabBubble[3];
				temp_Sentences_Player2= new Array();
				temp_Sentences_Player2[0]=_Player1_qa[4];
				temp_tabBubble2[0].setcorrect(_Player1_qa_bool[4]);
				temp_Sentences_Player2[1]=_Player1_qa[5];
				temp_tabBubble2[1].setcorrect(_Player1_qa_bool[5]);
				temp_Sentences_Player2[2]=_Player1_qa[6];
				temp_tabBubble2[2].setcorrect(_Player1_qa_bool[6]);
				temp_Sentences_Player2[3]=_Player1_qa[7];
				temp_tabBubble2[3].setcorrect(_Player1_qa_bool[7]);
				
				this._Sentences_Player2 = new Array(temp_Sentences_Player2.length);
				
				randomPos = 0;
				for (i = 0; i < _Sentences_Player2.length; i++)
				{
					randomPos = int(Math.random() * temp_Sentences_Player2.length);
					_Sentences_Player2[i] = temp_Sentences_Player2.splice(randomPos, 1)[0];   //since splice() returns an Array, we have to specify that we want the first (only) element
					tabBubble[i] = temp_tabBubble2.splice(randomPos, 1)[0];
				}
			}
			
			if (_turn==true && _turn1==false)
			{
				
				var random_qa:int = MathUtils.randi(0,_game.QUESTION.GROUP.length());

				//selection de la salve de qa
				_Player2_qa[0]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[0];//"qtrue2";
				_Player2_qa[1]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[1];//"qfalse12";
				_Player2_qa[2]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[2];//"qfalse22";
				_Player2_qa[3]=_game.QUESTION.GROUP[random_qa].SENTENCEQ[3];//"qfalse32";
				_Player2_qa[4]=_game.ANSWER.GROUP[random_qa].SENTENCEA[0];//"atrue";
				_Player2_qa[5]=_game.ANSWER.GROUP[random_qa].SENTENCEA[1];//"afalse1";
				_Player2_qa[6]=_game.ANSWER.GROUP[random_qa].SENTENCEA[2];//"afalse2";
				_Player2_qa[7]=_game.ANSWER.GROUP[random_qa].SENTENCEA[3];//"afalse3";
				
				for (i=0;i<4;i++)
				{
					if ( _game.QUESTION.GROUP[random_qa].SENTENCEQ[i].@answer == "true")
					{
						_Player2_qa_bool[i]=true;
					}
					else{
						_Player2_qa_bool[i]=false;
					}
					
					if ( _game.ANSWER.GROUP[random_qa].SENTENCEA[i].@answer == "true")
					{
						_Player2_qa_bool[i+4]=true;
					}
					else{
						_Player2_qa_bool[i+4]=false;
					}
					
				}
				
				temp_tabBubble2 = new Array;
				temp_tabBubble2[0] = tabBubble[0];
				temp_tabBubble2[1] = tabBubble[1];
				temp_tabBubble2[2] = tabBubble[2];
				temp_tabBubble2[3] = tabBubble[3];
				temp_Sentences_Player2= new Array();
				temp_Sentences_Player2[0]=_Player2_qa[0];
				temp_tabBubble2[0].setcorrect(_Player2_qa_bool[0]);
				temp_Sentences_Player2[1]=_Player2_qa[1];
				temp_tabBubble2[1].setcorrect(_Player2_qa_bool[1]);
				temp_Sentences_Player2[2]=_Player2_qa[2];
				temp_tabBubble2[2].setcorrect(_Player2_qa_bool[2]);
				temp_Sentences_Player2[3]=_Player2_qa[3];
				temp_tabBubble2[3].setcorrect(_Player2_qa_bool[3]);
				
				
				this._Sentences_Player2 = new Array(temp_Sentences_Player2.length);
				
				randomPos = 0;
				for (i = 0; i < _Sentences_Player2.length; i++)
				{
					randomPos = int(Math.random() * temp_Sentences_Player2.length);
					_Sentences_Player2[i] = temp_Sentences_Player2.splice(randomPos, 1)[0];   //since splice() returns an Array, we have to specify that we want the first (only) element
					tabBubble[i] = temp_tabBubble2.splice(randomPos, 1)[0];
				}
			}
		}
		

		override protected function prepareGame(assetManager:AssetManager):void {
			assetManager.loadAtlas("minigame_snowfight", "snowfight_assets");
			assetManager.loadTexture("minigame_snowfight","snowman");
			assetManager.loadAtlas("ui", "char_faces_atlas");
			
			_game = assetManager.loadXML("xml", "snowfight_game1");
						
		}
		
		override protected function initGame(assetManager:AssetManager):void {
			
			Game.getSoundManager().playMusic("snowfight");
			drawZonePlayer(assetManager);
			loadBubble();
			new_sentences1();
			place_questions_player(assetManager);
			turn_player2();
		}
		
		protected function legend_hidestate(ans:Boolean):void{
			
			
			var i:uint = 0;
			if (ans == false){
			applepad.effects.Effect.CreateFadeOut(legend1, 0.25);
			applepad.effects.Effect.CreateFadeOut(legend2, 0.25);
			applepad.effects.Effect.CreateFadeOut(legend3, 0.25);
			applepad.effects.Effect.CreateFadeOut(legend4, 0.25);
			for(i = 0; i < tabBubble.length; i++)
			{
				applepad.effects.Effect.CreateFadeOut(tabBubble[i], 0.25);
			}
			}
			else{
			
				applepad.effects.Effect.CreateFadeIn(legend1, 0.25);
				applepad.effects.Effect.CreateFadeIn(legend2, 0.25);
				applepad.effects.Effect.CreateFadeIn(legend3, 0.25);
				applepad.effects.Effect.CreateFadeIn(legend4, 0.25);
			
				for(i = 0; i < tabBubble.length; i++)
				{
					applepad.effects.Effect.CreateFadeIn(tabBubble[i], 0.25);
				}
			}
			
			legend1.visible = ans;
			legend2.visible = ans;
			legend3.visible = ans;
			legend4.visible = ans;
			for(i = 0; i < tabBubble.length; i++)
			{
				
				tabBubble[i].visible=ans;
			}

		}

		override protected function updateGame( elapsedTime:Number ):Boolean {	
			var i:uint = 0;
			var j:uint = 0;
			var k:uint = 0;
			
			//maj bonus
			
			if ((zone_avatar1_bonus.next_turn==false || zone_avatar2_bonus.next_turn==false) && bonus_update==false){
			var tmp:uint = 2;	
			for(i = 0; i < tabBubble.length; i++)
				{
					if(tabBubble[i].getcorrect()==false && tmp>0){
						tabBubble[i].visible=false;
						tmp--;		
					}
				}
				
				if(tabBubble[0].visible==false){
				legend1.visible=false;	
				}
				if(tabBubble[1].visible==false){
					legend2.visible=false;	
				}
				if(tabBubble[2].visible==false){
					legend3.visible=false;	
				}
				if(tabBubble[3].visible==false){
					legend4.visible=false;	
				}
				bonus_update=true;
				
			}
			
			for(i = 0; i < tabBubble.length; i++)
			{
				if ((tabBubble[i].x != tabBubble[i].initPosX || tabBubble[i].y != tabBubble[i].initPosY))
				{
					if (tabBubble[i].x > (canon1_Image.x) && tabBubble[i].x < (canon1_Image.x+canon1_Image.width)  && tabBubble[i].y < canon1_Image.height && _turn==true && _turn1==false && tabBubble[i].isReleased==true)
					{

						//animation construction bonhomme
						_turn=false;
						if  (tabBubble[i].getcorrect() == true){
							_Score_Player1+=1;
							_nb_tours+=1;
							snowman_maj_player1();
							// animation neige
							//animation canon
							throw_snowball2_animation.y=zone_avatar2_Image.y+(zone_avatar2_Image.height);
							throw_snowball2_animation.scaleY=-2;
							canon1_Image.visible = false;
							legend_hidestate(false);
							throw_snowball2_animation.visible = true;
							canon1_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon1_animation.start();
							Game.getSoundManager().playSound("snowfight_good");
							throw_snowball2_animation.start();
							snowball_explosion.x=zone_avatar2_Image.x-zone_avatar2_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=-1;
							snowball_explosion.y=zone_avatar2_Image.y+zone_avatar2_Image.height;


						}
						else{
							_nb_tours+=1;
							// animation neige
							//animation canon
							throw_snowball2_animation.y=zone_avatar2_Image.y-(zone_avatar2_Image.height>>1);
							throw_snowball2_animation.scaleY=-1;
							canon1_Image.visible = false;
							legend_hidestate(false);
							throw_snowball2_animation.visible = true;
							canon1_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon1_animation.start();
							Game.getSoundManager().playSound("snowfight_miss");
							throw_snowball2_animation.start();
							snowball_explosion.x=zone_avatar2_Image.x-zone_avatar2_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=-1;
							snowball_explosion.y=zone_avatar2_Image.y;



						}
						
						turn_player1();
						
					}
					else if (tabBubble[i].x > (canon1_Image.x) && tabBubble[i].x < (canon1_Image.x+canon1_Image.width)  && tabBubble[i].y < canon1_Image.height && _turn==true && _turn1==true && tabBubble[i].isReleased==true)
					{
						
						//animation construction bonhomme
						_turn1=false;
						
						if  (tabBubble[i].getcorrect() == true){
							_Score_Player1+=1;
							_nb_tours+=1;
							snowman_maj_player1();
							//trace("+1");
							// animation neige
							//animation canon
							throw_snowball2_animation.y=zone_avatar2_Image.y+(zone_avatar2_Image.height);
							throw_snowball2_animation.scaleY=-2;
							throw_snowball2_animation.visible = true;
							canon1_Image.visible = false;
							legend_hidestate(false);
							canon1_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon1_animation.start();
							Game.getSoundManager().playSound("snowfight_good");
							throw_snowball2_animation.start();
							snowball_explosion.x=zone_avatar2_Image.x-zone_avatar2_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=-1;
							snowball_explosion.y=zone_avatar2_Image.y+zone_avatar2_Image.height;


						}
						else{
							_nb_tours+=1;
							// animation neige
							//animation canon
							throw_snowball2_animation.y=zone_avatar2_Image.y-(zone_avatar2_Image.height>>1);
							throw_snowball2_animation.scaleY=-1;
							throw_snowball2_animation.visible = true;
							canon1_Image.visible = false;
							legend_hidestate(false);
							canon1_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon1_animation.start();
							Game.getSoundManager().playSound("snowfight_miss");
							throw_snowball2_animation.start();
							snowball_explosion.x=zone_avatar2_Image.x;
							snowball_explosion.y=zone_avatar2_Image.y-zone_avatar2_Image.height;
							snowball_explosion.x=zone_avatar2_Image.x-zone_avatar2_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=-1;
							snowball_explosion.y=zone_avatar2_Image.y;



						}
						turn_player2();
						
					}
					else if(tabBubble[i].x > canon2_Image.x && tabBubble[i].x < canon2_Image.x + canon2_Image.width && tabBubble[i].y > (stage.stageHeight-canon2_Image.height) && _turn==false && _turn1==false && tabBubble[i].isReleased==true)
					{

						_turn=false;
						_turn1=true;
						
						if  (tabBubble[i].getcorrect() == true){
							_Score_Player2+=1;
							_nb_tours+=1;
							snowman_maj_player2();
							// animation neige
							//animation canon
							throw_snowball1_animation.y=zone_avatar1_Image.y-(zone_avatar1_Image.height);
							throw_snowball1_animation.scaleY=2;
							throw_snowball1_animation.visible = true;
							canon2_Image.visible = false;
							legend_hidestate(false);
							canon2_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon2_animation.start();
							Game.getSoundManager().playSound("snowfight_good");
							throw_snowball1_animation.start();
							snowball_explosion.x=zone_avatar1_Image.x-zone_avatar1_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=1;
							snowball_explosion.y=zone_avatar1_Image.y-zone_avatar1_Image.height;

						}
						else{
							_nb_tours+=1;
							// animation neige
							//animation canon
							throw_snowball1_animation.y=zone_avatar1_Image.y+(zone_avatar1_Image.height>>1);
							throw_snowball1_animation.scaleY=1;
							throw_snowball1_animation.visible = true;
							canon2_Image.visible = false;
							legend_hidestate(false);
							canon2_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon2_animation.start();
							Game.getSoundManager().playSound("snowfight_miss");
							throw_snowball1_animation.start();
							snowball_explosion.x=zone_avatar1_Image.x-zone_avatar1_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=1;
							snowball_explosion.y=zone_avatar1_Image.y;


							// animation neige
						}
						turn_player1();
					}
					else if(tabBubble[i].x > canon2_Image.x && tabBubble[i].x < canon2_Image.x + canon2_Image.width && tabBubble[i].y > (stage.stageHeight-canon2_Image.height) && _turn==false && _turn1==true && tabBubble[i].isReleased==true)
					{
							
						_turn=true;
						_turn1=true;
						
						if  (tabBubble[i].getcorrect() == true){
							_Score_Player2+=1;
							_nb_tours+=1;
							snowman_maj_player2();
							// animation neige
							//animation canon
							throw_snowball1_animation.y=zone_avatar1_Image.y-(zone_avatar1_Image.height);
							throw_snowball1_animation.scaleY=2;
							throw_snowball1_animation.visible = true;
							canon2_Image.visible = false;
							legend_hidestate(false);
							canon2_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon2_animation.start();
							Game.getSoundManager().playSound("snowfight_good");
							throw_snowball1_animation.start();
							snowball_explosion.x=zone_avatar1_Image.x-zone_avatar1_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=1;
							snowball_explosion.y=zone_avatar1_Image.y-zone_avatar1_Image.height;



						}
						else{
							_nb_tours+=1;
							// animation neige
							//animation canon
							throw_snowball1_animation.y=zone_avatar1_Image.y+(zone_avatar1_Image.height>>1);
							throw_snowball1_animation.scaleY=1;
							throw_snowball1_animation.visible = true;
							canon2_Image.visible = false;
							legend_hidestate(false);
							canon2_animation.visible = true;
							Game.getSoundManager().playSound("snowfight_snowball");
							canon2_animation.start();
							Game.getSoundManager().playSound("snowfight_miss");
							throw_snowball1_animation.start();
							snowball_explosion.x=zone_avatar1_Image.x-zone_avatar1_Image.width+(snowball1_Image.width>>1);
							snowball_explosion.scaleY=1;
							snowball_explosion.y=zone_avatar1_Image.y;

						}
						turn_player2();
					}
					else
					{
						tabBubble[i].wait = true;
						tabBubble[i].replaceState == true
					}
				}
				
				if (tabBubble[i].replaceState == true)
				{	
					tabBubble[i].replaceState = false;
					tabBubble[i].replace(tabBubble[i].initPosX,tabBubble[i].initPosY);
					//trace("toto");
				}
			}
			
			
			return true;
		}
		
		private function endgame(restart:Boolean):void{
			
			legend1.visible = restart;
			legend2.visible = restart;
			legend3.visible = restart;
			legend4.visible = restart;
			
			zone_avatar1_Image.visible = restart;
			zone_avatar2_Image.visible = restart;
			
			canon1_Image.visible = restart;
			canon2_Image.visible = restart;
			
			wallpaper_Image.visible = restart;
			
			snowball1_Image.visible = restart;	
			snowball2_Image.visible = restart;
			snowball3_Image.visible = restart;
			snowball4_Image.visible = restart;
			
			zone_avatar1_bonus.visible = restart;
			zone_avatar2_bonus.visible = restart;
			Avatar_Hud1_Image.visible = restart;
			Avatar_Hud2_Image.visible = restart;
			Ice_Hud1_Image.visible = restart;
			Ice_Hud2_Image.visible = restart;
			zone_avatar1_shadow.visible = restart;
			zone_avatar2_shadow.visible = restart;
			
			for(var i:uint = 0; i < tabBubble.length; i++){
				tabBubble[i].visible = restart;
			}
			if (restart == false){

				switchGameState(GAME_STATE_FINISH);
				snowman1_Image.visible=restart;
				snowman2_Image.visible=restart;
			}
			
		}
		
		override protected function restartGame():void {
			endgame(true);
			_Score_Player1=0;
			_Score_Player2=0;
			_nb_tours=0;
			_turn=true;
			_turn1=false;
			snowman1_Image.texture=bonhomme0_Image.texture;
			snowman2_Image.texture=bonhomme0_Image.texture;
			zone_avatar1_bonus._bonus_player=2;
			zone_avatar2_bonus._bonus_player=2;
			//snowman
			snowman1_Image.scaleX=1;
			snowman1_Image.scaleY=1;

			snowman1_Image.rotation=deg2rad(0);
			
			snowman2_Image.scaleX=1;
			snowman2_Image.scaleY=1;

			
			snowman1_Image.x = stage.stageWidth-(snowman1_Image.width<<1);
			snowman1_Image.y =snowman1_Image.height;
			snowman1_Image.scaleY=-1;
			snowman2_Image.x = snowman2_Image.width;
			snowman2_Image.y = stage.stageHeight-snowman2_Image.height;

		}
		override protected function introGame():Boolean {
			zone_avatar1_bonus.visible = false;
			zone_avatar2_bonus.visible = false;
			canon1_Image.visible = false;
			canon2_Image.visible = false;
			zone_avatar1_shadow.visible = false;
			zone_avatar2_shadow.visible = false;
			Avatar_Hud1_Image.visible = false;
			Avatar_Hud2_Image.visible = false;
			Ice_Hud1_Image.visible = false;
			Ice_Hud2_Image.visible = false;
			legend1.visible = false;
			legend2.visible = false;
			legend3.visible = false;
			legend4.visible = false;
			for(var i:uint = 0; i < tabBubble.length; i++)
			{
					tabBubble[i].visible=false;
			}
			
			doFadeIn(this,500, this);
			return false;
		}
		override protected function finishGame():void {
			Game.getSoundManager().playSound("snowfight_ending");
			doFadeOut(this, 500, this);	
		}
		override protected function releaseGame():void {
			Game.getSoundManager().stopMusic("snowfight");
		}

		public function onFadeOutFinished(e:FadeOut):void {
			_scores[0] = Number(_Score_Player1/6);
			_scores[1] = Number(_Score_Player2/6);
			changeSequence("ResultSequence", [2, _scores]);	
			//changeSequence("MenuSequence");
		}
		
		public function completedHandler (e:Event) :void {
			
			//Starling.juggler.remove(e.target as MovieClip);
			
			switch(e.target) {
				case Ice_Hud2_animation://Avatar_Hud2_animation:
				{
					avatar1_animation.visible = false;
					avatar2_animation.visible = false;
					canon1_animation.visible = false;
					canon2_animation.visible = false;
					avatar1_shadow_animation.visible = false;
					avatar2_shadow_animation.visible = false;
					//Avatar_Hud1_animation.visible = false;
					//Avatar_Hud2_animation.visible = false;
					Ice_Hud1_animation.visible = false;
					Ice_Hud2_animation.visible = false;
					zone_avatar1_bonus.visible = true;
					zone_avatar2_bonus.visible = true;
					Avatar_Hud1_Image.visible = true;
					Avatar_Hud2_Image.visible = true;
					Ice_Hud1_Image.visible = true;
					Ice_Hud2_Image.visible = true;
					canon1_Image.visible = true;
					canon2_Image.visible = true;
					zone_avatar1_shadow.visible = true;
					zone_avatar2_shadow.visible = true;
					snowman1_Image.visible=false;
					snowman2_Image.visible=false;
					
					avatar1_animation.destroy();
					avatar2_animation.destroy();
					//canon1_animation.destroy();
					//canon2_animation.destroy();
					avatar1_shadow_animation.destroy();
					avatar2_shadow_animation.destroy();
					//Avatar_Hud1_animation.destroy();
					//Avatar_Hud2_animation.destroy();
					Ice_Hud1_animation.destroy();
					Ice_Hud2_animation.destroy();

					Fight.start();
					
				}

				break;
				
				case Fight:
				{
					Fight.visible = false;
					legend1.visible = true;
					legend2.visible = true;
					legend3.visible = true;
					legend4.visible = true;
					for(var i:uint = 0; i < tabBubble.length; i++)
					{
						tabBubble[i].visible=true;
					}
						
					legend_hidestate(true);
				}
				break;
				
				case throw_snowball1_animation:
				{
					throw_snowball1_animation.visible = false;
					canon1_Image.visible = true;
					canon1_animation.visible = false;
					snowball_explosion.visible = true;
					snowball_explosion.start();
				}
					break;
				
				case throw_snowball2_animation:
				{
					throw_snowball2_animation.visible = false;
					canon2_Image.visible = true;
					canon2_animation.visible = false;
					snowball_explosion.visible = true;
					snowball_explosion.start();
				}
					break;
				
				case snowball_explosion:
				{
					snowball_explosion.visible = false;
					legend_hidestate(true);
					if (_Score_Player1==6 && _Score_Player2==6 && _nb_tours==12){
						//trace("player1 & player2 win");
						canon1_Image.visible = false;
						canon1_animation.visible = false;
						endgame(false);
						_nb_tours+=1;
					}
						
					else if (_Score_Player1==6 && _Score_Player2<6 && _nb_tours==12){
						//trace("player1 win");
						canon1_Image.visible = false;
						canon1_animation.visible = false;
						endgame(false);
						_nb_tours+=1;
					}
					else if (_Score_Player2==6 && _Score_Player1<6 && _nb_tours==12){
						//trace("player2 win");
						canon1_Image.visible = false;
						canon1_animation.visible = false;
						endgame(false);
						_nb_tours+=1;
					}
					else if (_nb_tours==12){
						//trace("fin de partie");
						canon1_Image.visible = false;
						canon1_animation.visible = false;
						endgame(false);
						_nb_tours+=1;
					}
				}
					break;
			}
		}
		
		public function onFadeInFinished(e:FadeIn):void {
			avatar1_animation.visible = true;
			avatar2_animation.visible = true;
			canon1_animation.visible = true;
			canon2_animation.visible = true;
			avatar1_shadow_animation.visible = true;
			avatar2_shadow_animation.visible = true;
			//Avatar_Hud1_animation.visible = true;
			//Avatar_Hud2_animation.visible = true;
			Ice_Hud1_animation.visible = true;
			Ice_Hud2_animation.visible = true;

			avatar1_animation.start();
			avatar2_animation.start();
			canon1_animation.start();
			canon2_animation.start();
			avatar1_shadow_animation.start();
			avatar2_shadow_animation.start();
			//Avatar_Hud1_animation.start();
			//Avatar_Hud2_animation.start();
			Ice_Hud1_animation.start();
			Ice_Hud2_animation.start();

		}
	}
}
import applepad.entities.Entity;
import applepad.entities.Player;
import applepad.utils.Vector2;

import flash.geom.Point;

import starling.display.Quad;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.utils.deg2rad;

internal class Sentence extends Entity {
	
	
	private var _correct:Boolean;
	public var initPosX:Number;
	public var initPosY:Number;
	public var tabLocation:Array;
	public var replaceState:Boolean
	public var wait:Boolean;
	public var dir:Vector2;
	
	public var isReleased:Boolean = false;
	
	public function Sentence (answer:Boolean, x:Number, y:Number) {
		_correct = answer;
		this.x = initPosX = x;
		this.y = initPosY = y;
		this.replaceState = false;
		this.wait = false;
		this.dir = new Vector2();
		tabLocation = new Array();
		
	}
	
	public function getcorrect():Boolean {
		return _correct;
	}
	
	public function setcorrect(answer:Boolean) : void {
		_correct = answer;
	}
	
	
	public function isCollided(sentence:Sentence) : Boolean
	{
		var collided:Boolean = false;
		
		if (this.x >= sentence.x && this.x <= sentence.x + sentence.width)
		{
			if (this.y >= sentence.y && this.y <= sentence.y + sentence.height)
			{
				collided = true;
			}
		}	
		
		if (this.x + this.width >= sentence.x && this.x + this.width <= sentence.x + sentence.width)
		{
			if (this.y  >= sentence.y && this.y <= sentence.y + sentence.height)
			{
				collided = true;
			}
		}	
		
		
		if (this.y + this.height >= sentence.y && this.y + this.height <= sentence.y + sentence.height)
		{
			if (this.x >= sentence.x && this.x <= sentence.x + sentence.width)
			{
				collided = true;
			}
		}
		
		if (this.y + this.height >= sentence.y && this.y + this.height <= sentence.y + sentence.height)
		{
			if (this.x + this.width >= sentence.x && this.x + this.width <= sentence.x + sentence.width)
			{
				collided = true;
			}
		}
		
		return collided;			
	}
	
	public function replace(x:Number, y:Number): void {
		this.x = x;
		this.y = y;		

		/*var source:Vector2 = new Vector2(this.x, this.y);
		var dest:Vector2   = new Vector2(x,y);
		
		
		dest.substract(source);
		
		dest.normalize();
		
		this.dir.clone(dest);*/
		
		
	}
	
	override public function initialize() : void {
		
	}
	
	override public function update( elapsedTime:Number ):void {
		
	}
	
	override public function release():void {
	}
	
	override public function touch(e:TouchEvent):void {

		var touches:Vector.<Touch> = e.getTouches(this);
		var target:Quad = e.target as Quad;
		
		for each (var touch:Touch in touches)
		{
			
			if (touch.phase == TouchPhase.BEGAN || touch.phase == TouchPhase.MOVED && !replaceState)
			{	
				isReleased = false;
				
				var location:Point = touch.getLocation(stage);
								
				if (location.x >= stage.stageWidth - 10)
					this.x = stage.stageWidth - 10;
				else if (location.x <= 10)
					this.x = 10;
				else if (this.rotation==deg2rad(180))
					this.x = location.x + (target.width>>1);	
				else
					this.x = location.x - (target.width>>1);	
				
				if (location.y >= stage.stageHeight - 50)
					this.y = stage.stageHeight - 50;
				else if (location.y <= 10)
					this.y = 10;
				else if (this.rotation==deg2rad(180))
					this.y = location.y + (target.height>>1);	
				else				
					this.y = location.y - (target.height>>1);
				
				tabLocation.push(new Vector2(location.x, location.y));
				
				
				
										
				 
			}
			else if (touch.phase == TouchPhase.ENDED)
			{
				isReleased = true;
				
				if (this.wait == true)
				{
					this.wait=false;
					this.replaceState=true;
				}
			}
			
		}
			
	
	}
	
	
}

internal class Player_bonus extends Entity {
	
	
	public var initPosX:Number;
	public var initPosY:Number;
	public var player:Boolean;
	public var _turn:Boolean;
	public var _turn1:Boolean;
	public var _bonus_player:uint;
	
	public var next_turn:Boolean;
	
	public function Player_bonus (x:Number, y:Number,player:Boolean) {

		this.x = initPosX = x;
		this.y = initPosY = y;
		this.player = player;
		this._turn=true;
		this._turn1=false;
		this._bonus_player=2;
		this.next_turn=true;

	}
	
	
	override public function initialize() : void {
		
	}
	
	override public function update( elapsedTime:Number ):void {
		
	}
	
	override public function release():void {
	}
	
	override public function touch(e:TouchEvent):void {
		
		var touches:Vector.<Touch> = e.getTouches(this);
		var target:Quad = e.target as Quad;
		
		for each (var touch:Touch in touches)
		{
			
			if (touch.phase == TouchPhase.BEGAN) 
			{	
				if ((this._turn==true && this._turn1==false && this.player==true && this._bonus_player>0 && this.next_turn==true)||(this._turn==true && this._turn1==true && this.player==true && this._bonus_player>0 && this.next_turn==true)){
					//trace("toto1" + player);
					this._bonus_player--;
					this.next_turn=false;
				}
				
				if ((this._turn==false && this._turn1==false && this.player==false && this._bonus_player>0 && this.next_turn==true)||(this._turn==false && this._turn1==true && this.player==false && this._bonus_player>0 && this.next_turn==true)){
					//trace("toto2" + player);
					this._bonus_player--;
					this.next_turn=false;
				}
	
			}
			/*else if (touch.phase == TouchPhase.ENDED)
			{
				trace("tata");
			}*/
			
		}
		
		
	}
	
	
}

