package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.entities.MovieClip;
	
	import flash.display.Bitmap;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class LoadingSequence extends Sequence
	{	
		
		[Embed(source="loading/loading_atlas.png")]
		private static const Atlas:Class;
		
		[Embed(source="loading/loading_atlas.xml", mimeType="application/octet-stream")]
		private static const AtlasSheet:Class;
		
		private var _atlas:TextureAtlas;
		
		private var _background:Image;
		private var _running:MovieClip;
		
		public function LoadingSequence() {
		}
		
		public override function prepare(am:AssetManager):void {
			
			var bitmap:Bitmap = new Atlas();
			var texture:Texture = Texture.fromBitmap(bitmap);
			
			var xml:XML = XML(new AtlasSheet());
			
			_atlas = new TextureAtlas(texture, xml);
			
			_background = new Image(_atlas.getTexture("BG"));
			_background.height = Constants.SCREEN_HEIGHT;
			addChild(_background);
			
			_running = new MovieClip(_atlas.getTextures("run_"), 30, false, true);		
			_running.x = Constants.SCREEN_WIDTH - _running.width;
			_running.y = Constants.SCREEN_HEIGHT - _running.height;
			addChild(_running);
			
			//doFadeIn(this, 200);
		}
		
		public override function initialize(am:AssetManager):void {
			_running.start();
		}
		
		public override function release():void {
			
			_background.dispose();
			_running.destroy();
		}
	}
}