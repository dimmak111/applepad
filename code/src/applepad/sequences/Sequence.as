package applepad.sequences
{	
	import applepad.AssetManager;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.FadeOut;
	import applepad.effects.IEffect;
	import applepad.effects.IFadeIn;
	import applepad.effects.IFadeOut;
	import applepad.utils.Vector2;
	
	import org.as3commons.collections.LinkedSet;
	import org.as3commons.collections.framework.IIterator;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;

	public class Sequence extends Sprite
	{		
		private var _effectList:LinkedSet;
		private var _userParams:Array;
		
		public function Sequence() {
			
			_effectList = new LinkedSet();
		}	
		
		public final function setUserParams(params:Array):void {
			_userParams = params;
		}
		
		public final function getUserParams():Array {
			return _userParams;
		}
		
		public final function getCurDifficulty():uint {
			return Game.getDifficulty();
		}
		
		public function prepare(_assetManager:AssetManager):void {			
		}
		
		public function initialize(_assetManager:AssetManager):void {
		}
		
		public function update(elapsedTime:Number):void {
		}
		
		public final function updateEffects(elapsedTime:Number):void {

			var it:IIterator = _effectList.iterator();
			
			while(it.hasNext()) {
				
				var e:IEffect = it.next();
				
				if(e.update(elapsedTime)) {
					_effectList.remove(e);
				}
			}
		}
		
		public function release():void {
		}
		
		protected final function changeSequence( name:String, userParams:Array = null ):void {
			dispatchEventWith(Event.COMPLETE, false, new SequenceParams(name, userParams));
		}
		
		public final function doFadeIn(toFade:DisplayObject, duration:Number = 1500, listener:IFadeIn = null):void {	
		
			_effectList.add(new FadeIn(toFade, duration, listener)); 
		}	
		
		public final function doFadeOut(toFade:DisplayObject, duration:Number = 1500, listener:IFadeOut = null):void {	
			
			_effectList.add(new FadeOut(toFade, duration, listener)); 
		}
	}
}