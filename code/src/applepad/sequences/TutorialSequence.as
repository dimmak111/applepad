package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	import applepad.utils.Vector2;
	
	import feathers.controls.Button;
	
	import flash.display3D.textures.Texture;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;

	public class TutorialSequence extends Sequence implements IFadeIn
	{		
		private const common:String = "default";
		private var custom:String;
		
		private var _skipButton:Button;
		
		private var _background:Image;
		private var _base:Image;
		
		private var _tutorial:MovieClip;
		
		private var _game:Image;
		private var _instructs:MovieClip;
		private var _char:MovieClip;
		private var _skip:MovieClip;
		private var _anim:MovieClip;
		private var _tweenPlay:Tween;
		
		private const OFFSET_BASE:uint = 100;
		private const OFFSET_TUTORIAL:uint = 100;
		private const OFFSET_GAME_X:uint = 170;
		private const OFFSET_GAME_Y:uint = 200;
		private const OFFSET_INSTRUCT_X:uint = 120;
		private const OFFSET_CHAR:uint = 40;
		private const OFFSET_SKIP_X:uint = 50;
		private const OFFSET_SKIP_Y:uint = 10;
		
		// use map.
		
		public function TutorialSequence() {
		}
		
		public override function prepare(assetManager:AssetManager):void {
			
			custom = getUserParams()[1];
			
			assetManager.loadAtlas("tutorials", common);
			assetManager.loadAtlas("tutorials", custom);
			assetManager.loadAtlas("tutorials", custom + "_anim");
			assetManager.loadAtlas("ui", "ui_atlas");
		}
		
		public override function initialize(assetManager:AssetManager):void {
			
			_background = new Image(assetManager.getTextureFromAtlas("tutorials", common, "BG"));
			
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			
			addChild(_background);
			
			_base = new Image(assetManager.getTextureFromAtlas("tutorials", common, "base"));
			
			_base.x = (Constants.SCREEN_WIDTH - base.width >> 1) + OFFSET_BASE;
			_base.y = -base.height;
			
			addChild(_base);
			
			_tutorial = assetManager.getMovieFromAtlas("tutorials", common, "Tuto_", 30);
			_tutorial.addEventListener(Event.COMPLETE, completedHandler);
			_tutorial.touchable = false;
			_tutorial.loop = false;
			_tutorial.x -= OFFSET_TUTORIAL;
			addChild(_tutorial);
			
			_tweenPlay = new Tween(null, 0);
			Starling.juggler.add(_tweenPlay);
			_game = assetManager.getImageFromAtlas("tutorials", custom, "Title");
			_game.x = _tutorial.x + OFFSET_GAME_X;
			_game.y = Constants.SCREEN_HEIGHT - OFFSET_GAME_Y;
			_game.scaleX = 0;
			_game.scaleY = 0;
			addChild(_game);
			
			/*_game = assetManager.getMovieFromAtlas("tutorials", custom, "Title_", 20);
			_game.addEventListener(Event.COMPLETE, completedHandler);
			_game.loop = false;
			_game.visible = false;
			_game.touchable = false;
			_game.x = _tutorial.x + OFFSET_GAME_X;
			_game.y = Constants.SCREEN_HEIGHT - OFFSET_GAME_Y;
			addChild(_game);*/
			
			
			
			_instructs = assetManager.getMovieFromAtlas("tutorials", custom, "text_", 20);
			_instructs.addEventListener(Event.COMPLETE, completedHandler);
			_instructs.touchable = false;
			_instructs.loop = false;
			_instructs.visible = false;
			
			
			_instructs.x = (_tutorial.width >> 1) - OFFSET_INSTRUCT_X;
			_instructs.y = (_tutorial.height >> 1);
			
			//PUTAIN DE GROS HACK
			if (custom == "bubble")
			{
				_instructs.x = (_tutorial.width >> 1) - OFFSET_INSTRUCT_X;
				_instructs.y = (_tutorial.height >> 1);
			}
			else if (custom == "dancepad")
			{
				_instructs.x = (_tutorial.width >> 1) - 160;
				_instructs.y = (_tutorial.height >> 1) - 10;
			}
			else
			{
				_instructs.x = (_tutorial.width >> 1) - 160;
				_instructs.y = (_tutorial.height >> 1) - 110;
			}
			//FIN GROS HACK

			addChild(_instructs);
			
			_char = assetManager.getMovieFromAtlas("tutorials", custom, "chara_", 25);
			_char.addEventListener(Event.COMPLETE, completedHandler);
			_char.touchable = false;
			_char.loop = false;
			_char.visible = false;
			_char.x = Constants.SCREEN_WIDTH - _char.width + OFFSET_CHAR;
			_char.y = Constants.SCREEN_HEIGHT - _char.height - OFFSET_CHAR;
			addChild(_char);
			
			_skip = assetManager.getMovieFromAtlas("tutorials", common, "skip_", 25);
			_skip.addEventListener(Event.COMPLETE, completedHandler);
			_skip.loop = false;
			_skip.visible = false;
			_skip.x = Constants.SCREEN_WIDTH - _skip.width - OFFSET_SKIP_X;
			_skip.y = Constants.SCREEN_HEIGHT - _skip.height - OFFSET_SKIP_Y;
			addChild(_skip);
			
			_anim = assetManager.getMovieFromAtlas("tutorials", custom + "_anim", "tuto_", 25);
			_anim.addEventListener(Event.COMPLETE, completedHandler);
			_anim.touchable = false;
			_anim.visible = false;
			_anim.loop = true;
			_anim.scaleX = 1.5;
			_anim.scaleY = 1.5;
			_anim.x = Constants.SCREEN_HALF_WIDTH - (_anim.width >> 1);
			_anim.y = Constants.SCREEN_HALF_HEIGHT - (_anim.height >> 2);
			addChild(_anim);
			
			
			_skipButton = new Button();
			
			addChild(_skipButton);
			
			_skipButton.addEventListener(Event.TRIGGERED, onSkipButton);
			_skipButton.validate();
			_skipButton.defaultSkin = new Image(assetManager.getTextureFromAtlas("ui", "ui_atlas", "skip"));
			_skipButton.downSkin = new Image(assetManager.getTextureFromAtlas("ui", "ui_atlas", "skip_hover"));
			_skipButton.x = _skip.x;
			_skipButton.y = _skip.y;
			_skipButton.visible = false;
			
			doFadeIn(this, 250, this);
		}
		
		public function tweenPlay (object:Image) : void {
			
			object.scaleX = 0;
			object.scaleY = 0;
			_tweenPlay.reset(object, 0.4, Transitions.EASE_OUT_BACK);
			_tweenPlay.scaleTo(1.0);
			Starling.juggler.add(_tweenPlay);
		}
		
		public function onSkipButton(e:Event):void {
			Game.getSoundManager().stopMusic("menu");
			changeSequence(getUserParams()[0] as String);
		}
		
		public function onFadeInFinished(e:FadeIn):void {
			
			var tween:Tween = new Tween(_base, 1, Transitions.EASE_IN);
			
			tween.moveTo(_base.x, _base.y+Constants.SCREEN_HEIGHT);
			tween.onComplete = completedTweenHandler;
			tween.onCompleteArgs = [tween];
			
			Starling.juggler.add(tween);
		}
				
		public function completedTweenHandler(t:Tween):void {
			
			if(t.target == _base) {
				
				_game.visible = true;
				
				Starling.juggler.add(_tutorial);
				tweenPlay(_game);
				//Starling.juggler.add(_game);	
			}
		}
		
		public function completedHandler(e:Event):void {
			
			Starling.juggler.remove(e.target as MovieClip);
			
			switch(e.target) {
				
				case _tutorial: 
				{
					_instructs.visible = true;
					Starling.juggler.add(_instructs);
					
					_char.visible = true;
					Starling.juggler.add(_char);
				}
				break;
				case _char:
				{
					_skip.visible = true;
					Starling.juggler.add(_skip);
				}
				break;
				case _skip: 
				{
					_anim.visible = true;
					Starling.juggler.add(_anim);
					_skipButton.visible = true;
				}
				break;
				case _anim:
				{
					Starling.juggler.add(_anim);
				}
				break;
				
			}
		}
		
		public override function release():void {
			
			_background.dispose()
			_base.dispose()
			_skipButton.dispose();
			
			_char.dispose()
			_game.dispose()
			_instructs.dispose();
			_skip.dispose();
			_tutorial.dispose();
		}
	}
}