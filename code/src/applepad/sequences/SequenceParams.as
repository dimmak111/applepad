package applepad.sequences
{
	public class SequenceParams
	{
		public var nextSequence:String;
		public var userParams:Array;
		
		public function SequenceParams(name:String, params:Array) {
			
			nextSequence = name;
			userParams = params;
		}
	}
}