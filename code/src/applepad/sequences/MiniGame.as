 package applepad.sequences
{
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.ui.Button;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;

	public class MiniGame extends Sequence
	{
		protected const MENU_BUTTON:uint 			= Constants.SCREEN_HALF_WIDTH;
		
		
		protected const GAME_STATE_INIT:uint 		= 0;
		protected const GAME_STATE_RESTART:uint 	= 1;
		protected const GAME_STATE_INTRO:uint 		= 2;
		protected const GAME_STATE_PAUSE:uint		= 3;
		protected const GAME_STATE_UPDATE:uint 		= 4;
		protected const GAME_STATE_FINISH:uint 		= 5;
		protected const GAME_STATE_CUSTOM:uint 		= 6;
		
		protected var _curState:uint;
		
		protected var _menuButton:TextField;
		protected var _selectButton:TextField;
		
		public function MiniGame() {
			
			_menuButton = new TextField(100, 50, "TITLE SCREEN");
			_menuButton.x = Constants.SCREEN_HALF_WIDTH - 100;
			_menuButton.border = true;
			_menuButton.addEventListener(TouchEvent.TOUCH, onDebugButton);
			
			_selectButton = new TextField(100, 50, "GAME SELECTION ");
			_selectButton.x = Constants.SCREEN_HALF_WIDTH;
			_selectButton.border = true;
			_selectButton.addEventListener(TouchEvent.TOUCH, onDebugButton);
		}
		
		public final override function prepare(am:AssetManager):void {
			prepareGame(am);
		}
		
		public final override function initialize(am:AssetManager):void {	
			
			_curState = GAME_STATE_INIT;
			
			initGame(am);
			
			addChild(_menuButton);
			addChild(_selectButton);
			
			switchGameState(GAME_STATE_RESTART);
		}
		
		public final override function update(elapsedTime:Number):void {
		
			if(getCurState() == GAME_STATE_PAUSE)
				return; 
			
			if(getCurState() == GAME_STATE_INTRO)
			{
				if(!introGame()) {
					switchGameState(GAME_STATE_UPDATE);	
				}
			}
			else if(getCurState() == GAME_STATE_UPDATE) 
			{
				if(!updateGame(elapsedTime)) {
					switchGameState(GAME_STATE_FINISH);
				}
			}
			else if(getCurState() == GAME_STATE_CUSTOM) {
				updateCustom(elapsedTime)
			}
		}
		
		public final override function release():void {	
			
			releaseGame();
			
			removeChild(_menuButton);
			removeChild(_selectButton);
		}
		
		public final function touch(e:TouchEvent):void {
			
		}
		
		public final function onDebugButton(e:TouchEvent): void {
			
			var t:Touch = e.touches[0];
			if(t.phase == TouchPhase.ENDED) {
				if(e.currentTarget == _menuButton) {
					changeSequence("MenuSequence");
				}
				
				if(e.currentTarget == _selectButton) {
					changeSequence("SelectSequence");
				}
			}
		}
		
		protected function switchGameState(state:uint):void {
			
			if(_curState == state)
				return;
			
			_curState = state;
			
			switch(_curState) 
			{	
				case GAME_STATE_RESTART:
					restartGame();
					switchGameState(GAME_STATE_INTRO);
				break;
				
				case GAME_STATE_INTRO:
				break;
				
				case GAME_STATE_UPDATE:
				break;
				
				case GAME_STATE_FINISH:
					finishGame();
				break;
				
				case GAME_STATE_CUSTOM:
				break;
			}
		}
		
		protected final function getCurState():uint {
			return _curState;
		}
		
		protected function prepareGame(am:AssetManager):void {
		}
		
		protected function initGame(am:AssetManager):void {
		}
		
		protected function restartGame():void {	
		}
		
		protected function introGame():Boolean {
			return false;
		}
		
		protected function updateGame(elapsedTime:Number):Boolean {	
			return false;
		}
		
		protected function updateCustom(elapsedTime:Number):void {
		}
		
		protected function finishGame():void {
		}
		protected function releaseGame():void {
		}
	}
}