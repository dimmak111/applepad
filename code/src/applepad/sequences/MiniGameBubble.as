package applepad.sequences
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Joints.b2Joint;
	import Box2D.Dynamics.Joints.b2JointDef;
	import Box2D.Dynamics.Joints.b2MouseJointDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	
	import applepad.AssetEmbed;
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	import applepad.entities.MovieClip;
	import applepad.utils.MathUtils;
	import applepad.utils.Vector2;
	import applepad.utils.VectorUtils;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.deg2rad;

	public class MiniGameBubble extends MiniGame implements IFadeIn
	{
		//const
		private const DEBUG_MODE:Boolean				= false;
		
		private const TEXTURE_ATLAS_BUBBLE:String		= "bubble_atlas";
		private const TEXTURE_ATLAS_OBSTACLES:String	= "bubble_obstacles_atlas";
		private const TEXTURE_ATLAS_BACK:String 		= "bubble_back_atlas";
		private const TEXTURE_ATLAS_HUD:String 			= "bubble_hud_atlas";
		private const TEXTURE_ATLAS_STARTER:String		= "bubble_starter_atlas";
		private const XML_THEMES:String					= "bubble_themes";
		private const XML_LEVELS:String					= "bubble_levels";
		
		private const STARTER_3:uint 					= 0;
		private const STARTER_2:uint 					= 1;
		private const STARTER_1:uint 					= 2;
		private const STARTER_GO:uint		 			= 3;
		
		private const PANCARTE_OFFSET_X:int				= 60;
		private const PANCARTE_OFFSET_Y:int				= -85;
		
		private const SPLAT_OFFSET_X:int				= -20;
		
		private const TIME_BEFORE_RESTART:uint			= 100;
		
		private var starterName:Array = new Array("03_", "02_", "01_", "go_");
		
		// game const
		private const GAME_STATE_RESTART_ROUND:uint 	= GAME_STATE_CUSTOM+1;
		
		private const NUM_ROUND_MAX:uint				= 5;
		private const NUM_BUBBLE_MAX:uint 				= 8;
		
		// datas
		private var _starter:Vector.<MovieClip>;
		private var _background:Image;
		
		private var _bubblesAtlas:TextureAtlas;
		private var _obstaclesAtlas:TextureAtlas;
		
		private var _themes:XML;
		private var _levels:XML;
		
		// game vars
		private var _scale:Number;
		private var _worldFactory:WorldFactory;
		private var _world:b2World;
		
		private var _corners:Vector.<Corner>;
		private var _bubbles:Vector.<Bubble>;
		private var _rigids:Vector.<b2Body>;
		
		private var _isFirstFound:Boolean;
		private var _countTimeLeft:int;
		
		private var _wordsToFind:Vector.<String>;
		private var _roundsPlayed:uint;
		
		private var _scores:Vector.<Number>;
		private var _texts:Vector.<TextField>;
		
		// hud
		private var _pancartes:Vector.<MovieClip>;
		private var _avatars:Vector.<Avatar>;
		
		// debug
		private var _debugSprite:flash.display.Sprite;
		
		public function MiniGameBubble() {	
				
			_corners = new Vector.<Corner>(Constants.NUM_PLAYER_MAX);
			_bubbles = new Vector.<Bubble>(NUM_BUBBLE_MAX);
			_wordsToFind = new Vector.<String>(NUM_BUBBLE_MAX);
			_rigids = new Vector.<b2Body>();	
			
			//hud
			_starter = new Vector.<MovieClip>(STARTER_GO+1);
			_pancartes = new Vector.<MovieClip>(Constants.NUM_PLAYER_MAX);
			_avatars = new Vector.<Avatar>(Constants.NUM_PLAYER_MAX);
			
			//scoring
			_scores = new Vector.<Number>(Constants.NUM_PLAYER_MAX);
			_texts = new Vector.<TextField>(Constants.NUM_PLAYER_MAX);
		}
		
		protected override function prepareGame(am:AssetManager):void {
			
			am.loadAtlas("minigame_bubble", TEXTURE_ATLAS_BUBBLE);
			am.loadAtlas("minigame_bubble", TEXTURE_ATLAS_OBSTACLES);
			am.loadAtlas("minigame_bubble", TEXTURE_ATLAS_BACK);
			am.loadAtlas("minigame_bubble", TEXTURE_ATLAS_HUD);
			am.loadAtlas("minigame_bubble", TEXTURE_ATLAS_STARTER);
			am.loadAtlas("ui", "char_faces_atlas");
			
			_themes = am.loadXML("xml", XML_THEMES);
			_levels = am.loadXML("xml", XML_LEVELS);
		}
		
		protected override function initGame(am:AssetManager):void {
			
			_background = am.getImageFromAtlas("minigame_bubble", TEXTURE_ATLAS_BACK, "background_01");
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			_background.height = Constants.SCREEN_HEIGHT;
			
			addChild(_background);
			
			_world = new b2World(b2Vec2.Make(0, 0), true);
			_worldFactory = new WorldFactory(_world);
			
			var i:uint = 0;
			
			//init bubbles
			_bubblesAtlas = am.getTextureAtlas("minigame_bubble", TEXTURE_ATLAS_BUBBLE);
			for(i = 0; i < NUM_BUBBLE_MAX; ++i) {
				_bubbles[i] = new Bubble(_world);
				_bubbles[i].visible = false;
				addChild(_bubbles[i]);
			}
			
			//init corners
			var cornerPos:Vector.<Vector2> = new Vector.<Vector2>();
			cornerPos.push(new Vector2(0, 0),
						   new Vector2(Constants.SCREEN_WIDTH, 0),
						   new Vector2(0, Constants.SCREEN_HEIGHT),
						   new Vector2(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT));
						   
			var cornerRot:Vector.<Vector2> = new Vector.<Vector2>();
			cornerRot.push(new Vector2(1, 1),
						   new Vector2(-1, 1),
						   new Vector2(1, -1),
						   new Vector2(-1, -1));
			
			
			var textCorner:Texture = am.getTextureFromAtlas("minigame_bubble", TEXTURE_ATLAS_BACK, "corner_01");
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				
				_corners[i] = new Corner();
				_corners[i].skin = new Image(textCorner);
				
				_corners[i].x = cornerPos[i].x;
				_corners[i].y = cornerPos[i].y;
				
				_corners[i].scaleX = cornerRot[i].x;
				_corners[i].scaleY = cornerRot[i].y;
				
				addChild(_corners[i]);
			}
			
			// obstacles
			_obstaclesAtlas = am.getTextureAtlas("minigame_bubble", TEXTURE_ATLAS_OBSTACLES);
			
			// hud, starter
			for (i = 0 ; i < STARTER_GO+1; ++i) {
				
				var mv:MovieClip = am.getMovieFromAtlas("minigame_bubble", TEXTURE_ATLAS_STARTER, starterName[i], 15);
				mv.addEventListener(Event.COMPLETE, completeHandler);
				mv.loop = false;
				mv.touchable = false;			
				mv.visible = false;
				mv.x = Constants.SCREEN_HALF_WIDTH - (mv.width >> 1);
				mv.y = Constants.SCREEN_HALF_HEIGHT - (mv.height >> 1);	
				
				mv.stop();
				
				_starter[i] = mv;
				
				addChild(mv);
				Starling.juggler.add(mv);
			}
			
			// avatars
			var textSplat:Texture = am.getTextureFromAtlas("minigame_bubble", TEXTURE_ATLAS_HUD, "splat_00008");
			var texts:Vector.<Texture> = am.getTexturesFromAtlas("minigame_bubble", TEXTURE_ATLAS_HUD, "pancarte_");
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				
				var textFace:Texture = am.getTextureFromAtlas("ui", "char_faces_atlas", "char_0"+Game.getPlayer(i).avatar);
				
				var imgSplate:Image = new Image(textSplat);
				var imgFace:Image = new Image(textFace);
				
				_avatars[i] = new Avatar(imgFace, imgSplate, i);
				_avatars[i].x = (_corners[i].width >> 1) - 15;
				_avatars[i].y = (_corners[i].height >> 1) - 15;
				
				_pancartes[i] = new MovieClip(texts, 25, false, false);
				_pancartes[i].addEventListener(Event.COMPLETE, function():void {
					for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) { _texts[i].visible = true; }
				});
					
				_pancartes[i].visible = false;
				
				_pancartes[i].x = PANCARTE_OFFSET_X;
				_pancartes[i].y = PANCARTE_OFFSET_Y;
				
				_corners[i].addChild(_avatars[i]);
				_corners[i].addChild(_pancartes[i]);
				
				// player names 
				
				/*
				var text:TextField = new TextField(100, 50, "Player "+(i+1), AssetEmbed.getFont("grobold").fontName, 20, 0xFFFFFF);
				// position
				text.x = (i % 2 <= 0) ? _corners[i].x : _corners[i].x - text.width;
				text.y = (i < 2) ? _corners[i].y + 20 : _corners[i].y - text.height - 20;
				// rotate
				if(i < 2) {
					text.scaleY = -1;
					text.scaleX = -1;
					text.x += text.width;
					text.y += text.height;
				}
				addChild(text);	*/
			}
			
			//texts
			var _textPos:Vector.<Vector2> = new Vector.<Vector2>();
			_textPos.push(new Vector2(240, 65),
						  new Vector2(Constants.SCREEN_WIDTH-240, 65),
						  new Vector2(240, Constants.SCREEN_HEIGHT-65),
						  new Vector2(Constants.SCREEN_WIDTH-240, Constants.SCREEN_HEIGHT-65));
			
			var _textRot:Vector.<int> = new Vector.<int>();
			_textRot.push(-15, 15, 15, -15);
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				
				_texts[i] = new TextField(150, 25, "Bathroom", AssetEmbed.getFont("grobold").fontName, 20, 0x813514);
				addChild(_texts[i]);
				
				_texts[i].pivotX = _texts[i].width >> 1;
				_texts[i].pivotY = _texts[i].height >> 1;
				
				_texts[i].x = _textPos[i].x;
				_texts[i].y = _textPos[i].y;
				_texts[i].rotation = deg2rad(_textRot[i]);
				
				if(i < (Constants.NUM_PLAYER_MAX >> 1)) {
						
					_texts[i].scaleX = -1;
					_texts[i].scaleY = -1;
					
					//_texts[i].x -= _texts[i].width;
					//_texts[i].y -= _texts[i].height;
				}
				
				_texts[i].visible = false;
			}
			
			if(DEBUG_MODE) {
				prepareDebug();
			}
		}
		
		protected override function switchGameState(state:uint):void {
			
			super.switchGameState(state);
			
			switch(state) {
				
				case GAME_STATE_RESTART_ROUND:
				{
					restartRound();
					super.switchGameState(GAME_STATE_INTRO);
				}	
				break;	
			}
		}
		
		public function prepareDebug():void {
			
			var debugDraw:b2DebugDraw = new b2DebugDraw();
			_debugSprite = new flash.display.Sprite();
			
			Starling.current.nativeOverlay.addChild(_debugSprite);
			
			debugDraw.SetSprite(_debugSprite);
			debugDraw.SetDrawScale(Constants.PIXEL_TO_METER);
			debugDraw.SetFlags(b2DebugDraw.e_shapeBit|b2DebugDraw.e_jointBit);
			debugDraw.SetFillAlpha(0.4);
			
			_world.SetDebugDraw(debugDraw);
		}
		
		
		protected override function restartGame():void {
			
			_roundsPlayed = 0;
			
			restartScores();
			restartRound();
		}
		
		private function restartScores():void {
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_scores[i] = 0;
			}
		}
		
		private function restartRound():void {
			
			_isFirstFound = false;
			_countTimeLeft = TIME_BEFORE_RESTART;
			
			// find a theme
			var nbThemes:uint = _themes.THEME.length();
			var nextTheme:uint = MathUtils.randi(0, nbThemes);
			var themeName:String = _themes.THEME[nextTheme].@ID;
			
			var image_id:Vector.<uint> = new Vector.<uint>(); 
			
			var i:uint = 0;
			
			for(i = 0; i < NUM_BUBBLE_MAX; ++i) {
				_wordsToFind[i] = _themes.THEME[nextTheme].ITEM[i];
				image_id.push(_themes.THEME[nextTheme].ITEM[i].@ID);
			}
			
			var index:Vector.<uint> = new Vector.<uint>();
			for(i = 0; i < NUM_BUBBLE_MAX; ++i) {
				index.push(i);
			}
			
			VectorUtils.shuffle(index);
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_corners[i].type = index[i];
				_pancartes[i].visible = false;
				_texts[i].visible = false;
				_texts[i].text = _wordsToFind[index[i]];
			}
					
			for(i = 0; i < NUM_BUBBLE_MAX; ++i) {
				
				_bubbles[i].visible = false;
				_bubbles[i].clear();
				_bubbles[i].skin = new Image(_bubblesAtlas.getTexture(themeName+"_"+image_id[index[i]]));
				
				_bubbles[i].pos = b2Vec2.Make(MathUtils.randi(Constants.SCREEN_HALF_WIDTH-100, Constants.SCREEN_HALF_WIDTH+100), 
											  MathUtils.randi(Constants.SCREEN_HALF_HEIGHT-100, Constants.SCREEN_HALF_HEIGHT+100) );
				
				_bubbles[i].body.SetLinearVelocity(b2Vec2.Make (  MathUtils.randi(-150, 150) / 30,  MathUtils.randi(-150, 150) / 30) );
				_bubbles[i].type = index[i];
			}
			
			prepareLevel();
			
			doFadeIn(this, 250, this);	
		}
		
		protected function prepareLevel():void {
			
			//prepare walls
			_rigids.push(_worldFactory.CreateQuad(0,0,1,Constants.SCREEN_HEIGHT));
			_rigids.push(_worldFactory.CreateQuad(0,0,Constants.SCREEN_WIDTH,1));
			_rigids.push(_worldFactory.CreateQuad(0,Constants.SCREEN_HEIGHT,Constants.SCREEN_WIDTH,1));
			_rigids.push(_worldFactory.CreateQuad(Constants.SCREEN_WIDTH,0,1,Constants.SCREEN_HEIGHT));
			
			//bounds
			_rigids.push(_worldFactory.CreateQuad(235, 48, 5, 40, -40));
			_rigids.push(_worldFactory.CreateQuad(50, 228, 5, 40, -40));
			
			_rigids.push(_worldFactory.CreateQuad(Constants.SCREEN_WIDTH-235, 48, 5, 40, 40));
			_rigids.push(_worldFactory.CreateQuad(Constants.SCREEN_WIDTH-50, 228, 5, 40, 40));
			
			_rigids.push(_worldFactory.CreateQuad(Constants.SCREEN_WIDTH-50, Constants.SCREEN_HEIGHT-228, 5, 40, -40));
			_rigids.push(_worldFactory.CreateQuad(Constants.SCREEN_WIDTH-235,  Constants.SCREEN_HEIGHT-48, 5, 40, -40));
			
			_rigids.push(_worldFactory.CreateQuad(235, Constants.SCREEN_HEIGHT-48, 5, 40, 40));
			_rigids.push(_worldFactory.CreateQuad(50, Constants.SCREEN_HEIGHT-228, 5, 40, 40));
			
			//prepare obstacles
			var level:uint = MathUtils.randi(0, _levels.LEVEL.length()-2);
			var shapes:XMLList = _levels.LEVEL[level].SHAPE;
			
			for(var i:uint = 0; i < shapes.length(); ++i) {
					
				var skin:Image; 
				
				if(shapes[i].@TYPE.toString() == "QUAD") {
					
					skin = new Image(_obstaclesAtlas.getTexture("obstacle_"+shapes[i].@SKIN));
					addChildAt(skin, getChildIndex(_background) + 1);
					
					_rigids.push(_worldFactory.CreateQuad(shapes[i].POSITION.@X,
														  shapes[i].POSITION.@Y,
														  shapes[i].SIZE.@WIDTH,
														  shapes[i].SIZE.@HEIGHT, 
														  shapes[i].ROTATION,
														  skin));
				}
				else if(shapes[i].@TYPE.toString() == "CIRCLE") {
					
					skin = new Image(_obstaclesAtlas.getTexture("circle"));
					addChildAt(skin, getChildIndex(_background) + 1);
					
					_rigids.push(_worldFactory.CreateCircle(shapes[i].POSITION.@X,
															shapes[i].POSITION.@Y,
															shapes[i].RADIUS, 
															skin));
				}
			}
		}
		
		protected function releaseLevel():void {
			
			for(var i:uint = 0; i < _rigids.length; ++i) {
				_world.DestroyBody(_rigids[i]);
				removeChild(_rigids[i].GetUserData());
			}
			_rigids.length = 0;
		}
		
		protected override function introGame():Boolean {
			return true;
		}
		
		protected override function updateGame(elapsedTime:Number):Boolean {
			
			_world.Step(1/30,10,10);
			_world.ClearForces();
			
			// time limit
			if(_isFirstFound) {
				_countTimeLeft -= elapsedTime;
				if(_countTimeLeft <= 0)
					onRoundFinished();
			}
			
			// normal mode
			for(var i:uint = 0; i < NUM_BUBBLE_MAX; ++i) {
				
				if(!_bubbles[i].visible)
					continue;
				
				_bubbles[i].update();
				
				for(var j:uint = 0; j < Constants.NUM_PLAYER_MAX; ++j) {
					if(_corners[j].isInside(_bubbles[i])) {
						if(_corners[j].type == _bubbles[i].type) {
							onBubbleGood(j, i);
							//onRoundFinished();
						}
						else {
							Game.getSoundManager().playSound("bubble_wrong");
							_bubbles[i].clear();
							_bubbles[i].pos = b2Vec2.Make(Constants.SCREEN_HALF_WIDTH, Constants.SCREEN_HALF_HEIGHT); 
						}
					}
				}
			}
			
			_world.DrawDebugData();
			
			return true;
		}
		
		protected override function finishGame():void {
			calculateScore();
			changeSequence("ResultSequence", [Constants.NUM_PLAYER_MAX, _scores]);
		}
		
		protected override function releaseGame():void {
			Game.getSoundManager().stopMusic("bubble");
			for(var i:uint = 0; i < STARTER_GO+1; ++i) {
				Starling.juggler.remove(_starter[i]);
				
				_starter[i].dispose();
				_starter[i] = null;
			}
		}
		
		protected function completeHandler(e:Event):void {
			
			var mv:MovieClip = e.target as MovieClip;
			
			switch(mv) {
				
				case _starter[STARTER_3]:
					Game.getSoundManager().playSound("bubble_start");
					_starter[STARTER_3].visible = false;
					_starter[STARTER_3].stop();
					_starter[STARTER_2].visible = true;
					_starter[STARTER_2].play();
				break;
				case _starter[STARTER_2]:
					_starter[STARTER_2].visible = false;
					_starter[STARTER_2].stop();
					_starter[STARTER_1].visible = true;
					_starter[STARTER_1].play();
				break;
				case _starter[STARTER_1]:
					_starter[STARTER_1].visible = false;
					_starter[STARTER_1].stop();
					_starter[STARTER_GO].visible = true;
					_starter[STARTER_GO].play();
				break;
				case _starter[STARTER_GO]:
					
					_starter[STARTER_GO].stop();
					_starter[STARTER_GO].visible = false;
					
					var i:uint = 0;
					
					for(i = 0; i < NUM_BUBBLE_MAX; ++i) {
						_bubbles[i].visible = true;
					}
					
					for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
						_pancartes[i].visible = true;
						_pancartes[i].start();
						_avatars[i].reset();
					}
					
					Game.getSoundManager().playMusic("bubble");
					switchGameState(GAME_STATE_UPDATE);		
				break;
			}
		}
		
		public function onBubbleGood(idxCorner:uint, idxBubble:uint):void {
			++_scores[idxCorner];
			_bubbles[idxBubble].visible = false;
			
			if(!_isFirstFound) {
				_isFirstFound = true;
			}
		}
	
		
		public function onRoundFinished():void {
			++_roundsPlayed; 
			Game.getSoundManager().stopMusic("bubble");
			if(_roundsPlayed >= NUM_ROUND_MAX) {
				switchGameState(GAME_STATE_FINISH);		
			}
			else {
				
				releaseLevel();
				switchGameState(GAME_STATE_RESTART_ROUND);		
			}
		}
		
		public function onFadeInFinished(e:FadeIn):void {
			
			_starter[STARTER_3].visible = true;
			_starter[STARTER_3].play();
		}
		
		public function calculateScore():void {
			
			for(var i:uint = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				_scores[i] /= NUM_ROUND_MAX;
			}
		}
	}
}

import Box2D.Collision.Shapes.b2CircleShape;
import Box2D.Collision.Shapes.b2PolygonShape;
import Box2D.Common.Math.b2Vec2;
import Box2D.Dynamics.Joints.b2MouseJoint;
import Box2D.Dynamics.Joints.b2MouseJointDef;
import Box2D.Dynamics.b2Body;
import Box2D.Dynamics.b2BodyDef;
import Box2D.Dynamics.b2FixtureDef;
import Box2D.Dynamics.b2World;

import applepad.AssetEmbed;
import applepad.entities.MovieClip;
import applepad.utils.MathUtils;
import applepad.utils.Vector2;

import flash.geom.Point;
import flash.geom.Rectangle;

import org.as3commons.collections.Map;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;
import starling.utils.deg2rad;
import starling.utils.rad2deg;

internal class Skinnable extends Sprite {
	
	protected var _skin:Image;
	
	public function set skin(skin:Image):void {
		
		if( _skin == skin)
			return;
		
		removeChild(_skin);
		_skin = skin;
		addChild(_skin);
	}
	
	public function get skin():Image { return _skin; }
	
}

internal class Avatar extends Skinnable {
	
	private var _avatar:Image;
	private var _splat:Image;
	
	public function Avatar(avatar:Image, splat:Image, id:uint):void {
		
		_splat = splat;
		_splat.x = -20;
		_splat.y = avatar.height - 40;
		
		_avatar = avatar;
		
		addChild(_avatar);
		addChild(_splat);
		
		pivotX = width >> 1;
		pivotY = height >> 1;
		
		scaleY = -1;
	}
	
	public function reset():void {
	}
}

internal class Bubble extends Skinnable {
	
	private var _body:b2Body;
	private var _type:uint;
	
	private var _touched:Boolean;
	private var _touchJoint:b2MouseJoint;
	private var _touchDef:b2MouseJointDef;
	
	public function Bubble(_world:b2World) {
		
		var bodyDef:b2BodyDef = new b2BodyDef();
		var circleShape:b2CircleShape = new b2CircleShape(50.0/30);
		var fixtureDef:b2FixtureDef = new b2FixtureDef();
		
		bodyDef.type = b2Body.b2_dynamicBody;
		bodyDef.position.Set(0, 0);
		bodyDef.linearDamping = 0.8;
		bodyDef.angularDamping = 0.4;
		bodyDef.userData this;
		
		fixtureDef.shape = circleShape;
		fixtureDef.density = 1.0;
		fixtureDef.friction = 0.5;
		fixtureDef.restitution = 0.2;
		
		_body = _world.CreateBody(bodyDef);
		_body.CreateFixture(fixtureDef);
		_body.SetAngle(rad2deg(MathUtils.randi(0, 360)));
		
		_touched = false;
		_touchJoint = null;
		_touchDef = new b2MouseJointDef();
		
		addEventListener(TouchEvent.TOUCH, touch);
	}
	
	public function clear():void {
		
		_touched = false;
		
		if(_touchJoint) {
		
			body.GetWorld().DestroyJoint(_touchJoint);
			_touchJoint=null;
		}
		
		body.SetLinearVelocity(b2Vec2.Make(0, 0));
	}
	
	public function update():void {
		
		x = _body.GetPosition().x * 30; 
		y = _body.GetPosition().y * 30; 
		
		rotation = _body.GetAngle();
	}
	
	public function get touched():Boolean {
		return _touched;
	}
	
	public function set type(type:uint):void { _type = type; }
	public function get type():uint { return _type; }
	
	public function get body():b2Body { return _body; }
	
	public function set pos(vec:b2Vec2):void {
		
		x = vec.x;
		y = vec.y;
		
		_body.SetPosition(new b2Vec2(x/30, y/30));	
	}
	
	public override function set skin(s:Image):void {

		if(s == skin)
			return;
		
		removeChild(skin);
		_skin = s;
		
		s.pivotX = s.width >> 1;
		s.pivotY = s.height >> 1;
		
		addChild(_skin);
	}
	
	public function touch(e:TouchEvent):void {
		
		var touch:Touch = e.getTouch(this);
		if(touch == null) return;
		
		var pos:Point = touch.getLocation(Starling.current.stage);
		var world:b2World = _body.GetWorld();
		
		if(touch.phase == TouchPhase.BEGAN) {
			
			_touched = true;
			
			_touchDef.bodyA 	= world.GetGroundBody();
			_touchDef.bodyB 	= _body;
			_touchDef.target	= convertTouch(pos);
			_touchDef.maxForce	= 1000*_body.GetMass();
			
			_touchJoint = world.CreateJoint(_touchDef) as b2MouseJoint;
			
		}
		else if(touch.phase == TouchPhase.MOVED && _touched) {
			
			_touchJoint.SetTarget(convertTouch(pos));
			
		}
		else if(touch.phase == TouchPhase.ENDED) {
			
			if(_touched) {
				_touched = false;
			
				world.DestroyJoint(_touchJoint);
				_touchJoint=null;
			}
		}
	}
	
	private function convertTouch(p:Point):b2Vec2 {
		return b2Vec2.Make(p.x / 30, p.y / 30);
	}
	
	public function destroy():void {
		removeEventListeners();
	}
}

internal class Corner extends Skinnable {
	
	private var _type:uint;
	private var _size:Vector2;
	
	public function Corner() {
		
		_size = new Vector2(210, 260);
	}
	
	public function set type(t:uint):void { _type = t; }
	public function get type():uint { return _type; }
	
	public function isInside(bubble:Bubble):Boolean {
		
		var radius:Number = _size.x;
		return  ((x-bubble.x)*(x-bubble.x) + (y-bubble.y)*(y-bubble.y) <= radius * radius);
	}
	
	public override function hitTest(localPoint:Point, forTouch:Boolean=false):DisplayObject {
			
		if (forTouch && (!visible || !touchable)) 
			return null; 
		
		var sqDist:Number = Math.pow(localPoint.x, 2) + 
			Math.pow(localPoint.y, 2);
		
		var radius:Number = _size.x;
		if (sqDist < Math.pow(radius, 2)) return this;
		else return null;
	}
}

internal class WorldFactory {

	private var worldRef:b2World;
	
	public function WorldFactory(w:b2World) {
		worldRef = w;
	}
	
	public function CreateQuad(x:Number, y:Number, w:Number, h:Number, rot:Number = 0, ref:Image = null):b2Body {
		
		var body:b2Body;
		var boxShape:b2PolygonShape = new b2PolygonShape();
		var bodyDef:b2BodyDef = new b2BodyDef();
		var fixtureDef:b2FixtureDef = new b2FixtureDef();
		
		fixtureDef.density=2;
		fixtureDef.friction=0.5;
		fixtureDef.restitution=0.5;
		
		bodyDef.type = b2Body.b2_staticBody;	
		
		bodyDef.position.Set(x/30, y/30);
		bodyDef.angle = deg2rad(rot);
		fixtureDef.shape = boxShape;
		boxShape.SetAsBox(w/30.0, h/30);
		
		if(ref != null) {
			
			ref.x = x;
			ref.y = y;
			ref.pivotX = ref.width >> 1;
			ref.pivotY = ref.height >> 1;
			ref.rotation = deg2rad(rot);
			bodyDef.userData = ref;
		}
		
		body = worldRef.CreateBody(bodyDef);
		body.CreateFixture(fixtureDef);
		
		return body;
	}
	
	public function CreateCircle(x:Number, y:Number, radius:Number, ref:Image = null):b2Body {
		
		var body:b2Body;
		var circleShape:b2CircleShape = new b2CircleShape(radius/30);
		var bodyDef:b2BodyDef = new b2BodyDef();
		var fixtureDef:b2FixtureDef = new b2FixtureDef();
		
		fixtureDef.density=2;
		fixtureDef.friction=0.5;
		fixtureDef.restitution=0.5;
		
		bodyDef.type = b2Body.b2_staticBody;	
		bodyDef.position.Set(x/30, y/30);
		
		if(ref) {
			
			ref.x = x;
			ref.y = y;
			
			ref.pivotX = ref.width >> 1;
			ref.pivotY = ref.height >> 1;
			
			bodyDef.userData = ref;
		}
		
		fixtureDef.shape = circleShape;
		
		body = worldRef.CreateBody(bodyDef);
		body.CreateFixture(fixtureDef);
		
		return body;
	}
}