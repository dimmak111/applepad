package applepad.sequences
{
	import applepad.AssetManager;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class AchievementSequence extends Sequence
	{
		
		private var _fake:Image;
		
		public function AchievementSequence() {
		}
		
		public override function prepare(am:AssetManager):void {
			
			am.loadTexture("achievement", "fake_trophy");
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public override function initialize(am:AssetManager):void {
			
			_fake = new Image(am.getTexture("achievement", "fake_trophy"));
			addChild(_fake);
		}
		
		public override function release():void {
			_fake.dispose();
		}
		
		public function onTouch(e:TouchEvent):void {
			
			var t:Touch = e.touches[0];
			
			if(t.phase == TouchPhase.ENDED) {
				changeSequence("MenuSequence");
			}
		}
	}
}