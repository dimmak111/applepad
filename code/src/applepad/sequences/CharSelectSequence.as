package applepad.sequences
{
	import applepad.AssetEmbed;
	import applepad.AssetManager;
	import applepad.Constants;
	import applepad.Game;
	import applepad.effects.Effect;
	import applepad.effects.FadeIn;
	import applepad.effects.IFadeIn;
	import applepad.entities.MovieClip;
	import applepad.ui.Button;
	import applepad.ui.IButton;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;

	public class CharSelectSequence extends Sequence implements IButton, IFadeIn
	{
		private var _characterContainer:CharacterContainer;
		private var _button:Button;
		
		private var _background:Image;
		private var _splat:Image;
		private var _sidebar:MovieClip;
		
		private var _textCur:TextField;
		
		public function CharSelectSequence() {
		}
		
		public override function prepare(am:AssetManager):void {
			
			am.loadAtlas("char_select", "char_select_atlas");
			am.loadAtlas("ui", "ui_atlas");
		}
		
		public override function initialize(am:AssetManager):void {	
			
			_background = am.getImageFromAtlas("char_select", "char_select_atlas", "BG");
			_background.blendMode = BlendMode.NONE;
			_background.touchable = false;
			_background.height = Constants.SCREEN_HEIGHT;
			addChild(_background);
			
			_button = new Button(am.getTextureFromAtlas("ui", "ui_atlas", "ok"), am.getTextureFromAtlas("ui", "ui_atlas", "ok_hover"), this);
			
			_button.x = Constants.SCREEN_WIDTH - 200;
			_button.y = Constants.SCREEN_HEIGHT - 200;
			_button.pivotX = _button.width >> 1;
			_button.pivotY = _button.height >> 1;
			_button.scaleX = 0.1;
			_button.scaleY = 0.1;			
			
			var char:Vector.<Character>;
			char = new Vector.<Character>();
			
			var tokens:Vector.<Image>;
			tokens = new Vector.<Image>();
			
			var i:uint = 0;
			
			for(i = 0; i < Constants.NUM_CHARACTER_MAX; ++i) {
				
				var charName:String = "character_0"+(i+1);
				char.push(new Character(i, am.getImageFromAtlas("char_select", "char_select_atlas", charName)));	
			}
			
			for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
				var tokenName:String = "token_0"+(i+1);
				tokens.push(am.getImageFromAtlas("char_select", "char_select_atlas", tokenName));
			}
			
			_characterContainer = new CharacterContainer(char, tokens);
			_characterContainer.y = 300;
			_characterContainer.x = 80+Constants.SCREEN_HALF_WIDTH - (_characterContainer.width >> 1);
			
			addChild(_characterContainer);
			
			_sidebar = am.getMovieFromAtlas("char_select", "char_select_atlas", "sidebar_",20, false, false);
			_sidebar.y = Constants.SCREEN_HALF_HEIGHT + 150;
						
			_splat = am.getImageFromAtlas("char_select", "char_select_atlas", "splat");
			_splat.x = - _splat.width;
			_splat.height =  Constants.SCREEN_HEIGHT;
			
			addChild(_splat);
			addChild(_sidebar);	
			addChild(_button);
			
			_textCur = new TextField(450, 200, "SELECT PLAYER 1", AssetEmbed.getFont("grobold").fontName, 45, 0x652804);
			_textCur.x = Constants.SCREEN_WIDTH - _textCur.width >> 1;
			_textCur.y = Constants.SCREEN_HEIGHT - _textCur.height - 25;
			addChild(_textCur);
			
			doFadeIn(this, 250, this);
		}
		
		public function onFadeInFinished (e:FadeIn) : void {
			
			Effect.CreateMoveTo(_splat, -22, 0, 0.4, Transitions.EASE_IN);
			Effect.CreateScaleTo(_button, 1.0, 0.6, Transitions.EASE_OUT_BACK);
			
			_sidebar.start();
		}
		
		public function onButton(b:Button):void {
			
			if(b.state == Button.BUTTON_NORMAL) {
				Game.getPlayer(_characterContainer.selected).avatar = _characterContainer.getCurChar();
				_characterContainer.selected = _characterContainer.getCurChar();
				
				_textCur.text = "SELECT PLAYER " + (_characterContainer.selected+1);
				
				if(_characterContainer.selected == Constants.NUM_PLAYER_MAX) {
					changeSequence("SelectSequence");
				}
			}
		}
		
		public override function release():void {
			_characterContainer.destroy();	
		}
	}
}

import applepad.Constants;
import applepad.utils.Vector2;

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

internal class Character extends Sprite {
	
	private var _enabled:Boolean;
	private var _selected:Boolean;
	private var _id:uint;
	
	private var _tween:Tween;
	
	public function Character(id:uint, image:Image) {
		
		_id = id;
		_enabled = true;
	
		image.pivotX = image.width >> 1;
		image.pivotY = image.height >> 1;
		
		addChild(image);
		
		_tween = new Tween(this, 0.0, Transitions.EASE_IN);
	}
	
	public function set enable(state:Boolean):void 	{
		
		_enabled = state;
		
		_tween.reset(this, 0.2, Transitions.EASE_OUT_BACK);
		_tween.fadeTo(_enabled ? Constants.ALPHA_MAX : 0.8);
		_tween.scaleTo(_enabled ? 1.0 : 0.98); 
		
		Starling.juggler.add(_tween);
	}
	
	public function get enable():Boolean 			{ return _enabled; }
	
	public function set selected(s:Boolean):void 	{ _selected = s; }
	public function get selected():Boolean 			{ return _selected; }
	
	public function set id(i:uint):void 			{ _id = i; }
	public function get id():uint 					{ return _id; }
}

internal class CharacterContainer extends DisplayObjectContainer {

	private var _characters:Vector.<Character>;
	
	private var _tokens:Vector.<Image>;
	private var _tokenOffset:Vector2;
	
	private var _select:uint;
	private var _curChar:uint;
	
	public function CharacterContainer(chars:Vector.<Character>, tokens:Vector.<Image>) {
	
		_characters = chars;
		_tokens = tokens;
		
		_select = 0;
		var i:uint = 0;
		
		var offset:uint = 0;
		for(i = 0; i < Constants.NUM_CHARACTER_MAX; ++i) {
			
			if(i > 0)
				offset += chars[i-1].width;
			
			chars[i].x = offset;
			
			chars[i].enable = false;
			addChild(chars[i]);
			chars[i].addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		/*
		var displacement:Number = (chars[0].width * Constants.NUM_CHARACTER_MAX);
		
		var startX:Number = -200;
		var startY:Number = 0;
		
		for(i = 0; i < Constants.NUM_CHARACTER_MAX; ++i) {
			
			chars[i].x = startX;
			chars[i].y = startY;
			
			var tween:Tween = new Tween(chars[ (Constants.NUM_CHARACTER_MAX-1) - i ], 2, Transitions.EASE_OUT);
			tween.moveTo(displacement, 0);
			displacement -= chars[i].width;
			
			Starling.juggler.add(tween);
			
			addChild(chars[i]);
		}
		*/
		/*
		var offsetMax:Number = (chars[0].width * Constants.NUM_CHARACTER_MAX);
		
		for (i = 0; i < Constants.NUM_CHARACTER_MAX; i++)
		{
			chars[i].x = -800;
			chars[i].enable = false;
			addChild(chars[i]);
			chars[i].addEventListener(TouchEvent.TOUCH, onTouch);
			
			
			var tween:Tween = new Tween(chars[Constants.NUM_CHARACTER_MAX - 1 - i],2, Transitions.EASE_OUT);
			tween.moveTo(offsetMax - 500, 0);
			offsetMax -= chars[i].width;
			
			Starling.juggler.add(tween);
		}*/
		
		var offsetX:Number = -chars[0].width>>1;
		var offsetY:Number = -chars[0].height>>1;
		for(i = 0; i < Constants.NUM_PLAYER_MAX; ++i) {
			
			_tokens[i].x = offsetX;
			_tokens[i].y = offsetY;
			
			_tokens[i].pivotX = _tokens[i].width >> 1;
			_tokens[i].pivotY = _tokens[i].height >> 1;
		}
		
		_curChar = 0;
		_characters[_curChar].enable = true;
		_characters[_curChar].addChild(_tokens[_select]);
	}	
	
	public function getCurChar():uint {
		return _curChar;
	}
	
	public function set selected(idx:uint):void {
		
		if(!_characters[idx].selected) {
			_characters[idx].selected = true;
			++_select;
		}
	}
	
	public function get selected():uint {
		return _select;
	}
	
	public function onTouch(e:TouchEvent):void {
		
		var touch:Touch = e.getTouch(this);
		if(!touch) return;
		
		if(touch.phase == TouchPhase.BEGAN) {
			var char:Character = e.currentTarget as Character;
			
			if(char.selected)
				return;
			
			if( !char.enable ) {
				
				for(var i:uint = 0; i < Constants.NUM_CHARACTER_MAX; ++i) {
					
					if(char.id == i) {
						
						_characters[_curChar].removeChild(_tokens[_select]);
						char.enable = true;
						_curChar = i;
						_characters[_curChar].addChild(_tokens[_select]);
					}
					else {
						
						if(!_characters[i].selected)
							_characters[i].enable = false;
					}
				}
			}
		}
	}
	
	public function destroy():void {
		_characters.length = 0;
	}
}