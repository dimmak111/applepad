package applepad.ui.widgets
{
	import starling.display.Sprite;

	public interface ISlider
	{
		function onItemSelected(item:Sprite):void;
		function onItemChanged(item:Sprite):void;
	}
}