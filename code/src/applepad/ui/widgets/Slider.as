package applepad.ui.widgets
{
	import applepad.Constants;
	import applepad.Game;
	import applepad.utils.MathUtils;
	
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class Slider extends Sprite
	{
		private var _listener:ISlider;
		
		private var _itemList:Vector.<SliderSheet>;
		
		private var _width:Number;
		private var _height:Number;
		
		private var _xMargin:Number; 
		
		private var _speedX:Number;

		private var _beginCoord:Point;
		private var _lastCoord:Point;
		private var _endCoord:Point;
		
		private var _beginTime:Number;
		private var _endTime:Number;
		private var _lastTime:Number;
		
		private var _isDragging:Boolean;
		
		private var _centerSpot:Point;
		
		private var _curItem:uint;
		private var _nextItem:int;
		private var _touchedItem:uint;
		
		public function Slider(pos_x:Number, pos_y:Number, w:Number, h:Number, listener:ISlider = null):void {
			
			x = pos_x;
			y = pos_y;
			
			_width = w;
			_height = h;
			
			_centerSpot = new Point(_width >> 1, _height >> 1);
			_itemList = new Vector.<SliderSheet>;
			
			_curItem = 0;
			_nextItem = 0;
			_touchedItem = 0;
			
			_xMargin = 35;
			
			_speedX = 0;
			
			_lastTime = 0;
			_beginTime = 0;
			_endTime = 0;
			
			_isDragging = false;
			
			_listener = listener;
			addEventListener(Event.ADDED_TO_STAGE, start);
			addEventListener(TouchEvent.TOUCH, touch);
		}
		
		public function addSheet(sheet:Sprite):void {
			
			_itemList.push(sheet);
		}
		
		public function removeSheet(sheet:SliderSheet):void {
			
		}
		
		private function start():void {
					
			for(var i:uint = 0; i < _itemList.length; ++i) {
				_itemList[i].addEventListener(TouchEvent.TOUCH, touchSheet);
			}
			
			setCurItem(1);
			if (_listener) {
				_listener.onItemChanged(_itemList[_curItem]);
			}
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function touchSheet(e:TouchEvent): void {
			
			var touch:Touch = e.getTouch(stage);
			
			if(touch.phase == TouchPhase.BEGAN) {
				
				for(var i:uint = 0; i < _itemList.length; ++i) {
					
					if(e.currentTarget as DisplayObject == _itemList[i]) {
						_touchedItem = i;
						return;
					}
				}
			}
		}
		
		public function updateWidth():void {
			
			var nbItems:uint = _itemList.length;			
			var pos:Point = new Point(0, 0);
			
			for(var i:uint; i < nbItems; ++i) {
				
				var item:SliderSheet = _itemList[i];
				
				item.alpha = Constants.ALPHA_MAX;
				//item.scaleX = 1.5;
				//item.scaleY = 1.5;
				
				if(_curItem != i) {
					
					item.alpha = 0.6;
					//item.scaleX = 1.0;
					//item.scaleY = 1.0;
				}
				
				item.x = pos.x; 
				item.y = _height - item.height >> 1;
				
				pos.x += item.width + _xMargin;
				
				addChild(item);	
			}
		}
		
		public function isItemChanged(idx:uint):Boolean {
			
			if(_nextItem < _curItem)
			{
				if(_itemList[idx].getCenterX() >= _centerSpot.x){
					return true;
				}
			}
			else {
				if(_itemList[idx].getCenterX() <= _centerSpot.x) {
					return true;
				}
			}
			
			return false;
		}
		
		public function update():void {
			
			if(_itemList.length <= 0)
				return;
			
			var time:Number = getTimer();
			var dt:Number = time - _lastTime;
			
			if(MathUtils.abs(_speedX) > 10.0 ) 
			{	
				if(isItemChanged(_nextItem)) {
					
					_speedX = 0;
					_curItem = _nextItem;
					
					setCurItem(_curItem);
					if (_listener) {
						_listener.onItemChanged(_itemList[_curItem]);
					}
					
				} else {
					
					for(var i:uint = 0; i < _itemList.length; ++i) {
						
						_itemList[i].x += _speedX * dt * 0.001;		
					}
				}
			}
			
			_lastTime = time;
		}
		
		public function touch(e:TouchEvent):void {
			
			var touch:Touch = e.getTouch(stage);
			var pos:Point = touch.getLocation(stage);
			
			if( touch.phase == TouchPhase.BEGAN ) {
				
				_isDragging = true;
				
				_beginTime = getTimer();
				_beginCoord = pos;
				_lastCoord = pos;
			}
			else if( touch.phase == TouchPhase.MOVED ) {
				drag(pos);
			}
			else if( touch.phase == TouchPhase.ENDED ) {
				
				_isDragging = false;
				
				_endTime = getTimer();
				_endCoord = pos;
				
				var time:Number = _endTime - _beginTime;
				
				if(MathUtils.abs(_endCoord.x - _beginCoord.x) < 20 &&
					MathUtils.abs(_endCoord.y - _beginCoord.y) < 20) {
					
					if(_touchedItem != _curItem) {
						changeItem(_touchedItem);	
					}
					else if(_listener) {
						_listener.onItemSelected(_itemList[_curItem]); 
					}
				}
				else if(time > 0 && time < 250) {
					
					var item:uint = 0;
					
					if(_beginCoord.x > _endCoord.x) {
						item = _curItem + 1;	
					} else {
						item = _curItem - 1;	
					}
					
					changeItem(item);
					
				} else {
					
					for(var i:uint; i < _itemList.length; ++i) {
							
						if(_itemList[i].x < _centerSpot.x 
						&& _itemList[i].x + _itemList[i].width > _centerSpot.x
						&& _curItem != i) {
							setCurItem(i);
						}
					}
					
				}
			}
		}
		
		public function getCurItem():uint {
			return 0;
		}
		
		public function setCurItem(idx:uint):void {
		
			if(idx > _itemList.length)
				return;
			
			_curItem = idx;
			var item:SliderSheet = _itemList[idx];
			
			updateWidth();
			
			var stepX:Number = _centerSpot.x - item.getCenterX();
			
			for(var i:uint = 0; i < _itemList.length; ++i) {
				
				_itemList[i].x += stepX;
			}
		}
		
		public function changeItem(idx:uint): void {
			Game.getSoundManager().playSound("slide");
			_speedX = 1400.0;
			
			if(idx > _curItem) {
				
				_speedX = -_speedX;
			}
			
			_nextItem = idx;
			_nextItem = MathUtils.clamp(_nextItem, 0, _itemList.length-1);
			
			if(_nextItem == _curItem)
				_speedX = 0;
		}
		
		public function drag(p:Point):void {
			
			if(_itemList.length <= 0)
				return;
			
			if(_itemList[0].getCenterX() > _centerSpot.x ||
				_itemList[_itemList.length-1].getCenterX() < _centerSpot.x) {
				return;
			}
			
			var interval:Point = new Point();			
			
			interval.x = p.x - _lastCoord.x;
			interval.y = p.y - _lastCoord.y;
			
			for(var i:uint; i < _itemList.length; ++i) {
				
				_itemList[i].x += interval.x;
				
				/*
				if(_itemList[i].x < _centerSpot.x 
				&& _itemList[i].x + _itemList[i].width > _centerSpot.x) {
					setCurItem(i);
				}
				*/
			}
			
			_lastCoord = p;
			
		}
	}
}