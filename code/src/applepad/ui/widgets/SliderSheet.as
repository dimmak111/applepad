package applepad.ui.widgets
{
	import starling.display.Sprite;

	public class SliderSheet extends Sprite
	{
		public function SliderSheet(){
		}
		
		public function getCenterX():Number {
			return x + (width >> 1);	
		}
		
		public function getCenterY():Number {
			return y + (height >> 1);
		}
	}
}