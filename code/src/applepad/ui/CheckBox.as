package applepad.ui
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class CheckBox extends Sprite
	{	
		private var _back:Image;
		private var _checked:Image;
		
		private var _state:Boolean;
		
		private var _listener:ICheckBox
		
		public function CheckBox(back:Texture, checked:Texture, state:Boolean = false, listener:ICheckBox = null)
		{
			_back = new Image (back);
			_checked = new Image (checked);
			
			addChild(_back);
			addChild(_checked);
			
			_listener = listener;
			_state = state;
			
			_checked.visible = _state ? true : false; 
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function isChecked () : Boolean {
			return _state;
		}
		
		public function get state():Boolean {
			return _state; 
		}
		
		public function set state(s:Boolean):void {
			
			if(_state == s)
				return;
			
			_state = s;
			_checked.visible = s;			
		}
				
		public function onTouch (e:TouchEvent) :void {
			
			var touch:Touch = e.getTouch(this);
			
			if(!touch)
				return;
			
			if(touch.phase == TouchPhase.BEGAN) {
				state = !state;
				
				if(_listener)
					_listener.onCheckBox(this);
			}
		}
	}
}