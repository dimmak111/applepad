package applepad.ui
{
	public interface ICheckBox
	{
		function onCheckBox(checkBox:CheckBox):void;
	}
}