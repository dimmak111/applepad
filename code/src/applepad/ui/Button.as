package applepad.ui
{
	import feathers.controls.Button;
	import feathers.display.Image;
	
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class Button extends Sprite
	{
		public static const BUTTON_NORMAL:uint = 0;
		public static const BUTTON_TOUCHED:uint = 1;
		
		private var _state:int = -1;
		private var _statsImage:Vector.<Image>;
		
		private var _listener:IButton;
		
		public function Button(normal:Texture, touched:Texture = null, listener:IButton = null)
		{
			_statsImage = new Vector.<Image>();
			_statsImage.push(new Image(normal));
			
			if(touched) {
				_statsImage.push(new Image(touched));
			}
			
			for(var i:uint = 0; i < _statsImage.length; ++i) {
				addChild(_statsImage[i]);
			}
			
			_listener = listener;
			
			state = BUTTON_NORMAL;
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function get state():uint {
			return _state;
		}
		
		public function set state(s:uint):void {
			
			if(_state == s)
				return;
			
			_state = s;
			for(var i:uint = 0; i < _statsImage.length; ++i) {
				_statsImage[i].visible = false;
			}
			_statsImage[_state].visible = true;
		}
		
		private function onTouch(e:TouchEvent):void {
			
			var touch:Touch = e.getTouch(this);
			
			if(!touch)
				return;
			
			if(touch.phase == TouchPhase.BEGAN) {
				state = BUTTON_TOUCHED;
				
				if(_listener) {
					_listener.onButton(this);
				}
				
			}
			else if(touch.phase == TouchPhase.ENDED) {
				state = BUTTON_NORMAL;
				
				if(_listener) {
					_listener.onButton(this);
				}
			}
		}
	}
}