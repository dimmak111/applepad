package applepad.ui
{
	import applepad.utils.MathUtils;
	
	import feathers.controls.Slider;
	import feathers.display.Image;
	
	import flash.geom.Point;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.textures.Texture;

	public class SliderBar extends Sprite
	{
		private var _min:Number;
		private var _max:Number;
		private var _cur:Number;
		
		private var _empty:Image;
		private var _full:Image;
		
		private var _width:Number;
		private var _height:Number;
		
		private var _listener:ISliderBar;
		
		private var _texts:Array = new Array( new Point(0.0, 0.0), new Point(0.0, 1.0));
		
		public function SliderBar(empty:Texture, full:Texture, min:Number, max:Number, cur:Number = 0.5, listener:ISliderBar = null)
		{			
			_empty = new Image(empty);
			_full = new Image(full);
			
			
			_min = min;
			_max = max;
			
			value = cur;
			
			_width = _empty.width;
			_height = _empty.height;
			
			_listener = listener;
			
			addChild(_empty);
			addChild(_full);
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public function get value():Number {
			return _min + _cur * (_max - _min);
		}
		
		public function set value(v:Number):void {
			
			v = MathUtils.clamp(v, 0.0, 1.0);
			
			_cur = v;
			
			_texts[0].x = _cur;
			_texts[1].x = _cur;
			
			_full.scaleX = _cur;
			_full.setTexCoords(1, _texts[0]);
			_full.setTexCoords(3, _texts[1]);
			
			if(_listener) {
				_listener.onSliderChange(this);
			}
		}
		
		private function onTouch(e:TouchEvent):void {
			
			var touch:Touch = e.getTouch(this);
			
			if(!touch)
				return;
			
			var pos:Point = touch.getLocation(this);
			
			value = pos.x / _width;
		}
	}
}