package applepad.ui
{
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;

	public class TextInput extends DisplayObjectContainer
	{
		private var _back:Image;
		
		private var _showText:starling.text.TextField;
		private	var _inputText:flash.text.TextField;
		private var _inputFormat:TextFormat;
		
		private var _text:String;
		private var _displayAsPassword:Boolean;
		
		public function TextInput(width:Number, height:Number, text:String, fontName:String = "Verdana", fontSize:Number = 12, color:uint = 0x0, back:Texture = null) {
			super();
			
			_showText = new starling.text.TextField(width, height, text, fontName, fontSize, color);
			
			_inputText = new flash.text.TextField();
			_inputText.type = TextFieldType.INPUT;
			
			_inputText.x = _showText.x;
			_inputText.y = _showText.y;
			_inputText.width = width;
			_inputText.height = height;
			
			_inputFormat = new TextFormat();
			
			_back = new Image(back);
			
			addChild(_back);
			addChild(_showText);
			Starling.current.nativeOverlay.addChild(_inputText);
		}
		
		public function set text(t:String):void {
			_inputText.text = t;
		}
		
		public function get text():String {
			return _inputText.text; 
		}
	}
}