package applepad.ui
{
	import applepad.ui.widgets.Slider;

	public interface ISliderBar
	{
		function onSliderChange(slider:SliderBar):void;
	}
}