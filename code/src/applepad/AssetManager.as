package applepad
{		
	import applepad.utils.Closure;
	import applepad.entities.MovieClip;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import org.as3commons.collections.Map;
	import org.as3commons.collections.framework.IIterable;
	import org.as3commons.collections.framework.IIterator;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class AssetManager extends EventDispatcher
	{
		public var _assetBase:String = "app:"; 
		
		private var _textures:Map;
		private var _textureAtlas:Map;
		
		private var _queue:Map;
	
		public function AssetManager() {
			
			_textures = new Map();
			_textureAtlas = new Map();
			
			_queue = new Map();
		}
			
		private function Key(atlasGroup:String, atlasName:String):String {
			return atlasGroup + "|" + atlasName;
		}
			
		private function findFile( group:String, name:String):String {
			return this._assetBase + "/" + group + "/" + name;
		}
			
		public function getTexture(group:String, name:String):Texture {
			
			var texture:Texture = _textures.itemFor(Key(group, name));		
			return texture;
		}
		
		public function getTextureAtlas(group:String, name:String):TextureAtlas {
			
			if(!_textureAtlas.hasKey(Key(group, name))) {
				throw new Error("Texture atlas : " + Key(group, name) + " not found");
			}
			
			return _textureAtlas.itemFor(Key(group, name));
		}
		
		public function getImageFromAtlas(group:String, name:String, text:String):Image {
			
			if(!_textureAtlas.hasKey(Key(group, name))) {
				throw new Error("Texture atlas : " + Key(group, name) + " not found");
			}
			
			var atlas:TextureAtlas = _textureAtlas.itemFor(Key(group, name));
			return new Image(atlas.getTexture(text));
		}
		
		public function getTextureFromAtlas(group:String, name:String, text:String):Texture {
			
			if(!_textureAtlas.hasKey(Key(group, name))) {
				throw new Error("Texture atlas : " + Key(group, name) + " not found");
			}
			
			var atlas:TextureAtlas = _textureAtlas.itemFor(Key(group, name));	
			return atlas.getTexture(text);
		}
		
		public function getTexturesFromAtlas(group:String, name:String, text:String):Vector.<Texture> {
			
			if(!_textureAtlas.hasKey(Key(group, name))) {
				throw new Error("Texture atlas : " + Key(group, name) + " not found");
			}
			
			var atlas:TextureAtlas = _textureAtlas.itemFor(Key(group, name));	
			return atlas.getTextures(text);	
		}
		
		public function getMovieFromAtlas(group:String, name:String, movie:String, frameRate:uint = 12, toJuggle:Boolean = false, isLooping:Boolean = true):MovieClip {
			
			var t:Vector.<Texture> = getTexturesFromAtlas(group, name, movie);	
			
			if(t.length == 0) {
				throw new Error("MovieClip " + movie + " not found in " + group + "/" + name);
			}
			
			return new MovieClip(t, frameRate, toJuggle, isLooping);
		}
		
		public function loadTexture(group:String, name:String):void {
			
			var path:String = findFile(group, name);	
			_queue.add(Key(group, name), new AssetWrapperInfo("Texture", group, name));	
		}
		
		public function loadAtlas(group:String, name:String):void {
			
			var path:String = findFile(group, name);			
			_queue.add(Key(group, name), new AssetWrapperInfo("Atlas", group, name));	
		}
		
		public function loadXML(group:String, name:String):XML {
			
			var filePath:String = findFile(group, name);
			var stream:FileStream = new FileStream();
			var file:File = new File((filePath + ".xml"));
			
			try {
			
				stream.open(file, FileMode.READ);
				var xml:XML = XML(stream.readUTFBytes(stream.bytesAvailable));
			}
			catch(e:Error) {
				
				throw e + " " + filePath + ".xml";
			}
			
			stream.close();
			
			return xml;
		}
		
		public function loadAssets():void {
			
			var iterator:IIterator = _queue.iterator();				
			var keyIterator:IIterator = _queue.keyIterator();
			
			while(iterator.hasNext()) {
				
				var key:String = keyIterator.next();
				var item:AssetWrapperInfo = iterator.next();
				
				var filePath:String = findFile(item._group, item._name);
				
				var url:URLRequest = new URLRequest(filePath + ".png");
				var loader:Loader = new Loader();
				
				switch(item._type)
				{
					case "Texture":
					{
						loader.load(url);
						loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, Closure.create(this, this.onTextureLoaded, key));
						loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorTextureLoad);
					}
					break;
					
					case "Atlas":
					{		
						var xml:XML = loadXML(item._group, item._name);
									
						loader.load(url);
						loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, Closure.create(this, this.onTextureAtlasLoaded, key, xml));
						loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorTextureAtlasLoad);
					}	
					break;
				}
			}
		}
		
		
		public function onTextureLoaded(e:flash.events.Event, key:String):void {
				
			_queue.remove(_queue.itemFor(key));
			
			var bm:Bitmap = e.target.content as Bitmap;
			var texture:Texture = Texture.fromBitmap(bm, false, false);
			
			_textures.add(key, texture);
			
			if(_queue.size == 0) {
				dispatchEventWith(starling.events.Event.COMPLETE);
			}
		}
		
		public function onTextureAtlasLoaded(e:flash.events.Event, key:String, xml:XML):void {
			
			_queue.remove(_queue.itemFor(key));
			
			var bm:Bitmap = e.target.content as Bitmap;
			var texture:Texture = Texture.fromBitmap(bm, false, false);
			
			// create texture atlas
			var atlas:TextureAtlas = new TextureAtlas(texture, xml); 
			_textureAtlas.add(key, atlas);
			
			if(_queue.size == 0) {
				dispatchEventWith(starling.events.Event.COMPLETE);
			}
		}
		
		public function errorTextureLoad(e:IOErrorEvent):void {
			trace(e);
		}
		
		public function errorTextureAtlasLoad(e:IOErrorEvent):void {
			trace(e);
		}
		
		public function freeAssets(): void {

			var it:IIterator = _textureAtlas.iterator();
			while(it.hasNext()) {
				var text:TextureAtlas = it.next();
				text.dispose();
			}
			
			_textures.clear();
			_textureAtlas.clear();
		}
				
		public function isQueueEmpty():Boolean {
			return _queue.size == 0;
		}
		
		//load Font, Sounds.
	}
}
import flash.net.URLRequest;
internal class AssetWrapperInfo {
	
	public var _type:String;
	public var _group:String;
	public var _name:String;
	
	public function AssetWrapperInfo(type:String, group:String, name:String) {
		
		_group = group;
		_name = name;
		_type = type;
	}
	
}
