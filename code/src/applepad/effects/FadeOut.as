package applepad.effects
{
	import starling.display.DisplayObject;

	public class FadeOut implements IEffect
	{
		private var _toFadeIn:DisplayObject;
		private var _duration:Number;
		private var _listener:IFadeOut;
		private var _curTime:Number;
		private var _curAlpha:Number;
		
		public function FadeOut(toFadeIn:DisplayObject, duration:Number, listener:IFadeOut = null)
		{
			_toFadeIn = toFadeIn;
			_duration = duration;
			_listener = listener;
			_curTime = 0;
			_curAlpha = 1.0;
		}
		
		public function update(elapsedTime:Number):Boolean {
			
			_curTime += elapsedTime;
			
			if(_curTime >= _duration) {
				
				_curTime = _duration;
				
				if(_listener != null) {
					_listener.onFadeOutFinished(this);
				}
			}
			
			_curAlpha = 1.0 - (1.0 * _curTime) / _duration;
			_toFadeIn.alpha = _curAlpha;
			
			return (_curTime == _duration);
		}
	}
}