package applepad.effects
{
	import applepad.utils.MathUtils;
	import applepad.utils.Vector2;
	
	import starling.animation.IAnimatable;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class EarthQuake extends EventDispatcher implements IAnimatable
	{
		private var mTarget:DisplayObject;
		private var mDeltaIntensity:Number;
		private var mTime:Number;
		private var mCurTime:Number;
		
		private var mStartPos:Vector2;
		
		public function EarthQuake(target:DisplayObject, time:Number, delta:Number)
		{
			mTarget = target;
			mDeltaIntensity = delta;
			
			mTime = time;
			mCurTime = 0;
			
			mStartPos = new Vector2(target.x, target.y);
		}
		
		public function advanceTime(time:Number):void {
			
			if(mCurTime < mTime)
			{
				mTarget.x = mStartPos.x + MathUtils.randi(-mDeltaIntensity, mDeltaIntensity);
				mTarget.y = mStartPos.y + MathUtils.randi(-mDeltaIntensity, mDeltaIntensity);
				
				mCurTime += time;
			}
			else
			{
				mTarget.x = mStartPos.x;
				mTarget.y = mStartPos.y;
				
				dispatchEventWith(Event.REMOVE_FROM_JUGGLER);
			}
		}
	}
}