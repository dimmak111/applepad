package applepad.effects
{
	import starling.display.DisplayObject;

	public class FadeIn implements IEffect
	{
		private var _toFadeIn:DisplayObject;
		private var _duration:Number;
		private var _listener:IFadeIn;
		private var _curTime:Number;
		private var _curAlpha:Number;
		
		public function FadeIn(toFadeIn:DisplayObject, duration:Number, listener:IFadeIn = null)
		{
			_toFadeIn = toFadeIn;
			_duration = duration;
			_listener = listener;
			_curTime = 0;
			_curAlpha = 0.0;
		}
		
		public function update(elapsedTime:Number):Boolean {
			
			_curTime += elapsedTime;
			
			if(_curTime >= _duration) {
				
				if(_listener != null) {
					_listener.onFadeInFinished(this);
				}
				
				_curTime = _duration;
			}
			
			_curAlpha = (1.0 * _curTime) / _duration;
			_toFadeIn.alpha = _curAlpha;
			
			return (_curTime == _duration);
		}
	}
}