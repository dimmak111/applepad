package applepad.effects
{
	public interface IFadeOut
	{
		function onFadeOutFinished(e:FadeOut):void
	}
}