package applepad.effects
{
	import applepad.Constants;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;

	public class Effect
	{
		private static var pool:EffectPool;
		
		public static function Init():void {
			pool = new EffectPool();
		}
		
		public static function CreateFadeIn(target:DisplayObject, duration:Number, transition:Object="linear", listener:ITween = null):Tween {
			
			target.alpha = 0.0;
			
			var t:Tween = pool.createTween(target, duration, transition);
			Starling.juggler.add(t);
			
			t.onComplete = Destroy;
			t.onCompleteArgs = [t, listener];
			
			t.fadeTo(Constants.ALPHA_MAX);
			
			return t;
		} 
		
		public static function CreateFadeOut(target:DisplayObject, duration:Number, transition:Object="linear", listener:ITween = null):Tween {
			
			target.alpha = Constants.ALPHA_MAX;
			
			var t:Tween = pool.createTween(target, duration, transition);
			Starling.juggler.add(t);
			
			t.onComplete = Destroy;
			t.onCompleteArgs = [t, listener];
			
			t.fadeTo(0);
			
			return t;
		}
		
		public static function CreateScaleTo(target:DisplayObject, scale:Number, duration:Number, transition:Object="linear", listener:ITween = null):Tween {
			
			var t:Tween = pool.createTween(target, duration, transition);
			Starling.juggler.add(t);
			
			t.onComplete = Destroy;
			t.onCompleteArgs = [t, listener];
			
			t.scaleTo(scale);	
			
			return t;
		}
		
		public static function CreateMoveTo(target:DisplayObject, x:Number, y:Number, duration:Number, transition:Object="linear", listener:ITween = null):Tween {
			
			var t:Tween = pool.createTween(target, duration, transition);
			Starling.juggler.add(t);
			
			t.onComplete = Destroy;
			t.onCompleteArgs = [t, listener];
			
			t.moveTo(x, y);
			
			return t;
		}
		
		public static function CreateEarthQuake(target:DisplayObject, duration:Number):void {
			
			Starling.juggler.add(new EarthQuake(target, duration, 15));
		}
		
		public static function Destroy(t:Tween, listener:ITween):void {
			
			if(listener)
				listener.onTweenFinished(t);
			
			pool.destroyTween(t);
		}
	}
}
import starling.animation.Tween;
import starling.display.DisplayObject;

internal class EffectPool {
	
	private var pool:Vector.<Tween>;
	private var count:uint;
	
	public function EffectPool(size:uint = 20):void {
		
		pool = new Vector.<Tween>(size);
		for(var i:uint = 0; i < size; ++i) {
			pool[i] = new Tween(null, 0);
		}
		
		count = size;
	}
	
	public function createTween(target:DisplayObject, duration:Number, transition:Object):Tween {
		
		if(count > 0) {
			--count;
			return pool.pop().reset(target, duration, transition);
		}
		
		++count
		pool.push(new Tween(target, duration, transition));
		return pool[pool.length-1];
	}
	
	public function destroyTween(t:Tween):void {
		
		t.reset(null, 0);
		++count;
		pool.push(t);
	}
}