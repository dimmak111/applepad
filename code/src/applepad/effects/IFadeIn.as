package applepad.effects
{
	public interface IFadeIn
	{
		function onFadeInFinished(e:FadeIn):void
	}
}