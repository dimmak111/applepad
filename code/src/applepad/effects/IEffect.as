package applepad.effects
{
	public interface IEffect
	{
		function update(elapsedTime:Number):Boolean;
	}
}