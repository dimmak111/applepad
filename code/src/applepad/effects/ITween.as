package applepad.effects
{
	import starling.animation.Tween;

	public interface ITween
	{
		function onTweenFinished(t:Tween):void
	}
}