#!/bin/sh


echo "Resizing file $file ..."
convert icon.png -resize 128x128 icon_128.png
convert icon.png -resize 114x114 icon_114.png
convert icon.png -resize 72x72 icon_72.png
convert icon.png -resize 57x57 icon_57.png
convert icon.png -resize 48x48 icon_48.png
convert icon.png -resize 36x36 icon_36.png
convert icon.png -resize 32x32 icon_32.png
convert icon.png -resize 16x16 icon_16.png
