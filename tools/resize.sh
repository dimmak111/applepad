#!/bin/sh

for file in *.png 
do 
	echo "Resizing file $file ..."
	mogrify -resize $1x$2 "$file" "$file" 
done