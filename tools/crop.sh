#!/bin/sh

for file in *.png 
do 
	echo "Cropping file $file ..."
	convert -crop 200x200+10+10 "$file" cropped_"$file"
done